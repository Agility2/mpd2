<?php
namespace DesignStudioElementorAddons\Core\Settings\General;

use Elementor\Controls_Manager;
use Elementor\Core\Settings\Base\Model as BaseModel;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class Model extends BaseModel {

	/**
	 * @since 1.6.0
	 * @access public
	 */
	public function get_name() {
		return 'global-settings';
	}

	/**
	 * @since 1.6.0
	 * @access public
	 */
	public function get_css_wrapper_selector() {
		return '';
	}

	/**
	 * @since 1.6.0
	 * @access public
	 */
	public function get_panel_page_settings() {
		return [
			'title' => __( 'Global Settings', 'elementor' ),
			'menu' => [
				'icon' => 'fa fa-cogs',
				'beforeItem' => 'elementor-settings',
			],
		];
	}

	/**
	 * @since 1.6.0
	 * @access public
	 * @static
	 */
	 protected function _register_controls() {





	 	$this->start_controls_section(
	 		'section_content',
	 		[
	 			'label' => __( 'Content', 'ds-el' ),
	 		]
	 	);




	 	$this->add_control(
	 			'per_line',
	 			[
	 					'label' => __('Columns per row', 'ds-el'),
	 					'type' => Controls_Manager::NUMBER,
	 					'min' => 1,
	 					'max' => 6,
	 					'step' => 1,
	 					'default' => 3,
	 			]
	 	);


	 	$this->add_control(
	 			'cubes',
	 			[
	 					'label' => __('Cubes', 'ds-el'),
	 					'type' => Controls_Manager::REPEATER,
	 					'separator' => 'before',
	 					'default' => [
	 							[
	 									'cube_title' => __('Cube 1', 'ds-el'),
	 									'cube_content' => __('Content Here', 'ds-el'),
	 							],
	 							[
	 									'cube_title' => __('Cube 2', 'ds-el'),
	 									'cube_content' => __('Content Here', 'ds-el'),
	 							],
	 							[
	 									'cube_title' => __('Cube 3', 'ds-el'),
	 									'cube_content' => __('Content Here', 'ds-el'),
	 							],
	 					],
	 					'fields' => [
	 							[
	 									'name' => 'cube_title',
	 									'label' => __('Title', 'ds-el'),
	 									'type' => Controls_Manager::TEXT,
	 							],

	 							 [
	 									'name'				=> 'icon_url',
	 									'label'       => __( 'Image URL', 'ds-el' ),
	 									'type'        => Controls_Manager::TEXT,
	 									// 'default'     => __( '/wp-content/plugins/elementor/assets/images/placeholder.png', 'ds-el' ),
	 									'placeholder' => __( 'http://imagepathhere.com', 'ds-el' ),
	 							 ],
	 							[
	 									'name' => 'cube_content',
	 									'label' => __('Cube Content', 'ds-el'),
	 									'type' => Controls_Manager::TEXTAREA,
	 									'default' => __('Cube Content', 'ds-el'),
	 									'description' => __('Keep this short.', 'ds-el'),
	 									'label_block' => true,
	 							],
	 							[
	 									'name' => 'cube_cta_text',
	 									'label' => __('Cube CTA Text', 'ds-el'),
	 									'type' => Controls_Manager::TEXT,
	 									'default' => __('', 'ds-el'),
	 									'label_block' => true,
	 							],
	 							[
	 									'name' => 'cube_cta_url',
	 									'label' => __('Cube CTA URL', 'ds-el'),
	 									'type' => Controls_Manager::TEXT,
	 									'default' => __('', 'ds-el'),
	 									'label_block' => true,
	 							],


	 							[
	 									'name'	=> 'cube_bg_options',
	 									'label' => __( 'Background Options', 'ds-el' ),
	 									'type' => Controls_Manager::CHOOSE,
	 									'options' => [
	 											'image'    => [
	 													'title' => __( 'Image', 'ds-el' ),
	 													'icon' => 'fa fa-picture-o',
	 											],
	 											'classic' => [
	 													'title' => __( 'Classic', 'ds-el' ),
	 													'icon' => 'fa fa-paint-brush',
	 											],
	 											'gradient' => [
	 													'title' => __( 'Gradient', 'ds-el' ),
	 													'icon' => 'fa fa-barcode',
	 											],
	 									],
	 							],

	 							[
	 								 'name'				=> 'cube_bg_image',
	 								 'label'       => __( 'BG Image URL', 'ds-el' ),
	 								 'type'        => Controls_Manager::TEXT,
	 								 'placeholder' => __( 'URL', 'ds-el' ),
	 								 'condition' => [
	 									'cube_bg_options' => 'image',
	 								],

	 							],

	 							[
	 								'name'				=> 'cube_bg_color',
	 								'label' => __( 'Color', 'ds-el' ),
	 								'type' => Controls_Manager::COLOR,
	 								'condition' => [
	 								 'cube_bg_options' => 'classic',
	 							 ],
	 							],


	 					],
	 					'title_field' => '{{{ cube_title }}}',
	 			]
	 	);

	 	$this->end_controls_section();

	 	$this->start_controls_section(
	 			'section_cube_styles',
	 			[
	 					'label' => __('Cube Styles', 'ds-el'),
	 					'tab' => Controls_Manager::TAB_STYLE,
	 			]
	 	);

	 	$this->add_responsive_control(
	 			'cube_min_height',
	 			[
	 					'label' => __('Min-Height (px)', 'ds-el'),
	 					'type' => Controls_Manager::SLIDER,
	 					'default' => [
	 							'size' => 50,
	 					],
	 					'range' => [
	 							'px' => [
	 									'max' => 1000,
	 									'min' => 50,
	 									'step' => 10,
	 							],
	 					],
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face' => 'min-height: {{SIZE}}px;',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 			'cube_gutters',
	 			[
	 					'label' => __('Cube Gutters', 'ds-el'),
	 					'type' => Controls_Manager::DIMENSIONS,
	 					'size_units' => ['px', '%'],
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube__wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 			'cube_padding',
	 			[
	 					'label' => __('Cube Padding', 'ds-el'),
	 					'type' => Controls_Manager::DIMENSIONS,
	 					'size_units' => ['px', '%'],
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube__wrapper .ds-cube__face' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 			'cube-default-bg',
	 			[
	 					'label' => __('Front Face Background Color', 'ds-el'),
	 					'type' => Controls_Manager::COLOR,
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-default' => 'background-color: {{VALUE}};',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 			'cube-hover-bg',
	 			[
	 					'label' => __('Back Face Background Color', 'ds-el'),
	 					'type' => Controls_Manager::COLOR,
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-hover' => 'background-color: {{VALUE}};',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 'cube-content-vert-pos',
	 [
	 	 'label'       => __( 'Content Vertical Position', 'ds-el' ),
	 	 'type' => Controls_Manager::SELECT,
	 	 'default' => 'center',
	 	 'options' => [
	 		'center'  => __( 'Middle', 'ds-el' ),
	 		'flex-start' => __( 'Top', 'ds-el' ),
	 		'flex-end' => __( 'Bottom', 'ds-el' ),
	 	 ],
	  'selectors' => [ // You can use the selected value in an auto-generated css rule.
	 		'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face' => 'justify-content: {{VALUE}}',
	  ],
	 ]
	 );



	 	$this->end_controls_section();


	 	$this->start_controls_section(
	 			'cubes_title',
	 			[
	 					'label' => __('Cube Title', 'ds-el'),
	 					'tab' => Controls_Manager::TAB_STYLE,
	 			]
	 	);

	 	$this->add_control(
	 		'title_position',
	 		[
	 			 'label'       => __( 'Title Position', 'ds-el' ),
	 			 'type' => Controls_Manager::SELECT,
	 			 'default' => '2',
	 			 'options' => [
	 				'2'  => __( 'Bottom', 'ds-el' ),
	 				'1' => __( 'Top', 'ds-el' ),
	 			 ],
	 		 'selectors' => [ // You can use the selected value in an auto-generated css rule.
	 				'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title' => 'order: {{VALUE}}',
	 		 ],
	 		]
	 	);

	 	$this->add_control(
	 			'title_tag',
	 			[
	 					'label' => __('Title HTML Tag', 'ds-el'),
	 					'type' => Controls_Manager::SELECT,
	 					'options' => [
	 							'h1' => __('H1', 'ds-el'),
	 							'h2' => __('H2', 'ds-el'),
	 							'h3' => __('H3', 'ds-el'),
	 							'h4' => __('H4', 'ds-el'),
	 							'h5' => __('H5', 'ds-el'),
	 							'h6' => __('H6', 'ds-el'),
	 							'div' => __('div', 'ds-el'),
	 					],
	 					'default' => 'h3',
	 			]
	 	);


	 	$this->add_control(
	 			'title_color',
	 			[
	 					'label' => __('Title Color', 'ds-el'),
	 					'type' => Controls_Manager::COLOR,
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title' => 'color: {{VALUE}};',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 			'title_background',
	 			[
	 					'label' => __('Title Background Color', 'ds-el'),
	 					'type' => Controls_Manager::COLOR,
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title' => 'background: {{VALUE}};',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 			'title_padding',
	 			[
	 					'label' => __('Title Padding', 'ds-el'),
	 					'type' => Controls_Manager::DIMENSIONS,
	 					'size_units' => ['px', '%'],
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube__wrapper .ds-cube__face .ds-cube__title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	 					],
	 			]
	 	);

	 	$this->add_control(
	 			'title_margin',
	 			[
	 					'label' => __('Title Margins', 'ds-el'),
	 					'type' => Controls_Manager::DIMENSIONS,
	 					'size_units' => ['px', '%'],
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube__wrapper .ds-cube__face .ds-cube__title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	 					],
	 			]
	 	);

	 	$this->add_group_control(
	 		Group_Control_Text_Shadow::get_type(),
	 		[
	 			'name' => 'title_text_shadow',
	 			'selector' => '{{WRAPPER}} .ds-cube__title',
	 		]
	 	);

	 	$this->add_group_control(
	 			Group_Control_Typography::get_type(),
	 			[
	 					'name' => 'title_typography',
	 					'selector' => '{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title',
	 			]
	 	);

	 	$this->end_controls_section();

	 	$this->start_controls_section(
	 			'cubes_content',
	 			[
	 					'label' => __('Cube Content', 'ds-el'),
	 					'tab' => Controls_Manager::TAB_STYLE,
	 			]
	 	);

	 	$this->add_control(
	 			'content_color',
	 			[
	 					'label' => __('Content Color', 'ds-el'),
	 					'type' => Controls_Manager::COLOR,
	 					'selectors' => [
	 							'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-hover' => 'color: {{VALUE}};',
	 					],
	 			]
	 	);

	 	$this->add_group_control(
	 		Group_Control_Text_Shadow::get_type(),
	 		[
	 			'name' => 'content_text_shadow',
	 			'selector' => '{{WRAPPER}} .ds-cube__face-hover .ds-cube__content',
	 		]
	 	);

	 	$this->add_group_control(
	 			Group_Control_Typography::get_type(),
	 			[
	 					'name' => 'content_typography',
	 					'selector' => '{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-hover',
	 			]
	 	);

	 	$this->end_controls_section();


	 	$this->start_controls_section(
	 			'icon_styles',
	 			[
	 					'label' => __('Icon Styles', 'ds-el'),
	 					'tab' => Controls_Manager::TAB_STYLE,
	 			]
	 	);



	 $this->add_control(
	 	'icon_width',
	 	[
	 			'label' => __( 'Image Width', 'ds-el' ),
	 			'type' => Controls_Manager::SLIDER,
	 			'default' => [
	 					'size' => 150,
	 			],
	 			'range' => [
	 					'px' => [
	 							'min' => 0,
	 							'max' => 1000,
	 							'step' => 5,
	 					],
	 					'%' => [
	 							'min' => 0,
	 							'max' => 100,
	 					],
	 			],
	 			'size_units' => [ 'px', '%' ],
	 			'selectors' => [
	 					'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__icon' => 'width: {{SIZE}}{{UNIT}};',
	 			],
	 	]
	 );


	 $this->add_control(
	 'icon_margin',
	 [
	 	 'label' => __( 'Image Margin', 'ds-el' ),
	 	 'type' => Controls_Manager::DIMENSIONS,
	  'size_units' => [ 'px', '%', 'em' ],
	  'selectors' => [
	 		'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	  ],
	 ]
	 );


	 	$this->end_controls_section();


	 }


}
