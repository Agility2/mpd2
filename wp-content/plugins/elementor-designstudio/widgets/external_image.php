<?php
namespace DesignStudioElementorAddons\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class External_Image extends Widget_Base {
	/**
	 * Retrieve image widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'external_image';
	}

	/**
	 * Retrieve image widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'External Image', 'ds-el' );
	}

	/**
	 * Get widget category.
	 *
	 * Retrieve video widget category.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget Category.
	 */
	public function get_categories()
	{
			return array( 'ds-el-elements' );
	}

	/**
	 * Retrieve image widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-insert-image';
	}

	/**
	 * Register image widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_image',
			[
				'label' => __( 'Image', 'ds-el' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'       => __( 'Link to Image', 'ds-el' ),
				'type'        => Controls_Manager::TEXT,
				// 'default'     => __( 'URL', 'ds-el' ),
				'placeholder' => __( 'http://linktoimage.com', 'ds-el' ),
			]
		);

		// $this->add_group_control(
		// 	Group_Control_Image_Size::get_type(),
		// 	[
		// 		'name' => 'image', // Actually its `image_size`.
		// 		'label' => __( 'Image Size', 'ds-el' ),
		// 		'default' => 'large',
		// 	]
		// );

		$this->add_responsive_control(
			'align',
			[
				'label'     => __( 'Alignment', 'ds-el' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => __( 'Left', 'ds-el' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'ds-el' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'ds-el' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => 'center',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'caption',
			[
				'label'       => __( 'Caption', 'ds-el' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'placeholder' => __( 'Enter your caption about the image', 'ds-el' ),
				'title'       => __( 'Input image caption here', 'ds-el' ),
			]
		);

		$this->add_control(
			'link_to',
			[
				'label'   => __( 'Link to', 'ds-el' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'none',
				'options' => [
					'none'   => __( 'None', 'ds-el' ),
					'custom' => __( 'Custom URL', 'ds-el' ),
				],
			]
		);

		$this->add_control(
			'link',
			[
				'label'       => __( 'Link to', 'ds-el' ),
				'type'        => Controls_Manager::URL,
				'placeholder' => __( 'http://your-link.com', 'ds-el' ),
				'condition'   => [
					'link_to' => 'custom',
				],
				'show_label'  => false,
			]
		);

		$this->add_control(
			'open_lightbox',
			[
				'label'     => __( 'Lightbox', 'ds-el' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'default',
				'options'   => [
					'default' => __( 'Default', 'ds-el' ),
					'yes'     => __( 'Yes', 'ds-el' ),
					'no'      => __( 'No', 'ds-el' ),
				],
				'condition' => [
					'link_to' => 'file',
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label'   => __( 'View', 'ds-el' ),
				'type'    => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_image',
			[
				'label' => __( 'Image', 'ds-el' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'space',
			[
				'label'          => __( 'Size (%)', 'ds-el' ),
				'type'           => Controls_Manager::SLIDER,
				'default'        => [
					'size' => 100,
					'unit' => '%',
				],
				'tablet_default' => [
					'unit' => '%',
				],
				'mobile_default' => [
					'unit' => '%',
				],
				'size_units'     => [ '%' ],
				'range'          => [
					'%' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'selectors'      => [
					'{{WRAPPER}} .elementor-image img' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'opacity',
			[
				'label'     => __( 'Opacity (%)', 'ds-el' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'size' => 1,
				],
				'range'     => [
					'px' => [
						'max'  => 1,
						'min'  => 0.10,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image img' => 'opacity: {{SIZE}};',
				],
			]
		);

		$this->add_control(
			'hover_animation',
			[
				'label' => __( 'Hover Animation', 'ds-el' ),
				'type'  => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'image_border',
				'label'     => __( 'Image Border', 'ds-el' ),
				'selector'  => '{{WRAPPER}} .elementor-image img',
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'image_border_radius',
			[
				'label'      => __( 'Border Radius', 'ds-el' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .elementor-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'image_box_shadow',
				'exclude'  => [
					'box_shadow_position',
				],
				'selector' => '{{WRAPPER}} .elementor-image img',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_caption',
			[
				'label' => __( 'Caption', 'ds-el' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'caption_align',
			[
				'label'     => __( 'Alignment', 'ds-el' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'    => [
						'title' => __( 'Left', 'ds-el' ),
						'icon'  => 'fa fa-align-left',
					],
					'center'  => [
						'title' => __( 'Center', 'ds-el' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'   => [
						'title' => __( 'Right', 'ds-el' ),
						'icon'  => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'ds-el' ),
						'icon'  => 'fa fa-align-justify',
					],
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .widget-image-caption' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'text_color',
			[
				'label'     => __( 'Text Color', 'ds-el' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .widget-image-caption' => 'color: {{VALUE}};',
				],
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'caption_typography',
				'selector' => '{{WRAPPER}} .widget-image-caption',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render image widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings();

		if ( empty( $settings['image'] ) ) {
			return;
		}

		$has_caption = ! empty( $settings['caption'] );

		$this->add_render_attribute( 'wrapper', 'class', 'elementor-image' );

		if ( ! empty( $settings['shape'] ) ) {
			$this->add_render_attribute( 'wrapper', 'class', 'elementor-image-shape-' . $settings['shape'] );
		}

		$link = $this->get_link_url( $settings );

		if ( $link ) {
			$this->add_render_attribute( 'link', [
				'href'                         => $link['url'],
				'class'                        => 'elementor-clickable',
				'data-elementor-open-lightbox' => $settings['open_lightbox'],
			] );

			if ( ! empty( $link['is_external'] ) ) {
				$this->add_render_attribute( 'link', 'target', '_blank' );
			}

			if ( ! empty( $link['nofollow'] ) ) {
				$this->add_render_attribute( 'link', 'rel', 'nofollow' );
			}
		} ?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php if ( $has_caption ) : ?>
			<figure class="wp-caption">
				<?php
				endif;

				if ( $link ) :
				?>
				<a <?php echo $this->get_render_attribute_string( 'link' ); ?>>
					<?php
					endif;

					// echo Group_Control_Image_Size::get_attachment_image_html( $settings );
					echo '<img src="' . $settings['image'] . '">';

					if ( $link ) :
					?>
				</a>
			<?php
			endif;

			if ( $has_caption ) :
				?>
				<figcaption
					class="widget-image-caption wp-caption-text"><?php echo $settings['caption']; ?></figcaption>
				<?php
			endif;

			if ( $has_caption ) :
			?>
			</figure>
		<?php endif; ?>
		</div>
		<?php
	}

	/**
	 * Render image widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<# if ( '' !== settings.image.url ) {
			var image = {
		<!-- id: settings.image.id, -->
		url: settings.image,
		size: settings.image_size,
		dimension: settings.image_custom_dimension,
		model: view.getEditModel()
		};

		var image_url = settings.image;

		if ( ! image_url ) {
		return;
		}

		var link_url;

		if ( 'custom' === settings.link_to ) {
		link_url = settings.link.url;
		}

		if ( 'file' === settings.link_to ) {
		link_url = settings.image.url;
		}

		#>
		<div class="elementor-image{{ settings.shape ? ' elementor-image-shape-' + settings.shape : '' }}">
			<#
				var imgClass = '',
				hasCaption = '' !== settings.caption;

				if ( '' !== settings.hover_animation ) {
				imgClass = 'elementor-animation-' + settings.hover_animation;
				}

				if ( hasCaption ) {
				#>
				<figure class="wp-caption">
					<#
						}

						if ( link_url ) {
						#><a class="elementor-clickable" data-elementor-open-lightbox="{{ settings.open_lightbox }}"
					         href="{{ link_url }}">
							<#
								}
								#><img src="{{ image_url }}" class="{{ imgClass }}"/>
								<#

									if ( link_url ) {
									#>
						</a>
						<#
							}

							if ( hasCaption ) {
							#>
							<figcaption class="widget-image-caption wp-caption-text">{{{ settings.caption }}}
							</figcaption>
							<#
								}

								if ( hasCaption ) {
								#>
				</figure>
				<#
					}

					#>
		</div>
		<#
			} #>
		<?php
	}

	/**
	 * Retrieve image widget link URL.
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @param object $instance
	 *
	 * @return array|string|false An array/string containing the link URL, or false if no link.
	 */
	private function get_link_url( $instance ) {
		if ( 'none' === $instance['link_to'] ) {
			return false;
		}

		if ( 'custom' === $instance['link_to'] ) {
			if ( empty( $instance['link']['url'] ) ) {
				return false;
			}

			return $instance['link'];
		}

		return [
			'url' => $instance['image'],
		];
	}
}
