<?php
namespace DesignStudioElementorAddons\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;

use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Accordion Widget
 */
class Tech_Specs_Accordion extends Widget_Base {

	/**
	 * Retrieve accordion widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'tech_specs_accordion';
	}

	/**
	 * Retrieve accordion widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Tech Specs Accordion', 'ds-el' );
	}


	/**
	 * Get widget category.
	 *
	 * Retrieve video widget category.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget Category.
	 */
	public function get_categories()
	{
			return array( 'ds-el-elements' );
	}

	/**
	 * Retrieve accordion widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-accordion';
	}

	/**
	 * Register accordion widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Primary Specs', 'ds-el' ),
			]
		);

		$this->add_control(
			'primary',
			[
				'label'       => __( 'Primary Specs', 'ds-el' ),
				'type'        => Controls_Manager::REPEATER,
				'default'     => [
					[
						'primary_title'   => __( 'Seating Capacity', 'ds-el' ),
						'primary_content' => __( '6 Adults', 'ds-el' ),
					],
					[
						'primary_title'   => __( 'Dimensions', 'ds-el' ),
						'primary_content' => __( '7\'3" x 7\'3" x 36" / 220cm x 220cm x 91cm', 'ds-el' ),
					],
				],
				'fields'      => [
					[
						'name'        => 'primary_title',
						'label'       => __( 'Title', 'ds-el' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => __( 'Accordion Title', 'ds-el' ),
						'label_block' => true,
					],
					[
						'name'       => 'primary_content',
						'label'      => __( 'Value', 'ds-el' ),
						'type'       => Controls_Manager::WYSIWYG,
						'default'    => __( 'Accordion Content', 'ds-el' ),
						'show_label' => false,
					],
				],
				'title_field' => '{{{ primary_title }}}',
			]
		);

		$this->add_control(
			'view',
			[
				'label'   => __( 'View', 'ds-el' ),
				'type'    => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_additional_title',
			[
				'label' => __( 'Additional Specs', 'ds-el' ),
			]
		);

		$this->add_control(
			'additional',
			[
				'label'       => __( 'Additional Specs', 'ds-el' ),
				'type'        => Controls_Manager::REPEATER,
				'default'     => [
					[
						'additional_title'   => __( 'Lighting System', 'ds-el' ),
						'additional_content' => __( 'Luminescence® multi-color four-zone', 'ds-el' ),
					],
					[
						'additional_title'   => __( 'Water Features', 'ds-el' ),
						'additional_content' => __( 'BellaFontana® with 3 illuminated arcs of water', 'ds-el' ),
					],
				],
				'fields'      => [
					[
						'name'        => 'additional_title',
						'label'       => __( 'Title', 'ds-el' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => __( 'Accordion Title', 'ds-el' ),
						'label_block' => true,
					],
					[
						'name'       => 'additional_content',
						'label'      => __( 'Value', 'ds-el' ),
						'type'       => Controls_Manager::WYSIWYG,
						'default'    => __( 'Accordion Content', 'ds-el' ),
						'show_label' => false,
					],
				],
				'title_field' => '{{{ additional_title }}}',
			]
		);

		$this->add_control(
			'view',
			[
				'label'   => __( 'View', 'ds-el' ),
				'type'    => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Accordion', 'ds-el' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);


		$this->add_control(
			'tab_active_color',
			[
				'label'     => __( 'Active Color', 'ds-el' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-accordion .elementor-tab-title.elementor-active' => 'color: {{VALUE}};',
				],
				'scheme'    => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_4,
				],
			]
		);

		$this->add_control(
			'odd_row_bg',
			[
				'label'     => __( 'Odd Rows Background', 'ds-el' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tech-spec-item:nth-child(odd)' => 'background: {{VALUE}};',
				],

			]
		);

		$this->add_control(
			'odd_row_color',
			[
				'label'     => __( 'Odd Rows Text Color', 'ds-el' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tech-spec-item:nth-child(odd)' => 'color: {{VALUE}};',
				],

			]
		);

		$this->add_control(
			'even_row_bg',
			[
				'label'     => __( 'Even Rows Background', 'ds-el' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tech-spec-item:nth-child(even)' => 'background: {{VALUE}};',
				],

			]
		);

		$this->add_control(
			'even_row_color',
			[
				'label'     => __( 'Even Rows Text Color', 'ds-el' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tech-spec-item:nth-child(even)' => 'color: {{VALUE}};',
				],

			]
		);


		$this->add_control(
			'heading_content',
			[
				'label'     => __( 'Content', 'ds-el' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);


		$this->end_controls_section();
	}

	/**
	 * Render accordion widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings();

		$id_int = substr( $this->get_id_int(), 0, 3 );
		?>

		<div class="tech-specs-container elementor-widget-text-editor mb-3" id="accordion-primary" role="tablist">
			<div class="card">
				<div class="card-header" role="tab" id="headingOne">
					<h5 class="mb-0">
						<a data-toggle="collapse" href="#primary-collapse" aria-expanded="true"
						   aria-controls="primary-collapse">
							Primary Specs</a>
					</h5>
				</div>
				<div id="primary-collapse" class="collapse show" role="tabpanel" aria-labelledby="headingOne"
				     data-parent="#accordion-primary">
					<?php $counter = 1; ?>
					<?php foreach ( $settings['primary'] as $item ) :
						$primary_content_setting_key = $this->get_repeater_setting_key( 'primary_content', 'primary', $counter - 1 );

						$this->add_render_attribute( $primary_content_setting_key, [
							'class'           => [ 'elementor-primary-content', 'elementor-clearfix' ],
							'data-primary' => $counter,
							'role'            => 'primarypanel',
						] );

						$this->add_inline_editing_attributes( $primary_content_setting_key, 'advanced' );
						?>

						<div class="tech-spec-item d-flex justify-content-between flex-column flex-sm-row pl-4 pr-4 pt-3 pb-3">
							<strong class="tech-title"><?php echo $item['primary_title']; ?></strong>
							<div <?php echo $this->get_render_attribute_string( $primary_content_setting_key ); ?>><?php echo $this->parse_text_editor( $item['primary_content'] ); ?></div>
						</div>

						<?php
						$counter ++;
					endforeach;
					?>
				</div>
			</div>
		</div>

		<div class="tech-specs-container mb-3 elementor-widget-text-editor" id="accordion-additional" role="tablist">
			<div class="card">
				<div class="card-header" role="tab" id="headingOne">
					<h5 class="mb-0">
						<a class="collapsed" data-toggle="collapse" href="#additional-collapse" aria-expanded="true"
						   aria-controls="additional-collapse">
							Secondary Specs</a>
					</h5>
				</div>
				<div id="additional-collapse" class="collapse" role="tabpanel" aria-labelledby="headingOne"
				     data-parent="#accordion-additional">
					<?php $counter = 1; ?>
					<?php foreach ( $settings['additional'] as $item ) :
						$additional_content_setting_key = $this->get_repeater_setting_key( 'additional_content', 'additional', $counter - 1 );

						$this->add_render_attribute( $additional_content_setting_key, [
							'class'           => [ 'elementor-additional-content', 'elementor-clearfix' ],
							'data-additional' => $counter,
							'role'            => 'additionalpanel',
						] );

						$this->add_inline_editing_attributes( $additional_content_setting_key, 'advanced' );
						?>

						<div class="tech-spec-item d-flex justify-content-between flex-column flex-sm-row pl-4 pr-4 pt-3 pb-3">
							<strong class="tech-title"><?php echo $item['additional_title']; ?></strong>
							<div <?php echo $this->get_render_attribute_string( $additional_content_setting_key ); ?>><?php echo $this->parse_text_editor( $item['additional_content'] ); ?></div>
						</div>

						<?php
						$counter ++;
					endforeach;
					?>
				</div>
			</div>
		</div>


		<?php
	}

	/**
	 * Render accordion widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _content_template() {
		?>

		<div class="tech-specs-container" id="accordion-additional" role="tablist">
			<div class="card">
				<div class="card-header" role="tab" id="headingOne">
					<h5 class="mb-0">
						<a class="collapsed" data-toggle="collapse" href="#additional-collapse" aria-expanded="true"
						   aria-controls="additional-collapse">
							Secondary Specs</a>
					</h5>
				</div>
				<#
					if ( settings.additional ) {
					var counter = 1,
					tabindex = view.getIDInt().toString().substr( 0, 3 );

					_.each( settings.additional, function( item ) { #>
					<div id="additional-collapse" class="collapse" role="tabpanel" aria-labelledby="headingOne"
					     data-parent="#accordion-additional" tabindex="{{ tabindex + counter }}" data-tab="{{ counter }}">

						<div class="tech-spec-item d-flex justify-content-between pl-4 pr-4 pt-3 pb-3">
							<strong>{{{ item.additional_title }}}</strong>
							<div>{{{ item.additional_title }}}</div>
						</div>
					</div>
					<#
						counter++;
						} );
						} #>
			</div>
		</div>




		<?php
	}
}
