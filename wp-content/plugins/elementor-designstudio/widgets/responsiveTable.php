<?php
namespace DesignStudioElementorAddons\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Text_Shadow;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Accordion Widget
 */
class Responsive_Table extends Widget_Base {

	/**
	 * Retrieve accordion widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'responsiveTable';
	}

	/**
	 * Retrieve accordion widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Responsive Table', 'ds-el' );
	}


	/**
	 * Get widget category.
	 *
	 * Retrieve video widget category.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget Category.
	 */
	public function get_categories()
	{
			return array( 'ds-el-elements' );
	}

	/**
	 * Retrieve accordion widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-table';
	}

	/**
	 * Register accordion widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

    $this->start_controls_section(
      'table_general',
      [
        'label' => __( 'Table', 'ds-el' ),
      ]
    );

    $this->add_control(
      'column_num',
      [
         'label'   => __( 'Number of Columns', 'ds_el' ),
         'type'    => Controls_Manager::NUMBER,
         'default' => 3,
         'min'     => 2,
         'max'     => 4,
         'step'    => 1,
      ]
      );

    $this->end_controls_section();

		$this->start_controls_section(
			'table_header',
			[
				'label' => __( 'Table Header', 'ds-el' ),
			]
		);

    $this->add_control(
      'header_title_1',
      [
         'label'       => __( 'Header Title 1', 'ds-el' ),
         'type'        => Controls_Manager::TEXT,
         'default'     => __( '', 'ds-el' ),
         'placeholder' => __( '', 'ds-el' ),
      ]
    );

    $this->add_control(
      'header_title_2',
      [
         'label'       => __( 'Header Title 2', 'ds-el' ),
         'type'        => Controls_Manager::TEXT,
         'default'     => __( '', 'ds-el' ),
         'placeholder' => __( '', 'ds-el' ),
      ]
    );

    $this->add_control(
      'header_title_3',
      [
         'label'       => __( 'Header Title 3', 'ds-el' ),
         'type'        => Controls_Manager::TEXT,
         'default'     => __( '', 'ds-el' ),
         'placeholder' => __( '', 'ds-el' ),
      ]
    );

    $this->add_control(
      'header_title_4',
      [
         'label'       => __( 'Header Title 4', 'ds-el' ),
         'type'        => Controls_Manager::TEXT,
         'default'     => __( '', 'ds-el' ),
         'placeholder' => __( '', 'ds-el' ),
      ]
    );

    $this->add_control(
      'header_title_5',
      [
         'label'       => __( 'Header Title 5', 'ds-el' ),
         'type'        => Controls_Manager::TEXT,
         'default'     => __( '', 'ds-el' ),
         'placeholder' => __( '', 'ds-el' ),
      ]
    );



$this->end_controls_section();





$this->start_controls_section(
  'table_rows',
  [
    'label' => __( 'Rows', 'ds-el' ),
  ]
);

  $this->add_control(
    'table_row',
    [
      'label' => __( 'Row', 'ds-el' ),
      'type' => Controls_Manager::REPEATER,
      'default' => [
        [
          'table_data_1' => __( 'Row', 'ds-el' )

        ],
        [
          'table_data_2' => __( 'Row', 'ds-el' )
        ],
        [
          'table_data_3' => __( 'Row', 'ds-el' )
        ],
        [
          'table_data_4' => __( 'Row', 'ds-el' )
        ],
        [
          'table_data_5' => __( 'Row', 'ds-el' )
        ]
      ],
      'fields' => [
        [
          'name' => 'table_data_1',
          'label' => __( 'Column 1', 'ds-el' ),
          'type' => Controls_Manager::TEXT,
          'default' => __( '' , 'ds-el' ),
          'label_block' => true,
        ],
        [
          'name' => 'table_data_2',
          'label' => __( 'Column 2', 'ds-el' ),
          'type' => Controls_Manager::TEXT,
          'default' => __( '' , 'ds-el' ),
          'label_block' => true,
        ],
        [
          'name' => 'table_data_3',
          'label' => __( 'Column 3', 'ds-el' ),
          'type' => Controls_Manager::TEXT,
          'default' => __( '' , 'ds-el' ),
          'label_block' => true,
          // 'condition' => [
          //  'column_num' => '3',
          //  ],
        ],
        [
          'name' => 'table_data_4',
          'label' => __( 'Column 4', 'ds-el' ),
          'type' => Controls_Manager::TEXT,
          'default' => __( '' , 'ds-el' ),
          'label_block' => true,
        ],
        [
          'name' => 'table_data_5',
          'label' => __( 'Column 5', 'ds-el' ),
          'type' => Controls_Manager::TEXT,
          'default' => __( '' , 'ds-el' ),
          'label_block' => true,
        ],
        [
          'name' => 'subheader',
          'label' => __( 'Use row as subheader?', 'ds-el' ),
          'type' => Controls_Manager::SWITCHER,
          'default' => '',
      		'label_on' => __( 'Yes', 'ds-el' ),
      		'label_off' => __( 'No', 'ds-el' ),
          'return_value' => 'yes',
          'label_block' => true,
        ]


      ],
      // 'title_field' => '{{{ table_row }}}',
    ]
    );


$this->end_controls_section();


// Styles Tab

// Table Accordion

$this->start_controls_section(
    'section_table_style',
    [
        'label' => __('Table', 'ds-el'),
        'tab' => Controls_Manager::TAB_STYLE,
    ]
);

$this->add_control(
    'table_bg_color',
    [
        'label' => __('Table Background Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table' => 'background-color: {{VALUE}};',
        ],
    ]
);

$this->add_responsive_control(
  'table_border_radius',
  [
    'label'      => __( 'Border Radius', 'ds-el' ),
    'type'       => Controls_Manager::DIMENSIONS,
    'size_units' => [ 'px', '%' ],
    'selectors'  => [
      '{{WRAPPER}} .ds-table' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
    ],
  ]
);



$this->end_controls_section();


//HEADER Accordion
$this->start_controls_section(
    'section_header_style',
    [
        'label' => __('Header', 'ds-el'),
        'tab' => Controls_Manager::TAB_STYLE,
    ]
);

$this->add_control(
    'header_padding',
    [
        'label' => __('Header Cell Padding', 'ds-el'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px'],
        'selectors' => [
            '{{WRAPPER}} th' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
    ]
);

$this->add_control(
    'header_titles_color',
    [
        'label' => __('Header Titles Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table__header th' => 'color: {{VALUE}};',
        ],
    ]
);

$this->add_control(
    'table_header_bg_color',
    [
        'label' => __('Background Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table .ds-table__header th' => 'background-color: {{VALUE}};',
        ],
    ]
);


$this->add_group_control(
  Group_Control_Text_Shadow::get_type(),
  [
    'name' => 'header_text_shadow',
    'label' => 'Text Shadow',
    'selector' => '{{WRAPPER}} .ds-table .ds-table__header th',
  ]
);

$this->add_group_control(
    Group_Control_Typography::get_type(),
    [
        'name' => 'header_typography',
        'label' => 'Typography',
        'selector' => '{{WRAPPER}} .ds-table .ds-table__header th',
    ]
);

$this->end_controls_section();

$this->start_controls_section(
    'section_body_style',
    [
        'label' => __('Body', 'ds-el'),
        'tab' => Controls_Manager::TAB_STYLE,
    ]
);

$this->add_control(
    'cell_padding',
    [
        'label' => __('Body Cell Padding', 'ds-el'),
        'type' => Controls_Manager::DIMENSIONS,
        'size_units' => ['px'],
        'selectors' => [
            '{{WRAPPER}} .ds-table td' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
    ]
);


$this->add_control(
'table_body_color',
[
    'label' => __('Body Text Color', 'ds-el'),
    'type' => Controls_Manager::COLOR,
    'selectors' => [
        '{{WRAPPER}} .ds-table td' => 'color: {{VALUE}};',
    ],
]
);

$this->add_group_control(
  Group_Control_Text_Shadow::get_type(),
  [
    'name' => 'body_text_shadow',
    'label' => 'Body Text Shadow',
    'selector' => '{{WRAPPER}} .ds-table td',
  ]
);

$this->add_group_control(
    Group_Control_Typography::get_type(),
    [
        'name' => 'body_typography',
        'label' => 'Body Typography',
        'selector' => '{{WRAPPER}} .ds-table td',
    ]
);


$this->add_control(
    'even_row_color',
    [
        'label' => __('Even Rows Text Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table tr::nth-child(even)::not(.row-subheader)' => 'color: {{VALUE}};',
        ],
    ]
);

$this->add_control(
    'even_row_bg_color',
    [
        'label' => __('Even Rows Background Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table tr::nth-child(even)::not(.row-subheader)' => 'background-color: {{VALUE}};',
        ],
    ]
);

$this->add_control(
    'odd_row_color',
    [
        'label' => __('Odd Rows Text Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table tr::nth-child(odd)::not(.row-subheader)' => 'color: {{VALUE}};',
        ],
    ]
);


$this->add_control(
    'odd_row_bg_color',
    [
        'label' => __('Odd Rows Background Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table tr::nth-child(odd)::not(.row-subheader)' => 'background-color: {{VALUE}};',
        ],
    ]
);

$this->add_control(
    'first_col_bg_color',
    [
        'label' => __('First Column Background Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table td::nth-child(1)' => 'background-color: {{VALUE}};',
        ],
    ]
);




// Subheading

$this->add_control(
    'subheading_color',
    [
        'label' => __('Sub-header Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table .row-subheader td, {{WRAPPER}} .ds-table .row-subheader td:before' => 'color: {{VALUE}} !important;',
        ],
    ]
);

$this->add_control(
    'subheading_bg_color',
    [
        'label' => __('Sub-header Background Color', 'ds-el'),
        'type' => Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .ds-table .row-subheader td' => 'background-color: {{VALUE}} !important;',
        ],
    ]
);

$this->add_group_control(
  Group_Control_Text_Shadow::get_type(),
  [
    'name' => 'subheading_text_shadow',
    'label' => 'Subheading Text Shadow',
    'selector' => '{{WRAPPER}} .ds-table .row-subheader td',
  ]
);

$this->add_group_control(
    Group_Control_Typography::get_type(),
    [
        'name' => 'subheading_typography',
        'label' => 'Subheading Typography',
        'selector' => '{{WRAPPER}} .ds-table .row-subheader td',
    ]
);


$this->end_controls_section();


	}

	/**
	 * Render accordion widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings();
		$id_int = substr( $this->get_id_int(), 0, 3 );
		?>



<style media="screen">
.ds-table {
/* margin: 1em 0; */
width: 100%;
}

@media (min-width: 480px) {
  .ds-table.num_cols_2 tr td,
  .ds-table.num_cols_2 tr th {
    width: 50%;
  }

  .ds-table.num_cols_3 tr td,
  .ds-table.num_cols_2 tr th {
    width: 33.333%;
  }

  .ds-table.num_cols_4 tr td,
  .ds-table.num_cols_2 tr th {
    width: 25%;
  }

  .ds-table.num_cols_5 tr td,
  .ds-table.num_cols_2 tr th {
    width: 20%;
  }
}


.ds-table tr {
/* border-top: 1px solid #ddd; */
/* border-bottom: 1px solid #ddd; */
}
.ds-table th {
display: none;
}
.ds-table td {
display: block;
}
.ds-table td:first-child {
padding-top: .5em;
}
.ds-table td:last-child {
padding-bottom: .5em;
}
.ds-table td:before {
/* content: attr(data-th) ": "; */
content: attr(data-th) "";
font-weight: bold;
/* width: 6.5em; */
/* display: inline-block; */
display: block;
}


@media (min-width: 480px) {
.ds-table td:before {
  display: none;
}
}
.ds-table th, .ds-table td {
text-align: left;
}
@media (min-width: 480px) {
.ds-table th, .ds-table td {
  display: table-cell;
  /* padding: .25em .5em; */
  /* width: 33.33%; */
  text-align: right;
}
.ds-table th:first-child, .ds-table td:first-child {
  padding-left: 0;
}
.ds-table th:last-child, .ds-table td:last-child {
  padding-right: 0;
}
}



.ds-table {
overflow: hidden;
}
.ds-table tr {
border-color: #666;
}
.ds-table th, .ds-table td {
/* margin: .5em 1em; */
}
@media (min-width: 480px) {
.ds-table th, .ds-table td {
  /* padding: .25em 1em !important; */
}
}
.ds-table th, .ds-table td:before {
/* color: #666; */
}

/* First column */
.ds-table td:first-child {
  background: #f26e4b;
  color: #fff;
}

</style>
<?php $column_num = $settings['column_num']; ?>
<table class="ds-table num_cols_<?php echo $column_num ?>">

  <!-- Table Header  -->
  <tr class="ds-table__header">
    <?php


      echo '<th>' . $settings['header_title_1'] . '</th>';

      echo '<th>' . $settings['header_title_2'] . '</th>';

      if($settings['column_num'] >= 3):
        echo '<th>' . $settings['header_title_3'] . '</th>';
      endif;

      if($settings['column_num'] >= 4):
        echo '<th>' . $settings['header_title_4'] . '</th>';
      endif;

      ?>
  </tr>

  <!-- Table Rows -->

  <?php
  $rows = $this->get_settings('table_row');
  foreach($rows as $row):
    $subheader = $row['subheader'];
    if( $subheader == 'yes' ): $subheader = ' row-subheader'; else: $subheader = NULL; endif;
    echo '<tr class="ds-table__body' . $subheader . '">';
        echo '<td data-th="'.$settings['header_title_1'].'">'; echo $row['table_data_1']; echo '</td>';
        echo '<td data-th="'.$settings['header_title_2'].'">'; echo $row['table_data_2']; echo '</td>';

    if($settings['column_num'] >= 3):
        echo '<td data-th="'.$settings['header_title_3'].'">'; echo $row['table_data_3']; echo '</td>';
      endif;

    if($settings['column_num'] >= 4):
      echo '<td data-th="'.$settings['header_title_4'].'">'; echo $row['table_data_4']; echo '</td>';
    endif;

    if($settings['column_num'] >= 5):
      echo '<td data-th="'.$settings['header_title_5'].'">'; echo $row['table_data_5']; echo '</td>';
    endif;

    echo '</tr>';

  endforeach;
  ?>


</table>


		<?php
	}

	/**
	 * Render accordion widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _content_template() {
		?>



  		<!-- <div class="ds-tables elementor-row">
  		<# _.each( settings.tables, function( table ) { #>

  			<div class="ds-table__wrapper text-center elementor-column ds-table--col-{{ settings.per_line }}">
  				<div class="ds-table">
  					<div class="ds-table__face ds-table__face-default" style="background-image:url({{ table.table_bg_image }});background-color:{{ table.table_bg_color }};">
  						<# if ( table.icon_url ) { #> <img class="ds-table__icon" src="{{ table.icon_url}}" alt=""> <# } #>
  						<{{ settings.title_tag }} class="ds-table__title">{{ table.table_title }}</{{ settings.title_tag }}>
  					</div>
  					<div class="ds-table__face ds-table__face-hover">
  						<span class="ds-table__content">{{ table.table_content }}</span>
  						<a class="btn ds-table-btn" href="{{ table.table_cta_url }}">{{ table.table_cta_text }}</a>
  					</div>
  				</div>
  			</div>

  		<# }); #>
  		</div> -->




		<?php
	}
}
