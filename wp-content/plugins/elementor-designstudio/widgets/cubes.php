<?php
namespace DesignStudioElementorAddons\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;

use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * @since 1.1.0
 */
class Cubes extends Widget_Base {

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.1.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'cubes';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.1.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Cubes', 'ds-el' );
	}


	/**
	 * Get widget category.
	 *
	 * Retrieve video widget category.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget Category.
	 */
	public function get_categories()
	{
			return array( 'ds-el-elements' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.1.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-gallery-grid';
	}




	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	public function get_script_depends() {
		return [ 'cubes' ];
		// return [ 'modernizr' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.1.0
	 *
	 * @access protected
	 */
	protected function _register_controls() {





		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ds-el' ),
			]
		);




		$this->add_control(
				'per_line',
				[
						'label' => __('Columns per row', 'ds-el'),
						'type' => Controls_Manager::NUMBER,
						'min' => 1,
						'max' => 6,
						'step' => 1,
						'default' => 3,
				]
		);


		$this->add_control(
				'cubes',
				[
						'label' => __('Cubes', 'ds-el'),
						'type' => Controls_Manager::REPEATER,
						'separator' => 'before',
						'default' => [
								[
										'cube_title' => __('Cube 1', 'ds-el'),
										'cube_content' => __('Content Here', 'ds-el'),
								],
								[
										'cube_title' => __('Cube 2', 'ds-el'),
										'cube_content' => __('Content Here', 'ds-el'),
								],
								[
										'cube_title' => __('Cube 3', 'ds-el'),
										'cube_content' => __('Content Here', 'ds-el'),
								],
						],
						'fields' => [
								[
										'name' => 'cube_title',
										'label' => __('Title', 'ds-el'),
										'type' => Controls_Manager::TEXT,
								],

								 [
									 	'name'				=> 'icon_url',
										'label'       => __( 'Image URL', 'ds-el' ),
										'type'        => Controls_Manager::TEXT,
										// 'default'     => __( '/wp-content/plugins/elementor/assets/images/placeholder.png', 'ds-el' ),
										'placeholder' => __( 'http://imagepathhere.com', 'ds-el' ),
								 ],
								[
										'name' => 'cube_content',
										'label' => __('Cube Content', 'ds-el'),
										'type' => Controls_Manager::TEXTAREA,
										'default' => __('Cube Content', 'ds-el'),
										'description' => __('Keep this short.', 'ds-el'),
										'label_block' => true,
								],
								[
										'name' => 'cube_cta_text',
										'label' => __('Cube CTA Text', 'ds-el'),
										'type' => Controls_Manager::TEXT,
										'default' => __('', 'ds-el'),
										'label_block' => true,
								],
								[
										'name' => 'cube_cta_url',
										'label' => __('Cube CTA URL', 'ds-el'),
										'type' => Controls_Manager::TEXT,
										'default' => __('', 'ds-el'),
										'label_block' => true,
								],


						    [
										'name'	=> 'cube_bg_options',
						        'label' => __( 'Background Options', 'ds-el' ),
						        'type' => Controls_Manager::CHOOSE,
						        'options' => [
						            'image'    => [
						                'title' => __( 'Image', 'ds-el' ),
						                'icon' => 'fa fa-picture-o',
						            ],
						            'classic' => [
						                'title' => __( 'Classic', 'ds-el' ),
						                'icon' => 'fa fa-paint-brush',
						            ],
						            'gradient' => [
						                'title' => __( 'Gradient', 'ds-el' ),
						                'icon' => 'fa fa-barcode',
						            ],
						        ],
						    ],

							  [
									 'name'				=> 'cube_bg_image',
							     'label'       => __( 'BG Image URL', 'ds-el' ),
							     'type'        => Controls_Manager::TEXT,
							     'placeholder' => __( 'URL', 'ds-el' ),
									 'condition' => [
			 							'cube_bg_options' => 'image',
			 						],

							  ],

								[
									'name'				=> 'cube_bg_color',
									'label' => __( 'Color', 'ds-el' ),
					        'type' => Controls_Manager::COLOR,
									'condition' => [
									 'cube_bg_options' => 'classic',
								 ],
							  ],


						],
						'title_field' => '{{{ cube_title }}}',
				]
		);

		$this->end_controls_section();

		$this->start_controls_section(
				'section_cube_styles',
				[
						'label' => __('Cube Styles', 'ds-el'),
						'tab' => Controls_Manager::TAB_STYLE,
				]
		);

		$this->add_responsive_control(
				'cube_min_height',
				[
						'label' => __('Min-Height (px)', 'ds-el'),
						'type' => Controls_Manager::SLIDER,
						'default' => [
								'size' => 50,
						],
						'range' => [
								'px' => [
										'max' => 1000,
										'min' => 50,
										'step' => 10,
								],
						],
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face' => 'min-height: {{SIZE}}px;',
						],
				]
		);

		$this->add_control(
				'cube_gutters',
				[
						'label' => __('Cube Gutters', 'ds-el'),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => ['px', '%'],
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube__wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
				]
		);

		$this->add_control(
				'cube_padding',
				[
						'label' => __('Cube Padding', 'ds-el'),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => ['px', '%'],
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube__wrapper .ds-cube__face' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
				]
		);

		$this->add_control(
				'cube-default-bg',
				[
						'label' => __('Front Face Background Color', 'ds-el'),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-default' => 'background-color: {{VALUE}};',
						],
				]
		);

		$this->add_control(
				'cube-hover-bg',
				[
						'label' => __('Back Face Background Color', 'ds-el'),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-hover' => 'background-color: {{VALUE}};',
						],
				]
		);

		$this->add_control(
  'cube-content-vert-pos',
  [
     'label'       => __( 'Content Vertical Position', 'ds-el' ),
     'type' => Controls_Manager::SELECT,
     'default' => 'center',
     'options' => [
     	'center'  => __( 'Middle', 'ds-el' ),
     	'flex-start' => __( 'Top', 'ds-el' ),
     	'flex-end' => __( 'Bottom', 'ds-el' ),
     ],
	 'selectors' => [ // You can use the selected value in an auto-generated css rule.
	    '{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face' => 'justify-content: {{VALUE}}',
	 ],
  ]
);



		$this->end_controls_section();


		$this->start_controls_section(
				'cubes_title',
				[
						'label' => __('Cube Title', 'ds-el'),
						'tab' => Controls_Manager::TAB_STYLE,
				]
		);

		$this->add_control(
		  'title_position',
		  [
		     'label'       => __( 'Title Position', 'ds-el' ),
		     'type' => Controls_Manager::SELECT,
		     'default' => '2',
		     'options' => [
		     	'2'  => __( 'Bottom', 'ds-el' ),
		     	'1' => __( 'Top', 'ds-el' ),
		     ],
			 'selectors' => [ // You can use the selected value in an auto-generated css rule.
			    '{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title' => 'order: {{VALUE}}',
			 ],
		  ]
		);

		$this->add_control(
				'title_tag',
				[
						'label' => __('Title HTML Tag', 'ds-el'),
						'type' => Controls_Manager::SELECT,
						'options' => [
								'h1' => __('H1', 'ds-el'),
								'h2' => __('H2', 'ds-el'),
								'h3' => __('H3', 'ds-el'),
								'h4' => __('H4', 'ds-el'),
								'h5' => __('H5', 'ds-el'),
								'h6' => __('H6', 'ds-el'),
								'div' => __('div', 'ds-el'),
						],
						'default' => 'h3',
				]
		);


		$this->add_control(
				'title_color',
				[
						'label' => __('Title Color', 'ds-el'),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title' => 'color: {{VALUE}};',
						],
				]
		);

		$this->add_control(
				'title_background',
				[
						'label' => __('Title Background Color', 'ds-el'),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title' => 'background: {{VALUE}};',
						],
				]
		);

		$this->add_control(
				'title_padding',
				[
						'label' => __('Title Padding', 'ds-el'),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => ['px', '%'],
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube__wrapper .ds-cube__face .ds-cube__title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
				]
		);

		$this->add_control(
				'title_margin',
				[
						'label' => __('Title Margins', 'ds-el'),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => ['px', '%'],
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube__wrapper .ds-cube__face .ds-cube__title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
				]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'title_text_shadow',
				'selector' => '{{WRAPPER}} .ds-cube__title',
			]
		);

		$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
						'name' => 'title_typography',
						'selector' => '{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__title',
				]
		);

		$this->end_controls_section();

		$this->start_controls_section(
				'cubes_content',
				[
						'label' => __('Cube Content', 'ds-el'),
						'tab' => Controls_Manager::TAB_STYLE,
				]
		);

		$this->add_control(
				'content_color',
				[
						'label' => __('Content Color', 'ds-el'),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
								'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-hover' => 'color: {{VALUE}};',
						],
				]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'content_text_shadow',
				'selector' => '{{WRAPPER}} .ds-cube__face-hover .ds-cube__content',
			]
		);

		$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
						'name' => 'content_typography',
						'selector' => '{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__face-hover',
				]
		);

		$this->end_controls_section();


		$this->start_controls_section(
				'icon_styles',
				[
						'label' => __('Icon Styles', 'ds-el'),
						'tab' => Controls_Manager::TAB_STYLE,
				]
		);



$this->add_control(
    'icon_width',
    [
        'label' => __( 'Image Width', 'ds-el' ),
        'type' => Controls_Manager::SLIDER,
        'default' => [
            'size' => 150,
        ],
        'range' => [
            'px' => [
                'min' => 0,
                'max' => 1000,
                'step' => 5,
            ],
            '%' => [
                'min' => 0,
                'max' => 100,
            ],
        ],
        'size_units' => [ 'px', '%' ],
        'selectors' => [
            '{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__icon' => 'width: {{SIZE}}{{UNIT}};',
        ],
    ]
);


$this->add_control(
  'icon_margin',
  [
     'label' => __( 'Image Margin', 'ds-el' ),
     'type' => Controls_Manager::DIMENSIONS,
	 'size_units' => [ 'px', '%', 'em' ],
	 'selectors' => [
	 		'{{WRAPPER}} .ds-cubes .ds-cube .ds-cube__icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	 ],
  ]
);


		$this->end_controls_section();


	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.1.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings();
		$cubes = $this->get_settings( 'cubes' );
		$per_line = intval($settings['per_line']);



		?>

	<?php if($cubes) { ?>
		<div class="ds-cubes elementor-row">
		  <?php foreach ($cubes as $cube): ?>

		<div class="ds-cube__wrapper text-center elementor-column ds-cube--col-<?php echo $per_line ?>">
			<div class="ds-cube">
				<div class="ds-cube__face ds-cube__face-default" style="background-image:url(<?php echo $cube['cube_bg_image'] ?>);background-color: <?php echo $cube['cube_bg_color'] ?>;">
					<?php if ( isset($cube['icon_url'] )): ?><img class="ds-cube__icon" src="<?php echo $cube['icon_url'] ?>" alt=""><?php endif; ?>
					<<?php echo $settings['title_tag']; ?> class="ds-cube__title"><?php echo $cube['cube_title']; ?></<?php echo $settings['title_tag']; ?>>
				</div>
				<div class="ds-cube__face ds-cube__face-hover">
					<span class="ds-cube__content"><?php echo $cube['cube_content']; ?></span>
					<a class="btn ds-cube-btn" href="<?php echo $cube['cube_cta_url']; ?>"><?php echo $cube['cube_cta_text']; ?></a>
				</div>
			</div>
		</div>

		<?php
		endforeach;
		?>
		</div>
		<?php
	}
		?>



<style media="screen">

/*TEMPORARY STYLES*/
		@media (max-width: 767px) {
			.cube--col-4,
			.cube--col-3,
			.cube--col-2,
			.cube--col-1 {
				width: 100%;
			}
		}

		@media (min-width: 768px) and (max-width: 1024px) {
				.cube--col-4,
				.cube--col-2, {
					width: 50%;
				}
		}

		@media (min-width:1025px) {

			.ds-cube--col-4 {
					width: 25%;
				}
			.ds-cube--col-3 {
					width: 33.33333%;
				}
			.ds-cube--col-2 {
					width: 50%;
				}
			.ds-cube--col-1 {
					width: 100%;
				}

		}

		.ds-cubes {
			display: flex;
			flex-wrap: wrap;
			align-items: center;
			padding-left: 0px;
			padding-right: 0px;
		}
		.ds-cube__wrapper {
			display: flex;
			align-items: center;
			justify-content: center;
			flex-direction: column;
			padding: 0;
		}
		.ds-cube {
			backface-visibility: hidden;
			position: relative;
			width: 100%;
		}

		.ds-cube__title {
			display: block;
			width: 100%;
			text-align: center;
			margin-bottom: 0;
		}

		.ds-cube__content {
			margin: 0;
		}

		.ds-cube__icon {
			order: 2;
		}

		.ds-cube__face {
			background: #e1e1e1;
		}

		.ds-cube__face-default {
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
			width: 100%;
			background-size: cover;
			background-position: center;
			background-repeat: no-repeat;
		}

		.ds-cube__face-hover {
			display: flex;
			align-items: center;
			flex-direction: column;
			justify-content: center;
			height: 100%;
			width: 100%;
			left: 0;
			position: absolute;
			top: 0;
			backface-visibility: hidden;
			-webkit-transform: translateX(100%) rotateY(90deg);
			-moz-transform: translateX(100%) rotateY(90deg);
			-ms-transform: translateX(100%) rotateY(90deg);
			-o-transform: translateX(100%) rotateY(90deg);
			transform: translateX(100%) rotateY(90deg);
			-webkit-transform-origin: 0% 50% 0px;
			-moz-transform-origin: 0% 50% 0px;
			-ms-transform-origin: 0% 50% 0px;
			-o-transform-origin: 0% 50% 0px;
			transform-origin: 0% 50% 0px;
		}
</style>


		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.1.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
		?>


	<# if ( settings.cubes ) { #>
		<div class="ds-cubes elementor-row">
		<# _.each( settings.cubes, function( cube ) { #>

			<div class="ds-cube__wrapper text-center elementor-column ds-cube--col-{{ settings.per_line }}">
				<div class="ds-cube">
					<div class="ds-cube__face ds-cube__face-default" style="background-image:url({{ cube.cube_bg_image }});background-color:{{ cube.cube_bg_color }};">
						<# if ( cube.icon_url ) { #> <img class="ds-cube__icon" src="{{ cube.icon_url}}" alt=""> <# } #>
						<{{ settings.title_tag }} class="ds-cube__title">{{ cube.cube_title }}</{{ settings.title_tag }}>
					</div>
					<div class="ds-cube__face ds-cube__face-hover">
						<span class="ds-cube__content">{{ cube.cube_content }}</span>
						<a class="btn ds-cube-btn" href="{{ cube.cube_cta_url }}">{{ cube.cube_cta_text }}</a>
					</div>
				</div>
			</div>

		<# }); #>
		</div>
	<# } #>


	<style media="screen">

	/*TEMPORARY STYLES*/
			@media (max-width: 767px) {
				.cube--col-4,
				.cube--col-3,
				.cube--col-2,
				.cube--col-1 {
					width: 100%;
				}
			}

			@media (min-width: 768px) and (max-width: 1024px) {
					.cube--col-4,
					.cube--col-2, {
						width: 50%;
					}
			}

			@media (min-width:1025px) {

				.ds-cube--col-4 {
						width: 25%;
					}
				.ds-cube--col-3 {
						width: 33.33333%;
					}
				.ds-cube--col-2 {
						width: 50%;
					}
				.ds-cube--col-1 {
						width: 100%;
					}

			}

			.ds-cubes {
				display: flex;
				flex-wrap: wrap;
				align-items: center;
				padding-left: 0px;
				padding-right: 0px;
			}
			.ds-cube__wrapper {
				display: flex;
				align-items: center;
				justify-content: center;
				flex-direction: column;
				padding: 0;
			}
			.ds-cube {
				backface-visibility: hidden;
				position: relative;
				width: 100%;
			}

			.ds-cube__title {
				display: block;
				width: 100%;
				text-align: center;
				margin-bottom: 0;
			}

			.ds-cube__content {
				margin: 0;
			}

			.ds-cube__icon {
				order: 2;
			}

			.ds-cube__face {
				background: #e1e1e1;
			}

			.ds-cube__face-default {
				display: flex;
				flex-direction: column;
				align-items: center;
				justify-content: center;
				width: 100%;
				background-size: cover;
				background-position: center;
				background-repeat: no-repeat;
			}

			.ds-cube__face-hover {
				display: flex;
				align-items: center;
				flex-direction: column;
				justify-content: center;
				height: 100%;
				width: 100%;
				left: 0;
				position: absolute;
				top: 0;
				backface-visibility: hidden;
				-webkit-transform: translateX(100%) rotateY(90deg);
				-moz-transform: translateX(100%) rotateY(90deg);
				-ms-transform: translateX(100%) rotateY(90deg);
				-o-transform: translateX(100%) rotateY(90deg);
				transform: translateX(100%) rotateY(90deg);
				-webkit-transform-origin: 0% 50% 0px;
				-moz-transform-origin: 0% 50% 0px;
				-ms-transform-origin: 0% 50% 0px;
				-o-transform-origin: 0% 50% 0px;
				transform-origin: 0% 50% 0px;
			}
	</style>


		<?php


	}
}
