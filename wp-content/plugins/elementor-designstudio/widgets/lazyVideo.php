<?php
namespace DesignStudioElementorAddons\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Embed;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor video widget.
 *
 * Elementor widget that displays a video player.
 *
 * @since 1.0.0
 */
class LazyVideo extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve video widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'LazyVideo';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve video widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'LazyVideo', 'ds-el' );
	}

	/**
	 * Get widget category.
	 *
	 * Retrieve video widget category.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget Category.
	 */
	public function get_categories()
	{
			return array( 'ds-el-elements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve video widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-youtube';
	}

	/**
	 * Register video widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_video',
			[
				'label' => __( 'Video', 'ds-el' ),
			]
		);

		$this->add_control(
			'video_type',
			[
				'label' => __( 'Video Type', 'ds-el' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'youtube',
				'options' => [
					'youtube' => __( 'YouTube', 'ds-el' ),
					'vimeo' => __( 'Vimeo', 'ds-el' ),
				],
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'ds-el' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your YouTube link', 'ds-el' ),
				'default' => 'https://www.youtube.com/watch?v=9uOETcuFjbE',
				'label_block' => true,
				'condition' => [
					'video_type' => 'youtube',
				],
			]
		);

		$this->add_control(
			'vimeo_link',
			[
				'label' => __( 'Link', 'ds-el' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your Vimeo link', 'ds-el' ),
				'default' => 'https://vimeo.com/235215203',
				'label_block' => true,
				'condition' => [
					'video_type' => 'vimeo',
				],
			]
		);

		$this->add_control(
			'hosted_link',
			[
				'label' => __( 'Link', 'ds-el' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your video link', 'ds-el' ),
				'default' => '',
				'label_block' => true,
				'condition' => [
					'video_type' => 'hosted',
				],
			]
		);

		$this->add_control(
			'heading_youtube',
			[
				'label' => __( 'Video Options', 'ds-el' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		// YouTube.
		$this->add_control(
			'yt_autoplay',
			[
				'label' => __( 'Autoplay', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'condition' => [
					'video_type' => 'youtube',
				],
			]
		);

		$this->add_control(
			'yt_rel',
			[
				'label' => __( 'Suggested Videos', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'Hide', 'ds-el' ),
				'label_on' => __( 'Show', 'ds-el' ),
				'condition' => [
					'video_type' => 'youtube',
				],
			]
		);

		$this->add_control(
			'yt_controls',
			[
				'label' => __( 'Player Control', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'Hide', 'ds-el' ),
				'label_on' => __( 'Show', 'ds-el' ),
				'default' => 'yes',
				'condition' => [
					'video_type' => 'youtube',
				],
			]
		);

		$this->add_control(
			'yt_showinfo',
			[
				'label' => __( 'Player Title & Actions', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'Hide', 'ds-el' ),
				'label_on' => __( 'Show', 'ds-el' ),
				'default' => 'yes',
				'condition' => [
					'video_type' => 'youtube',
				],
			]
		);

		$this->add_control(
			'yt_mute',
			[
				'label' => __( 'Mute', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'condition' => [
					'video_type' => 'youtube',
				],
			]
		);

		$this->add_control(
			'yt_privacy',
			[
				'label' => __( 'Privacy Mode', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'description' => __( 'When you turn on privacy mode, YouTube won\'t store information about visitors on your website unless they play the video.', 'ds-el' ),
				'condition' => [
					'video_type' => 'youtube',
				],
			]
		);

		// Vimeo.
		$this->add_control(
			'vimeo_autoplay',
			[
				'label' => __( 'Autoplay', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'condition' => [
					'video_type' => 'vimeo',
				],
			]
		);

		$this->add_control(
			'vimeo_loop',
			[
				'label' => __( 'Loop', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'condition' => [
					'video_type' => 'vimeo',
				],
			]
		);

		$this->add_control(
			'vimeo_title',
			[
				'label' => __( 'Intro Title', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'Hide', 'ds-el' ),
				'label_on' => __( 'Show', 'ds-el' ),
				'default' => 'yes',
				'condition' => [
					'video_type' => 'vimeo',
				],
			]
		);

		$this->add_control(
			'vimeo_portrait',
			[
				'label' => __( 'Intro Portrait', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'Hide', 'ds-el' ),
				'label_on' => __( 'Show', 'ds-el' ),
				'default' => 'yes',
				'condition' => [
					'video_type' => 'vimeo',
				],
			]
		);

		$this->add_control(
			'vimeo_byline',
			[
				'label' => __( 'Intro Byline', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'Hide', 'ds-el' ),
				'label_on' => __( 'Show', 'ds-el' ),
				'default' => 'yes',
				'condition' => [
					'video_type' => 'vimeo',
				],
			]
		);

		$this->add_control(
			'vimeo_color',
			[
				'label' => __( 'Controls Color', 'ds-el' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'video_type' => 'vimeo',
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'ds-el' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'youtube',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_image_overlay',
			[
				'label' => __( 'Image Overlay', 'ds-el' ),
			]
		);

		$this->add_control(
			'show_image_overlay',
			[
				'label' => __( 'Image Overlay', 'ds-el' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => __( 'Hide', 'ds-el' ),
				'label_on' => __( 'Show', 'ds-el' ),
			]
		);

		$this->add_control(
			'image_overlay',
			[
				'label' => __( 'Image', 'ds-el' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'condition' => [
					'show_image_overlay' => 'yes',
				],
			]
		);

		$this->add_control(
			'show_play_icon',
			[
				'label' => __( 'Play Icon', 'ds-el' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'ds-el' ),
					'no' => __( 'No', 'ds-el' ),
				],
				'condition' => [
					// 'show_image_overlay' => 'yes',
					// 'image_overlay[url]!' => '',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_video_style',
			[
				'label' => __( 'Video', 'ds-el' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'aspect_ratio',
			[
				'label' => __( 'Aspect Ratio', 'ds-el' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'169' => '16:9',
					'43' => '4:3',
					'32' => '3:2',
				],
				'default' => '169',
				'prefix_class' => 'elementor-aspect-ratio-',
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'play_icon_title',
			[
				'label' => __( 'Play Icon', 'ds-el' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					// 'show_image_overlay' => 'yes',
				],
			]
		);

		$this->add_control(
			'play_icon_color',
			[
				'label' => __( 'Color', 'ds-el' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-custom-embed-play i' => 'color: {{VALUE}}',
				],
				'separator' => 'before',
				'condition' => [
					// 'show_image_overlay' => 'yes',
				],
			]
		);

		$this->add_responsive_control(
			'play_icon_size',
			[
				'label' => __( 'Size', 'ds-el' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 300,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-custom-embed-play i' => 'font-size: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					// 'show_image_overlay' => 'yes',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'play_icon_text_shadow',
				'selector' => '{{WRAPPER}} .elementor-custom-embed-play i',
				'fields_options' => [
					'text_shadow_type' => [
						'label' => _x( 'Shadow', 'Text Shadow Control', 'ds-el' ),
					],
				],
				'condition' => [
					// 'show_image_overlay' => 'yes',
				],
			]
		);

		$this->end_controls_section();

	}



		/**
		 * Extract the youTube Video ID.
		 *
		 * Written in PHP and used to generate the final HTML.
		 *
		 * @since 1.0.0
		 * @access protected
		 */
		protected function getYouTubeIdFromURL($url)
			{
				if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
					$video_id = $match[1];
				}
				return $video_id;
			}



	/**
	 * Render video widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {





		$settings = $this->get_active_settings();

		$video_link = 'youtube' === $settings['video_type'] ? $settings['link'] : $settings['vimeo_link'];

		if ( empty( $video_link ) ) {
			return;
		}

		$embed_params = $this->get_embed_params();

		$embed_options = [
			'privacy' => $settings['yt_privacy'],
		];

		$video_html = Embed::get_embed_html( $video_link, $embed_params, $embed_options );

		if ( empty( $video_html ) ) {
			echo esc_url( $video_link );
			return;
		}

		$this->add_render_attribute( 'video-wrapper', 'class', 'ds-el-wrapper elementor-wrapper' );

		// if ( ! $settings['lightbox'] ) {
		// 	$this->add_render_attribute( 'video-wrapper', 'class', 'ds-el-fit-aspect-ratio elementor-fit-aspect-ratio' );
		// }

		$this->add_render_attribute( 'video-wrapper', 'class', 'ds-el-open- .elementor-open' );
		?>
		<!-- We need to move these to a real css file or inject them into the stylesheet, elementor style -->
		<style>
		.youtube {
			margin-bottom: 30px;
			position: relative;
			padding-top: 56.25%;
			overflow: hidden;
			cursor: pointer;
			background-repeat: no-repeat;
			background-size: cover;
			background-position: 50% 50%;
		}
		.youtube img {
			width: 100%;
			top: -16.82%;
			left: 0;
			/* opacity: 0.7; */
		}

		.youtube img {
			cursor: pointer;
			opacity: 0;
		}

		.youtube.ds-el-default-cover img {
			opacity: 1;
		}

		.youtube img,
		.youtube iframe{
			position: absolute;
		}

		.youtube iframe {
			height: 100%;
			width: 100%;
			top: 0;
			left: 0;
			z-index: 2;
		}

		.ds-el-custom-embed-play {
			z-index: 2;
		}
		</style>

		<div <?php echo $this->get_render_attribute_string( 'video-wrapper' ); ?>>
			<?php

			if ( $this->has_image_overlay() ) {
				$this->add_render_attribute( 'image-overlay', 'class', 'ds-el-custom-embed-image-overlay youtube' );



					$this->add_render_attribute( 'image-overlay', 'style', 'background-image: url(' . $settings['image_overlay']['url'] . ');' );


				?>
				<div <?php echo $this->get_render_attribute_string( 'image-overlay' ) . 'data-url="' . $this->getYouTubeIdFromURL($video_link) . '"' ?>>
					<?php

						 ?>
						<img src="<?php echo $settings['image_overlay']['url']; ?>">
					<?php // endif; ?>
					<?php if ( 'yes' === $settings['show_play_icon'] ) : ?>
						<div class="ds-el-custom-embed-play elementor-custom-embed-play">
							<i class="eicon-play" aria-hidden="true"></i>
						</div>
					<?php endif; ?>
				</div>

			<?php } else {
        // use the default youtube cover.

				$this->add_render_attribute( 'image-overlay', 'class', 'ds-el-custom-embed-image-overlay ds-el-default-cover youtube' );

				?>

				<div <?php echo $this->get_render_attribute_string( 'image-overlay' ) . 'data-cover="false" data-url="' . $this->getYouTubeIdFromURL($video_link) . '"' ?>>

					<?php if ( 'yes' === $settings['show_play_icon'] ) : ?>
						<div class="ds-el-custom-embed-play elementor-custom-embed-play">
							<i class="eicon-play" aria-hidden="true"></i>
						</div>
					<?php endif; ?>

				</div>
<?php
			} ?>
		</div>
	<?php
	}

	/**
	 * Render video widget as plain content.
	 *
	 * Override the default behavior, by printing the video URL insted of rendering it.
	 *
	 * @since 1.4.5
	 * @access public
	 */
	public function render_plain_content() {
		$settings = $this->get_active_settings();
		$url = 'youtube' === $settings['video_type'] ? $settings['link'] : $settings['vimeo_link'];

		echo esc_url( $url );
	}

	/**
	 * Retrieve video widget embed parameters.
	 *
	 * @since 1.5.0
	 * @access public
	 *
	 * @return array Video embed parameters.
	 */
	public function get_embed_params() {
		$settings = $this->get_settings();

		$params = [];

		if ( 'youtube' === $settings['video_type'] ) {
			$youtube_options = [ 'autoplay', 'rel', 'controls', 'showinfo', 'mute' ];

			foreach ( $youtube_options as $option ) {
				if ( 'autoplay' === $option && $this->has_image_overlay() ) {
					continue;
				}

				$value = ( 'yes' === $settings[ 'yt_' . $option ] ) ? '1' : '0';
				$params[ $option ] = $value;
			}

			$params['wmode'] = 'opaque';
		}

		if ( 'vimeo' === $settings['video_type'] ) {
			$vimeo_options = [ 'autoplay', 'loop', 'title', 'portrait', 'byline' ];

			foreach ( $vimeo_options as $option ) {
				if ( 'autoplay' === $option && $this->has_image_overlay() ) {
					continue;
				}

				$value = ( 'yes' === $settings[ 'vimeo_' . $option ] ) ? '1' : '0';
				$params[ $option ] = $value;
			}

			$params['color'] = str_replace( '#', '', $settings['vimeo_color'] );
		}

		return $params;
	}

	/**
	 * Retrieve video widget hosted parameters.
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @return array Video hosted parameters.
	 */
	protected function get_hosted_params() {
		$settings = $this->get_settings();

		$params = [];

		$params['src'] = $settings['hosted_link'];

		$hosted_options = [ 'autoplay', 'loop' ];

		foreach ( $hosted_options as $key => $option ) {
			$value = ( 'yes' === $settings[ 'hosted_' . $option ] ) ? '1' : '0';
			$params[ $option ] = $value;
		}

		if ( ! empty( $settings['hosted_width'] ) ) {
			$params['width'] = $settings['hosted_width'];
		}

		if ( ! empty( $settings['hosted_height'] ) ) {
			$params['height'] = $settings['hosted_height'];
		}
		return $params;

	}

	/**
	 * Whether the video widget has an overlay image or not.
	 *
	 * Used to determine whether an overlay image was set for the video.
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @return bool Whether an image overlay was set for the video.
	 */
	protected function has_image_overlay() {
		$settings = $this->get_settings();

		return ! empty( $settings['image_overlay']['url'] ) && 'yes' === $settings['show_image_overlay'];
	}
}
