<?php
/**
 * Elementor Hero Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */

namespace DesignStudioElementorAddons\Widgets;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OtherProductsHero extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Hero widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'OtherProductsHero';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Hero widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Other Products Hero', 'ds-el' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Hero widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-youtube';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Hero widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ds-el-elements' ];
	}

	/**
	 * Register Hero widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ds-el' ),
			]
        );

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'ds-el' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your title', 'ds-el' ),
			]
		);

		$this->add_control(
			'body',
			[
				'label' => __( 'Body Content', 'ds-el' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your body content', 'ds-el' ),
			]
		);

		$this->add_control(
			'video_url',
			[
				'label'       => __( 'Background Video', 'ds-el' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'http://linktovideo.com', 'ds-el' ),
			]
		);

		$this->add_control(
			'background_image',
			[
				'label'       => __( 'Background Image', 'ds-el' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'http://linktoimage.com', 'ds-el' ),
			]
		);

		$this->add_control(
			'image',
			[
				'label'       => __( 'Company Logo', 'ds-el' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'http://linktoimage.com', 'ds-el' ),
			]
		);

		$this->add_control(
			'company_color',
			[
				'label' => __( 'Company Color', 'ds-el' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#fefefe',
			]
        );
        
		$this->add_control(
			'logo_background_color',
			[
				'label' => __( 'Logo Background Color', 'ds-el' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default' => '#fffff',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render Hero widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		?>

        <style>
            .elementor-widget-OtherProductsHero {
                margin-bottom: 75px;
            }
            .elementor-widget-OtherProductsHero > .elementor-widget-container {
				height: 400px;
            }
            .elementor-widget-OtherProductsHero video {
                width: unset;
                max-width: unset;
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%,-50%);
    			min-width: 100%;
            }
			.ds-el-oph {
				height: 100%;
			}
            .ds-el-oph-content {
                z-index: 1;
                position: absolute;
                color: white;
                width: 100%;
                text-align: center;
                font-weight: 700;
                height: 100%;
                display: flex;
                flex-direction: column;
                justify-content: center;
            }
            .ds-el-oph-title {
                font-size: 24px;
            }
            .ds-el-oph-body {
                font-size: 60px;
            }
            .ds-el-oph-image-container {
                position: absolute;
                z-index: 1;
                display: flex;
                justify-content: center;
                width: 100%;
                bottom: 0;
                border-bottom: solid 20px;
            }
            .ds-el-oph-image-wrapper {
                box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.5);
                transform: translate(0, 50%);
            }
            .ds-el-oph-image-container img {
                margin: 50px;
                max-height: 50px;
            }
			.ds-el-oph-video-container {
				height: 100%;
				overflow: hidden;
				position: relative;
                filter: brightness(50%);
				background-size: cover;
				background-position: center;
			}
        </style>
		<div class="ds-el-oph">
            <div class="ds-el-oph-content">
                <div class="ds-el-oph-title">
                    <?php echo $settings['title']; ?>
                </div>
                <div class="ds-el-oph-body">
                    <?php echo $settings['body']; ?>
                </div>

				<?php
				echo '<div class="ds-el-oph-image-container" style="border-color: ' . $settings['company_color'] . ';">';
					echo '<div class="ds-el-oph-image-wrapper" style="background-color: ' . $settings['logo_background_color'] . '">';
						echo '<img style="background-color: ' . $settings['logo_background_color'] . '" src="' . $settings['image'] . '">';
					echo '</div>';
				echo '</div>';
				?>
            </div>
			<?php echo '<div class="ds-el-oph-video-container" style="background-image: url(\'' . $settings['background_image'] . '\')">'; ?>
				<?php 
				echo '<video class="ds-el-oph-video elementor-hidden-phone" autoplay loop muted>';
					echo '<source src="' . $settings['video_url'] . '"  type="video/mp4">';
				echo '</video>'
				?>
			</div>
		</div>

		<?php
	}

}