<?php
/**
 * Elementor Product Lines Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
namespace DesignStudioElementorAddons\Widgets;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class OtherProcuctLines extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve Product Lines widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'OtherProductLines';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve Product Lines widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Other Product Lines', 'ds-el' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Product Lines widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Product Lines widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'ds-el-elements' ];
	}

	/**
	 * Register Product Lines widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'section_icon',
			[
				'label' => __( 'Product Categories', 'ds-el' ),
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'Column Count', 'ds-el' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => '2-col',
				'options' => [
					'2-col' => [
						'title' => __( '2 Columns', 'ds-el' ),
						'icon' => 'fa fa-columns',
					],
					'3-col' => [
						'title' => __( '3 Columns', 'ds-el' ),
						'icon' => 'fa fa-table',
					],
				],
				'render_type' => 'template',
				'classes' => 'elementor-control-start-end',
				'label_block' => false,
				'style_transfer' => true,
			]
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'background_image',
			[
				'label'       => __( 'Background Image', 'ds-el' ),
				'type'        => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => __( 'http://linktoimage.com', 'ds-el' ),
			]
		);

		$repeater->add_control(
			'title',
			[
				'label' => __( 'Title', 'ds-el' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => __( 'Enter your title', 'ds-el' ),
			]
		);

		$repeater->add_control(
			'body',
			[
				'label' => __( 'Body Content', 'ds-el' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => __( 'Enter your body content', 'ds-el' ),
			]
		);

		// $repeater->add_control(
		// 	'text',
		// 	[
		// 		'label' => __( 'Text', 'ds-el' ),
		// 		'type' => Controls_Manager::TEXT,
		// 		'label_block' => true,
		// 		'placeholder' => __( 'List Item', 'ds-el' ),
		// 		'default' => __( 'List Item', 'ds-el' ),
		// 	]
		// );

		// $repeater->add_control(
		// 	'icon',
		// 	[
		// 		'label' => __( 'Icon', 'ds-el' ),
		// 		'type' => Controls_Manager::ICON,
		// 		'label_block' => true,
		// 		'default' => 'fa fa-check',
		// 	]
		// );

		$repeater->add_control(
			'link',
			[
				'label' => __( 'Link', 'ds-el' ),
				'type' => Controls_Manager::URL,
				'label_block' => true,
				'placeholder' => __( 'https://your-link.com', 'ds-el' ),
			]
		);

		$this->add_control(
			'icon_list',
			[
				'label' => '',
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'text' => __( 'Tile Item #1', 'ds-el' ),
						'icon' => 'fa fa-check',
					],
					[
						'text' => __( 'Tile Item #2', 'ds-el' ),
						'icon' => 'fa fa-times',
					],
					[
						'text' => __( 'Tile Item #3', 'ds-el' ),
						'icon' => 'fa fa-dot-circle-o',
					],
				],
				'title_field' => '{{{ title }}}',
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render Product Lines widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();
		?>
		<style>
		.ds-el-opl-tiles-row {
			display: flex;
		}
		/* .ds-el-opl-tile:nth-child(3n+0) {
			border: solid pink 1px;
		} */
		.ds-el-opl-tile {
			min-height: 250px;
    		flex-grow: 1;
    		flex-basis: 0;
			background-size: cover;
    		background-position: center;
		}
		.ds-el-opl-title {
			padding: 10px;
			font-weight: 500;
			font-size: 28px;
    		color: white;
			text-align: center;
			background-color: transparent;
			background-image: linear-gradient(180deg, #0c0c0c 0%, rgba(12,0,0,0) 100%);
		}
		.ds-el-opl-body {
			padding: 40px;
			opacity: 0;
    		font-size: 18px;
    		color: white;
		}
		.ds-el-opl-background {
			height: 100%;
			width: 100%;
			background-color: rgba(0, 0, 0, 0);
			-webkit-transition: background .3s,border .3s,-webkit-border-radius .3s,-webkit-box-shadow .3s;
			transition: background .3s,border .3s,-webkit-border-radius .3s,-webkit-box-shadow .3s;
			-o-transition: background .3s,border .3s,border-radius .3s,box-shadow .3s;
			transition: background .3s,border .3s,border-radius .3s,box-shadow .3s;
			transition: background .3s,border .3s,border-radius .3s,box-shadow .3s,-webkit-border-radius .3s,-webkit-box-shadow .3s;
		}
		.ds-el-opl-background:hover {
			background-color: rgba(0, 0, 0, .5);
		}
		.ds-el-opl-background:hover .ds-el-opl-body {
			opacity: 1;
		}

		@media screen and (min-width: 768px) {
			/* different column count styling */
			.ds-el-opl-2-col .ds-el-opl-tile:nth-child(2n+1) {
				margin-right: 5px;
			}
			.ds-el-opl-2-col .ds-el-opl-tile:nth-child(2n+0) {
				margin-left: 5px;
			}
			.ds-el-opl-3-col .ds-el-opl-tile:nth-child(2) {
				margin: 0 10px;
			}
			.ds-el-opl .ds-el-opl-tile.ds-el-opl-last {
				margin-right: 0;
			}
			.ds-el-opl-tiles-row {
				margin: 10px 0;
			}
		}

		@media screen and (max-width: 767px) {
			.ds-el-opl-tiles-row {
    			flex-direction: column;
			}
			.ds-el-opl-tile {
				margin: 10px;
			}
		}

		</style>
		<div class="ds-el-opl">
			<?php
			$length = count($settings['icon_list']);
			foreach ( $settings['icon_list'] as $index => $item ) :
				$repeater_setting_key = $this->get_repeater_setting_key( 'text', 'icon_list', $index );
				if($settings['view'] === '2-col' && $index % 2 === 0 || 
				$settings['view'] === '3-col' && $index % 3 === 0) {
					echo '<div class="ds-el-opl-tiles-row ds-el-opl-' . $settings['view'] . '">';
				} ?>
					<a href="<?php echo $item["link"]["url"]; ?>" class="ds-el-opl-tile<?php echo $index + 1 === $length ? " ds-el-opl-last" : ""; ?>" style="background-image: url('<?php echo $item['background_image'] ?>')">
						<div class="ds-el-opl-background">
							<div class="ds-el-opl-title">
								<?php echo $item['title']; ?>
							</div>
							<div class="ds-el-opl-body">
								<?php echo $item['body']; ?>
							</div>
						</div>
					</a>
				<?php
				if($settings['view'] === '2-col' && $index % 2 === 1 || 
				$settings['view'] === '3-col' && $index % 3 === 2) {
					echo '</div>';
				} 
			endforeach;
			?>
			</div>
		</div>
		<?php
	}

}