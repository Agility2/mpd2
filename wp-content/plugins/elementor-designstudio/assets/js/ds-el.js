(function($) {
  var lazyVideoHandler = function( $scope, $ ) {

  	console.log( $scope );

    // not sure if we even need to wait for doc ready?
    // $(document).ready(function() {

      // the youtube widget element, with scope added so it doesn't affect anything outside of it.
      var youtube = $scope.find(".ds-el-custom-embed-image-overlay.youtube");

      // if any youtube widgets exist, which of course they do since this file is being loaded.
      if ((youtube).length) {
        for (var i = 0; i < youtube.length; i++) {


          // This section checks if there is a cover photo or not and if not it will default to the youtube cover
          if (youtube[i].hasChildNodes()) {
              // data attribute doesn't exist

              // It will then create an image and append it to the youtube widget element.
              var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.url +"/sddefault.jpg";
              var image = new Image();
                  image.src = source;
                  image.addEventListener( "load", function() {
                    youtube[ i ].appendChild( image );
                  }( i ) );
          } else {
          //
          }



          // On cover click, create and load the iframe yo.
          youtube[i].addEventListener("click", function(){
            var iframe = document.createElement("iframe");

            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("allowfullscreen", "");
            iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.url + "?rel=0&showinfo=0&showcontrols=0&autoplay=1");

            // this.innerHTML = "";
            this.appendChild(iframe);
          }); //end click function


        } //end loop
      } //end if youtube elements





    // }); //end doc ready

  }; // end lazyVideoHandler




  $( window ).on( 'elementor/frontend/init', function() {
  	elementorFrontend.hooks.addAction( 'frontend/element_ready/LazyVideo.default', lazyVideoHandler );
  } );
})(jQuery);
