<?php
namespace DesignStudioElementorAddons;

// use DesignStudioElementorAddons\Widgets\Hello_World;
use DesignStudioElementorAddons\Widgets\Cubes;
use DesignStudioElementorAddons\Widgets\External_Image;
use DesignStudioElementorAddons\Widgets\Tech_Specs_Accordion;
use DesignStudioElementorAddons\Widgets\LazyVideo;
use DesignStudioElementorAddons\Widgets\Responsive_Table;
use DesignStudioElementorAddons\Widgets\OtherProductsHero;
use DesignStudioElementorAddons\Widgets\OtherProcuctLines;
// use DesignStudioElementorAddons\Widgets\PostsSlider;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Main Plugin Class
 *
 * Register new elementor widget.
 *
 * @since 1.0.0
 */
class Plugin {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {
		$this->add_actions();
	}


	/**
	 * Add Actions
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function add_actions() {
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'on_widgets_registered' ] );

		// Add New Elementor Categories
				add_action( 'elementor/init', array( $this, 'add_elementor_category' ) );

		// 	Register Widget Scripts
				add_action( 'elementor/frontend/after_register_scripts', array( $this, 'register_widget_scripts' ), 10 );
				add_action('elementor/frontend/after_register_styles', array($this, 'register_frontend_styles'), 10);

	}

	/**
	 * Register Widget Scripts
	 *
	 * @since 1.6.0
	 *
	 * @access public
	 */
	public function register_widget_scripts()
	{

		wp_register_script( 'ds-el', plugins_url( 'assets/js/ds-el.js', __FILE__ ), array( 'jquery' ) );
		wp_enqueue_script('ds-el');

		wp_register_script( 'cubes', plugins_url( 'assets/js/cubes.js', __FILE__ ), array( 'jquery' ) );
		wp_enqueue_script('cubes');


	}


	/**
	 * Register Widget Styles
	 *
	 * @since 1.7.0
	 *
	 * @access public
	 */
	public function register_frontend_styles()
	{
			// Typing Effect
			// wp_register_style( 'ds-el-styles', plugins_url( 'elementor-designstudio/assets/css/ds-el-styles.css' ) );
			// wp_enqueue_style( 'ds-el-styles' );
	}


	/**
	 * Add Elementor Categories
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	 public function add_elementor_category()
 	{
 			\Elementor\Plugin::instance()->elements_manager->add_category( 'ds-el-elements', array(
 					'title' => __( 'DesignStudio Elements', 'ds-el' ),
 			), 0 );
 	}
	/**
	 * On Widgets Registered
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function on_widgets_registered() {
		$this->includes();
		$this->register_widget();

	}


	/**
	 * Includes
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function includes() {
		// require __DIR__ . '/widgets/hello-world.php';
		require __DIR__ . '/widgets/cubes.php';
		require __DIR__ . '/widgets/external_image.php';
		require __DIR__ . '/widgets/tech_specs_accordion.php';
		require __DIR__ . '/widgets/lazyVideo.php';
		require __DIR__ . '/widgets/responsiveTable.php';
		require __DIR__ . '/widgets/otherProductsHero.php';
		require __DIR__ . '/widgets/otherProductLines.php';
		// require __DIR__ . '/widgets/posts-slider.php';
	}

	/**
	 * Register Widget
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function register_widget() {
		// \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Hello_World() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Cubes() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new External_Image() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Tech_Specs_Accordion() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new LazyVideo() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Responsive_Table() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new OtherProductsHero() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new OtherProcuctLines() );
		// \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new PostsSlider() );

	}
}

new Plugin();
