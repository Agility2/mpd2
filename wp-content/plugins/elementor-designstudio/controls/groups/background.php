<?php
namespace DesignStudioElementorAddons\Controls\Groups;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Group_Control_Base;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Background;

class Group_Control_Background_dsel extends Group_Control_Background {
    public function init_fields() {
        $fields = parent::init_fields();
        //here you add your field
        $fields['ext_image'] = [
                'label' => _x( 'External Image', 'Background Control', 'elementor' ),
                'type' => Controls_Manager::TEXT,
                'title' => _x( 'Background Image', 'Background Control', 'elementor' ),
                'selectors' => [
                      '{{SELECTOR}}' => 'background-image: url("{{URL}}");',
                ],
                'condition' => [
                        'background' => [ 'classic' ],
                ],
         ];
         return $fields;
    }
}

new Group_Control_Background_dsel;
