<?php

/**
 * PLUGIN BY DESIGNSTUDIO
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://designstudio.com
 * @since             2.0.0
 * @package           waves2
 *
 * @wordpress-plugin
 * Plugin Name:       Waves 2.0
 * Plugin URI:        http://designstudio.com
 * Description:       Content Syndication
 * Version:           2.0.0
 * Author:            DesignStudio
 * Author URI:        https://designstudio.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       waves-2-0
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently pligin version.
 * Start at version 2.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '2.0.0' );
define( 'PLUGINURL', plugin_dir_path( __FILE__ ) );


/**
 * Waves Functions
 * 
 */
require plugin_dir_path( __FILE__ ) . 'inc/functions.php';

/**
 * Waves Settings
 * 
 */
require plugin_dir_path( __FILE__ ) . 'inc/settings.php';