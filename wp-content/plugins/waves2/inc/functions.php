<?php

/**
 * Functions File
 * 
 * @link       https://designstudio.com
 * @since      2.0.0
 *
 * @package    waves2
 * @subpackage waves2/inc
 * 
 * 
 * @since      2.0.0
 * @package    waves2
 * @subpackage waves2/inc
 * @author     DesignStudio <devteam1@DesignStudio.com>
 */


/**
 * function get rest api enable
 *
 * @since    2.0.0
 */

add_action( 'rest_api_init', 'rest_api_filter_add_filters' );

 /**
  * Add the necessary filter to each post type
  **/
function rest_api_filter_add_filters() {
	foreach ( get_post_types( array( 'show_in_rest' => true ), 'objects' ) as $post_type ) {
		add_filter( 'rest_' . $post_type->name . '_query', 'rest_api_filter_add_filter_param', 10, 2 );
	}
}

/**
 * Add the filter parameter
 *
 **/
function rest_api_filter_add_filter_param( $args, $request ) {
	// Bail out if no filter parameter is set.
	if ( empty( $request['filter'] ) || ! is_array( $request['filter'] ) ) {
		return $args;
	}

	$filter = $request['filter'];

	if ( isset( $filter['posts_per_page'] ) && ( (int) $filter['posts_per_page'] >= 1 && (int) $filter['posts_per_page'] <= 100 ) ) {
		$args['posts_per_page'] = $filter['posts_per_page'];
	}

	global $wp;
	$vars = apply_filters( 'query_vars', $wp->public_query_vars );

	foreach ( $vars as $var ) {
		if ( isset( $filter[ $var ] ) ) {
			$args[ $var ] = $filter[ $var ];
		}
	}
	return $args;
}


/**
 * function get content
 *
 * @since    2.0.0
 */
function getContent($getHost,$dsType,$dsFilter,$cName) {

	//MDP2 Default
	$host = "https://mpd2.wpengine.com/wp-json/wp/v2/";
		
	if ($getHost == "caldera") {
		$host = "https://calderaspas.com/wp-json/wp/v2/";
	}
	if ($getHost == "hotspring") {
		$host = "https://hotspring.com/wp-json/wp/v2/";
	}
	if ($getHost == "mpd") {
		$host = "https://mpd2.wpengine.com/wp-json/wp/v2/";
	}

	//example http://mpd2.wpengine.com/wp-json/wp/v2/product?filter[posts_per_page]=100&filter[product_cat]=fantasy-spas
	$response = wp_remote_get($host.$dsType.'?filter[posts_per_page]=100&filter['.$dsFilter.']=' . $cName);
	
	
		if( is_wp_error( $response ) ) {
				echo $response;
				echo "<br>";
				echo "try again please!";
				die;
		}
	
		$posts = json_decode( wp_remote_retrieve_body( $response ) );
	
		return $posts;
}

