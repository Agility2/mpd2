<?php

/**
 * Settings File
 * 
 * @link       https://designstudio.com
 * @since      2.0.0
 *
 * @package    waves2
 * @subpackage waves2/inc
 * 
 * 
 * @since      2.0.0
 * @package    waves2
 * @subpackage waves2/inc
 * @author     DesignStudio <devteam1@DesignStudio.com>
 */

function waves2_register_settings() {
	add_option( 'waves2_option_name', 'Waves 2');
	register_setting( 'waves2_options_group', 'waves2_option_name', 'waves2_callback' );
 }
 add_action( 'admin_init', 'waves2_register_settings' );
 function waves2_register_options_page() {
	add_options_page('Waves 2 Settings', 'Waves 2', 'manage_options', 'waves2', 'waves2_options_page');
  }
  add_action('admin_menu', 'waves2_register_options_page');

function waves2_options_page()
{
?>


  <div>
  <?php screen_icon(); ?>
  <h2>Waves 2</h2>

  <?php 
  // $getHost = "mpd";
  // $dsType = "product";
  // $dsFilter = "product_cat";
  // $cName = "hot-spring";
  // $getContent = getContent($getHost,$dsType,$dsFilter,$cName);

  // var_dump($getContent);
  ?>
  </div>


<?php
} ?>