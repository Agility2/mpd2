<?php
/**
 * The main template file
 *
 * This is the wrapper of all files
 *
 */
//head.php - includes the header switcher
get_header(); ?>

	<section class="main"> <!--begin main-->
		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				the_content();

			endwhile;

		else :


		endif;
		?>

	</section><!--end main-->

<?php get_footer(); ?>