<?php

//Promotions Content Block
function PromotionsContentBlock() {

	$labels = array(
		'name'              => _x( 'Promotions', 'Post Type Promotions', 'promotions' ),
		'singular_name'     => _x( 'Promotion', 'Post Type Singular Name', 'promotion' ),
		'menu_name'         => __( 'Promotions', 'promotions' ),
		'name_admin_bar'    => __( 'Promotions', 'promotions' ),
		'archives'          => __( 'Promotion Archives', 'promotions' ),
		'parent_item_colon' => __( 'Parent Promotion', 'promotions' ),
		'all_items'         => __( 'All Promotions', 'promotions' ),
		'add_new_item'      => __( 'Add New Promotion', 'promotions' ),
		'add_new'           => __( 'Add Promotion', 'promotions' ),
		'new_item'          => __( 'New Promotion', 'promotions' ),
		'edit_item'         => __( 'Edit Promotion', 'promotions' ),
		'update_item'       => __( 'Update Promotion', 'promotions' ),
		'view_item'         => __( 'View Promotion', 'promotions' ),
		'search_items'      => __( 'Search Promotion', 'promotions' ),
	);
	$args   = array(
		'label'               => __( 'Promotion', 'promotions' ),
		'description'         => __( 'Promotions for products', 'promotions' ),
		'labels'              => $labels,
		'supports'            => array(),
		'taxonomies'          => array( 'category', 'promotions-tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-megaphone',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'promotions', $args );

}

//Reviews Content Block
function ReviewsContentBlock() {

	$labels = array(
		'name'              => _x( 'Reviews', 'Post Type Reviews', 'reviews' ),
		'singular_name'     => _x( 'Review', 'Post Type Singular Name', 'Review' ),
		'menu_name'         => __( 'Reviews', 'reviews' ),
		'name_admin_bar'    => __( 'Reviews', 'reviews' ),
		'archives'          => __( 'Review Archives', 'reviews' ),
		'parent_item_colon' => __( 'Parent Review', 'reviews' ),
		'all_items'         => __( 'All Reviews', 'reviews' ),
		'add_new_item'      => __( 'Add New Review', 'reviews' ),
		'add_new'           => __( 'Add Review', 'reviews' ),
		'new_item'          => __( 'New Review', 'reviews' ),
		'edit_item'         => __( 'Edit Review', 'reviews' ),
		'update_item'       => __( 'Update Review', 'reviews' ),
		'view_item'         => __( 'View Review', 'reviews' ),
		'search_items'      => __( 'Search Review', 'reviews' ),
	);
	$args   = array(
		'label'               => __( 'Reviews', 'Reviews' ),
		'description'         => __( 'Reviews for products', 'reviews' ),
		'labels'              => $labels,
		'supports'            => array(),
		'taxonomies'          => array( 'category', 'reviews-tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-format-status',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'reviews', $args );

}

//Careers
function careers_post() {

	$labels = array(
		'name'              => _x( 'Careers', 'Post Type Careers', 'careers' ),
		'singular_name'     => _x( 'Career', 'Post Type Singular Name', 'Career' ),
		'menu_name'         => __( 'Careers', 'careers' ),
		'name_admin_bar'    => __( 'Careers', 'careers' ),
		'archives'          => __( 'Job Archives', 'careers' ),
		'parent_item_colon' => __( 'Parent Job', 'careers' ),
		'all_items'         => __( 'All Careers', 'careers' ),
		'add_new_item'      => __( 'Add New Job', 'careers' ),
		'add_new'           => __( 'Add Job', 'careers' ),
		'new_item'          => __( 'New Job', 'careers' ),
		'edit_item'         => __( 'Edit Job', 'careers' ),
		'update_item'       => __( 'Update Job', 'careers' ),
		'view_item'         => __( 'View Job', 'careers' ),
		'search_items'      => __( 'Search Job', 'careers' ),
	);
	$args   = array(
		'label'               => __( 'Careers', 'Careers' ),
		'description'         => __( 'Careers', 'careers' ),
		'labels'              => $labels,
		'supports'            => array(),
		'taxonomies'          => array( 'career-category', 'careers-tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-groups
',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'careers', $args );

}

/*Careers Categories*/
add_action( 'init', 'categories_careers' );
function categories_careers() {

	$labels = array(
		"name"          => __( 'Categories', '' ),
		"singular_name" => __( '', '' ),
	);

	$args = array(
		"label"              => __( 'Categories', '' ),
		"labels"             => $labels,
		"public"             => true,
		"hierarchical"       => 1,
		"label"              => "Categories",
		"show_ui"            => true,
		"show_in_menu"       => true,
		"show_in_nav_menus"  => true,
		"query_var"          => true,
		"rewrite"            => array( 'slug' => 'careers-categories', 'with_front' => false, ),
		"show_in_rest"       => false,
		"rest_base"          => "",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "careers-categories", array( "careers" ), $args );

// End categories_careers()
}


//Reviews Content Block
function Events() {

	$labels = array(
		'name'              => __( 'Events'),
		'singular_name'     => __( 'Event'),
		'menu_name'         => __( 'Events'),
		'name_admin_bar'    => __( 'Events'),
		'archives'          => __( 'Events Archives'),
		'parent_item_colon' => __( 'Parent Event'),
		'all_items'         => __( 'All Events'),
		'add_new_item'      => __( 'Add New Event' ),
		'add_new'           => __( 'Add Event'),
		'new_item'          => __( 'New Event' ),
		'edit_item'         => __( 'Edit Event'),
		'update_item'       => __( 'Update Event'),
		'view_item'         => __( 'View Event'),
		'search_items'      => __( 'Search Event' ),
	);
	$args   = array(
		'label'               => __( 'Events', 'Events' ),
		'description'         => __( 'Events', 'events' ),
		'labels'              => $labels,
		'supports'            => array(),
		'taxonomies'          => array( 'category', 'event_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-calendar-alt',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'events', $args );

}


//Triggers Content Block
function Triggers() {

	$labels = array(
		'name'              => __( 'Triggers'),
		'singular_name'     => __( 'Trigger'),
		'menu_name'         => __( 'Triggers'),
		'name_admin_bar'    => __( 'Triggers'),
		'archives'          => __( 'Triggers Archives'),
		'parent_item_colon' => __( 'Parent Trigger'),
		'all_items'         => __( 'All Triggers'),
		'add_new_item'      => __( 'Add New Trigger' ),
		'add_new'           => __( 'Add Trigger'),
		'new_item'          => __( 'New Trigger' ),
		'edit_item'         => __( 'Edit Trigger'),
		'update_item'       => __( 'Update Trigger'),
		'view_item'         => __( 'View Trigger'),
		'search_items'      => __( 'Search Trigger' ),
	);
	$args   = array(
		'label'               => __( 'Triggers', 'Triggers' ),
		'description'         => __( 'Triggers', 'triggers' ),
		'labels'              => $labels,
		'supports'            => array(),
		'taxonomies'          => array( 'category', 'Triggers_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-pressthis',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'Triggers', $args );

}


add_action( 'init', 'PromotionsContentBlock', 0 );
add_action( 'init', 'ReviewsContentBlock', 0 );
add_action( 'init', 'Events', 0 );
add_action( 'init', 'Triggers', 0 );
add_action( 'init', 'careers_post', 0 );


add_action( 'init', 'my_taxonomy_cleanup', 11 );
function my_taxonomy_cleanup() {
	global $wp_taxonomies;
	$wp_taxonomies['category']->object_type = array_diff( $wp_taxonomies['category']->object_type, array( 'events' ) );
}


add_action( 'init', 'create_event_tax' );

function create_event_tax() {
	register_taxonomy(
		'event_tag',
		'events',
		array(
			'label'                 => __( 'Event Tag' ),
			'rewrite'               => array( 'slug' => 'event-tag' ),
			'hierarchical'          => true,
			'update_count_callback' => '_update_post_term_count',
		)
	);
}
