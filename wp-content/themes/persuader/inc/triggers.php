<?php 
/**
 *  Functions: Triggers
 * 
 */

 //Check if trigger belongs here
 function triggerInPage() {

// Get access to the current WordPress object instance
global $wp;
// Get Page ID
 $id = get_the_ID();
// WP_Query arguments
$args = array(
	'post_type' => array( 'triggers' ),
	'posts_per_page' => '-1',
);

// The Query
$triggerNum = 0;
$query = new WP_Query( $args );
if($query) {
	while ( $query->have_posts() ) {
			$query->the_post();
			$triggerID = get_the_ID();
			// get the page id this trigger needs to appear
			$triggerPages = get_field('show_this_trigger_in');
			// Check if this page needs a trigger
			foreach ($triggerPages as $triggerPage) {
				if($triggerPage == $id) {
					//check dates of trigger and only trigger if within the days.
					$startDate = get_field('startDate');
					$endDate = get_field('endDate');
					$today = date('Ymd');
					if($today >= $startDate AND $today <= $endDate) {
						//trigger activate!
						$triggerNum ++;
						triggerActivate($triggerID, $triggerNum);
					}
				}
			}
	}
}


 } add_action( 'wp_footer', 'triggerInPage' );


 function triggerActivate($triggerID, $triggerNum) {
	 // Build trigger 
	$triggerHeight = get_field('height', $triggerID);
	$triggerWidth = get_field('width', $triggerID);
	$triggerLink = get_field('link', $triggerID);
	$triggerContent = get_field('content', $triggerID);
	$triggerTextColor = get_field('text_color', $triggerID);
	 // Check what type of trigger
	$triggerType = get_field('type_of_trigger', $triggerID);

	$triggerStyles = '<style type="text/css"> ';
	$triggerStyles .=".dsTrigger".$triggerNum." .triggerContent p, .dsTrigger".$triggerNum." .triggerContent a {color:".$triggerTextColor.";} a:hover{text-decoration:none;}";
	$bgType = get_field('background_type', $triggerID);
	if ($bgType == "image") {
		$bgImage = get_field('background_image', $triggerID);
		$triggerStyles .= ".dsTrigger".$triggerNum."{height:100%; background: url('".$bgImage."'); background-repeat:no-repeat; background-size:cover; height:100%; width:100%;}"; 
	}
	if ($bgType == "color") {
		$bgColor = get_field('background_color', $triggerID);
		$triggerStyles .= ".dsTrigger".$triggerNum."{padding:15px; height:100%; background: ".$bgColor.";}";
	}
	if ($bgType == "gradient") {
		$bgColor = get_field('background_color', $triggerID);
		$bgColor2 = get_field('background_color_2', $triggerID);
		$gradientOrientation = get_field('background_orientation', $triggerID);
		
		if($gradientOrientation == "horizontal") {
		$triggerStyles .= ".dsTrigger".$triggerNum." {
				background: ".$bgColor.";
				background: -moz-linear-gradient(left, ".$bgColor." 0%, ".$bgColor2." 100%);
				background: -webkit-gradient(left top, right top, color-stop(0%, ".$bgColor."), color-stop(100%, ".$bgColor2."));
				background: -webkit-linear-gradient(left, ".$bgColor." 0%, ".$bgColor2." 100%);
				background: -o-linear-gradient(left, ".$bgColor." 0%, ".$bgColor2." 100%);
				background: -ms-linear-gradient(left, ".$bgColor." 0%, ".$bgColor2." 100%);
				background: linear-gradient(to right, ".$bgColor." 0%, ".$bgColor2." 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='".$bgColor."', endColorstr='".$bgColor2."', GradientType=1 );
				height:100%;
				width:100%;
				padding:15px;
		}";
	}
	if($gradientOrientation == "vertical") {
		$triggerStyles .= ".dsTrigger".$triggerNum." {
			background: ".$bgColor.";
			background: -moz-linear-gradient(top, ".$bgColor." 0%, ".$bgColor2.") 100%);
			background: -webkit-gradient(left top, left bottom, color-stop(0%, ".$bgColor."), color-stop(100%, ".$bgColor2."));
			background: -webkit-linear-gradient(top, ".$bgColor." 0%, ".$bgColor2." 100%);
			background: -o-linear-gradient(top, ".$bgColor." 0%, ".$bgColor2." 100%);
			background: -ms-linear-gradient(top, ".$bgColor." 0%, ".$bgColor2." 100%);
			background: linear-gradient(to bottom, ".$bgColor." 0%, ".$bgColor2." 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='".$bgColor."', endColorstr='".$bgColor2."', GradientType=0 );
			height:100%;
			width:100%;
	     }";
	}

	}

	if($triggerType == "banner") {

		$fadeFrom = get_field('fade_from', $triggerID);




		if($fadeFrom == "topleft") {
			$animateClass = "dsTopLeft animated fadeInLeft";
			$triggerStyles .=".dsTopLeft {position:absolute; top:25px; left:25px; z-index:100000;}";
		}
		if($fadeFrom == "topright") {
			$animateClass = "dsTopRight animated fadeInRight";
			$triggerStyles .=".dsTopRight {position:absolute; top:25px; right:25px; z-index:100000;}";
		}
		if($fadeFrom == "botleft") {
			$animateClass = "dsBotLeft animated fadeInLeft";
			$triggerStyles .=".dsBotLeft {position:absolute; bottom:25px; left:25px; z-index:100000;}";
		}

		if($fadeFrom == "botright") {
			$animateClass = "dsBotRight animated fadeInRight";
			$triggerStyles .=".dsBotRight {position:absolute; bottom:25px; right:25px; z-index:10000011;}";
		}



		$bannerScript = "<script>
		jQuery('.dsTrigger".$triggerNum." .triggerBannerClose').click(function() {
			console.log('hello');
			jQuery('.dsTrigger".$triggerNum."').hide();
			});
		</script>";

		$triggerStyles .= '
		.triggerBannerClose {background: #000;
			color: white;
			border-radius: 50%;
			height: 25px;
			width: 25px;
			right: 10px;
			position: absolute;
			top: 10px;
			text-align:center;
			font-size: 13px;
			padding-top: 2px;
			z-index:10000;
		cursor:pointer;}';
		$triggerStyles .= ".dsTrigger".$triggerNum."{height:".$triggerHeight."px; width:".$triggerWidth."px;}";
		


		$triggerStyles .= "</style>";

		$bannerHTML = '
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
			<div class="dsTrigger'.$triggerNum.' '.$animateClass.'">
				<a href="'.$triggerLink.'">
				<div class="triggerContent">'. $triggerContent .'
				</div></a> 
				<div class="triggerBannerClose">X</div>
			</div>
		';


		echo $triggerStyles . $bannerHTML . $bannerScript;

	} elseif($triggerType == "bar") {

		$barScript = "<script>
		jQuery('.dsTrigger".$triggerNum." .triggerBarClose').click(function() {
			console.log('hello');
			jQuery('.dsTrigger".$triggerNum."').hide();
			jQuery('body').css('margin-top', '0');
			});
		</script>";

		$triggerStyles .= 'body {margin-top:'.$triggerHeight.'px;}
		.dsTrigger'.$triggerNum.'{width:100%; height:'.$triggerHeight.'px; text-align:center;}
		.triggerContent{padding:15px;}
		.triggerBarClose {background: #000;
			color: white;
			border-radius: 50%;
			height: 25px;
			width: 25px;
			right: 10px;
			position: absolute;
			top: 10px;
			font-size: 13px;
			padding-top: 2px;
			z-index:10000;
		cursor:pointer;}';
		$triggerStyles .= "</style>";

		$barHTML = '<div class="dsTrigger'.$triggerNum.'" style="position:absolute; top:0;"><a href="'.$triggerLink.'"><div class="triggerContent">'. $triggerContent .'</div></a> <div class="triggerBarClose">X</div></div>';

		echo $triggerStyles . $barHTML . $barScript;
	} else {

	$triggerDisplayOption = get_field('display_option', $triggerID);
	
	$popUpScript = "<script>
		jQuery('.triggerClose').click(function() {
			console.log('removing class');
			jQuery('#bodyTrigger".$triggerNum."').removeClass('showPopUp');
		});
	</script>";

	// Timed Trigger 
	if($triggerType == "auto") {
		$triggerTimer = get_field('time', $triggerID) . "000";
		
		$popUpScript .= '<script>  
		jQuery(function() { 
			function showPopUp() {
				jQuery("#bodyTrigger'.$triggerNum.'").addClass("showPopUp");
				console.log("boom!");
			}
			setTimeout(showPopUp, '.$triggerTimer.');
		}); 
		</script>';
	}


	// Scroll Trigger
	if($triggerType == "scroll") {
		$whenToTrigger = get_field('when_to_trigger', $triggerID);
		if ($whenToTrigger == "middle") {
		$popUpScript .= "
		<script>
			jQuery(function() {
				var poped = false;

				jQuery(window).scroll(function () { 
					if (jQuery(window).scrollTop() > $('body').height() / 2 && poped == false) {
						jQuery('#bodyTrigger".$triggerNum."').addClass('showPopUp');
						poped = true;
					} 
				  });
			});		
			</script>
		";
		}
		if ($whenToTrigger == "bottom") {
			$popUpScript .= "
			<script>
				jQuery(function() {
					var poped = false;

					jQuery(window).scroll(function () { 
						if (jQuery(window).scrollTop() >= jQuery(document).height() - jQuery(window).height() - 10 && poped == false) {
							jQuery('#bodyTrigger".$triggerNum."').addClass('showPopUp');
							poped = true;
						} 
					  });
				});		
				</script>
			";
			}
}


	// Exit Intent Trigger
	if($triggerType == "exit") { ?>		

		<?php 
		$triggerStyles .= ".dsTrigger".$triggerNum." p {color:".$triggerTextColor.";";
		$triggerStyles .= " </style>";
		?>

		<script type="text/javascript">
			bioEp.init({
				width:<?php echo $triggerWidth; ?>,
				height:<?php echo $triggerHeight; ?>,
				html: '<div id="bodyTrigger<?php echo $triggerNum; ?>" style="height:100%"><div class="dsTrigger<?php echo $triggerNum;?>">' + '<?php echo trim($triggerContent); ?>' + '</div></div>' + '<?php echo $triggerStyles; ?>',
				css: '#content {font-size: 20px;}',
				cookieExp: 0
			});
		</script>


<?php 	} else {

$triggerStyles .= '#bodyTrigger'.$triggerNum.' {    
	position: fixed;
   display: none;
   top: 0;
   padding:15px;
   z-index: 10000; height: 100vh; width:100%; background:rgba(0,0,0,0.6); }
   .dsTriggerContent {
	   padding:20px;
   }
   .triggerClose { 
	   background:#000; border-radius:50%; color:white; position:absolute; right: -10px;
	   top: -10px;
	   height: 30px;
	   width: 30px;
	   text-align: center;
	   padding-top: 3px;
   }
   .triggerClose:hover {cursor:pointer;}
   .dsTrigger'.$triggerNum.' {height:'.$triggerHeight.'px; width:'.$triggerWidth.'px; position:absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); }  .showPopUp {display:block !important; }";';
   $triggerStyles .= "</style>";	
   //create pop-up
	$popUpHTML = '<div id="bodyTrigger'.$triggerNum.'" style="height:100%"><div class="dsTrigger'.$triggerNum.'"><div class="triggerClose">X</div><div class="triggerContent">'.$triggerContent.'</div></div></div>'.$triggerStyles. $popUpScript;

	echo $popUpHTML; 

	}
}


 } // End of TriggerActivate Func