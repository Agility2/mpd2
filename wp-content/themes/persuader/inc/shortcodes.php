<?php

add_action( 'init', 'register_shortcodes' );



//Current year ShortCode
add_shortcode( 'currentyear', 'currentYear' );
function currentYear( $atts ) {
	$currentYear = date( 'Y' );

	return $currentYear;
}


//ShortCode For Calc Energy (WATKINS)
function register_shortcodes() {
  add_shortcode( 'calc-energy', 'calcEnergyFunction' );
}

function calcEnergyFunction( $brand ) {
  extract( shortcode_atts( array(
    'brand' => 'hotspring',
  ), $atts ) );

  ob_start();
  ?>
  <div class="panel-pane pane-energy-calculator">
    <div class="pane-content">
      <div class="energy-calculator">
        <h2>Estimate your monthly operating costs</h2>
        <form role="form" id="energy-calculator-form" method="post" name="ecf">
          <fieldset>
            <div class="form-group">


              <?php if ( $brand == "hotspring" ) { ?>
                <select name="model" class="form-control selectpicker">
                  <option value="" default selected>Select a Model</option>
                  <option value="grandee">Grandee</option>
                  <option value="envoy">Envoy</option>
                  <option value="aria">Aria</option>
                  <option value="vanguard">Vanguard</option>
                  <option value="sovereign">Sovereign</option>
                  <option value="prodigy">Prodigy</option>
                  <option value="jetsetter">Jetsetter</option>
                </select>
              <?php } ?>

            </div>
            <br/>
            <div class="form-group">
              <select name="climate" class="form-control selectpicker">
                <option value="" default selected>Select a Climate and Rate</option>
                <option value="0">Miami, FL (75&deg;) &mdash; $0.11 kWh</option>
                <option value="1">Las Vegas, NV (70&deg;) &mdash; $0.12 kWh</option>
                <option value="2">Macon, GA (65&deg;) &mdash; $0.10 kWh</option>
                <option value="3">Tulsa, OK (60&deg;) &mdash; $0.08 kWh</option>
                <option value="4">St. Louis, MO (55&deg;) &mdash; $0.09 kWh</option>
                <option value="5">Chicago, IL (50&deg;) &mdash; $0.09 kWh</option>
                <option value="6">Casper, WY (45&deg;) &mdash; $0.10 kWh</option>
                <option value="7">Calgary, Canada (40&deg;) &mdash; $0.09 kWh</option>
                <option value="8">Int'l Falls, MN (35&deg;) &mdash; $0.11 kWh</option>
                <option value="9">Fairbanks, AK (30&deg;) &mdash; $0.18 kWh</option>
              </select>
            </div>
            <br/>
            <div class="clearfix"></div>
            <div class="form-group text-center">
              <!--                        <button type="submit"><span class="blue-button">Calculate Cost</span></button>-->
            </div>
          </fieldset>
        </form>
        <div class="energy-calculator-result">
          <h3></h3>
          <span></span>
        </div>
      </div>
    </div>


  </div>
  <br>
  <br>
  <?php
  return ob_get_clean();

}
