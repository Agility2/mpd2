<?php //function for Promotions
function getPromoPageID() {
	$promoPages = get_posts( array(
		'post_type'  => 'page',
		'meta_key'   => '_wp_page_template',
		'meta_value' => 'template-promotions.php'
	) );
	foreach ( $promoPages as $promoPage ) {
//promotions page ID
		$promoPageID = $promoPage->ID;
	}

	return $promoPageID;
}


function promoCronJob() {
//this function is to check if a promo is expired and needs to be taken off the site or if there is a new
//promotion that needs to be added into the site.
//this should be run every day and maybe after promotion gets created/editted?

//find page id with the template promotions! (in case they move it or we update the site we always know where is at)
	$promoPageID = getPromoPageID();

//today's date
	$date = date( 'Y-m-d' );

//check if promotion needs to be on the promotions page
	$inactivePromos = get_posts( array(
		'post_type'   => 'promotions',
		'post_status' => 'draft',
		'meta_query'  => array(
			'relation' => 'AND',
			array(
				'key'     => 'promoStartDate',
				'value'   => $date,
				'compare' => '<=',
				'type'    => 'DATETIME',
			),
			array(
				'key'     => 'promoEndDate',
				'value'   => $date,
				'compare' => '>=',
				'type'    => 'DATETIME',
			),
		)
	) );

//all inactive promos that need to be publish and added to the promotions page
	if ( $inactivePromos ) {
		foreach ( $inactivePromos as $inactivePromo ) {
			$inactivePromoID = $inactivePromo->ID;

//make sure the promo is now publish
			wp_update_post( array(
				'ID'          => $inactivePromoID,
				'post_status' => 'publish'
			) );

//add the promo to the promotions page
			$arrangePromotions = get_field( 'arrangePromotions', $promoPageID );
			if ( ! is_array( $arrangePromotions ) ) {
				$arrangePromotions = array();
			}
			array_push( $arrangePromotions, $inactivePromoID );
			update_field( 'field_589b893d30f53', $arrangePromotions, $promoPageID );

		}
	}

//check if promotion needs to be off the promotions page
	$activePromos = get_posts( array(
		'post_type'   => 'promotions',
		'post_status' => 'publish',
		'meta_query'  => array(
			array(
				'key'     => 'promoEndDate',
				'value'   => $date,
				'compare' => '<',
				'type'    => 'DATETIME',
			),
		)
	) );

//remove active promos from the promo page and make them a draft
	if ( $activePromos ) {
		foreach ( $activePromos as $activePromo ) {
			$activePromoID = $activePromo->ID;

//make sure the promo is now a draft
			wp_update_post( array(
				'ID'          => $activePromoID,
				'post_status' => 'draft'
			) );

//remove it from promotions page
			$arrangePromotions = get_field( 'arrangePromotions', $promoPageID );
			if ( ! is_array( $arrangePromotions ) ) {
				$arrangePromotions = array();
			}

			if ( ( $key = array_search( $activePromoID, $arrangePromotions ) ) !== false ) {
				unset( $arrangePromotions[ $key ] );
			}
			update_field( 'field_589b893d30f53', $arrangePromotions, $promoPageID );

		}
	}

}

function promoDisplay( $pageID ) {
	promoDisplayBanner( $pageID );
	promoDisplayNotificationBar( $pageID );
}

function promoDisplayBanner( $pageID ) {
//find out if you need to display a promotion banner
	$promotions = get_posts( array(
		'post_type'      => 'promotions',
		'posts_per_page' => - 1,
		'meta_query'     => array(
			array(
				'key'     => 'promoBWhereDisplayed',
				'value'   => '"' . $pageID . '"',
				'compare' => 'LIKE'
			)
		)
	) );

	if ( $promotions ) { ?>
		<?php
		foreach ( $promotions as $promotion ) {
			$promoID = $promotion->ID;
			//direction of the banner to pop from.
			//5 options - top left, top right, center, bottom left, bottom right.
			//need to find out before I can start positioning the banner incase there is 2 or more I can convert them to a slider
			$promoDirection = get_field( 'promoBannerDirection', $promoID );
			if ( $promoDirection == "" ) {
				$promoDirection = "bdCenter";
			}
			switch ( $promoDirection ) {
				case "bdTopLeft":
					$promosTopLeft[] .= $promoID;
					break;
				case "bdBottomLeft":
					$promosBottomLeft[] .= $promoID;
					break;
				case "bdTopRight":
					$promosTopRight[] .= $promoID;
					break;
				case "bdBottomRight":
					$promosBottomRight[] .= $promoID;
					break;
				case "bdCenter":
					$promosCenter[] .= $promoID;
					break;
			}
		}

//Promos from Top Left
		if ( $promosTopLeft ) {
			$promoDirectionX = "left";
			$promoDType      = "TopLeft";
			CreateBanner( $promosTopLeft, $promoDType, $promoDirectionX );
		}

		if ( $promosTopRight ) {
			$promoDirectionX = "right";
			$promoDType      = "TopRight";
			CreateBanner( $promosTopRight, $promoDType, $promoDirectionX );
		}

		if ( $promosBottomLeft ) {
			$promoDirectionX = "left";
			$promoDType      = "BottomLeft";
			CreateBanner( $promosBottomLeft, $promoDType, $promoDirectionX );
		}

		if ( $promosBottomRight ) {
			$promoDirectionX = "right";
			$promoDType      = "BottomRight";
			CreateBanner( $promosBottomRight, $promoDType, $promoDirectionX );
		}

		if ( $promosCenter ) {
			$promoDirectionX = "center";
			$promoDType      = "Center";
			CreateBanner( $promosCenter, $promoDType, $promoDirectionX );
		}

	}
} // end of main function

function CreateBanner( $promos, $promoDType, $promoDirectionX ) {
	?>
	<div id="promo<?php echo $promoDType; ?>" class="promoWindow bd<?php echo $promoDType; ?>">
		<div id="closeB<?php echo $promoDType; ?>" class="closeBtn" style="cursor:pointer">X</div>
		<div id="promo<?php echo $promoDType; ?>Slider">
			<?php
			foreach ( $promos as $promoID ) {
				$promoBannerDelay = get_field( 'promoBannerDelay', $promoID );
				$promoTitle       = get_the_title( $promoID );
				$promoLink        = get_field( 'promoLink', $promoID );
				$promoSubTitle    = get_field( 'spDescription', $promoID );

				//display title
				$promoBannerTitleDisplay  = get_field( 'promoBannerTitleDisplay', $promoID );  //true or false
				$promoBannerTitlePosition = get_field( 'promoBannerTitlePosition', $promoID ); // radio btn
				$promoBannerTextColor     = get_field( 'promoBannerTextColor', $promoID ); //text color white or black
				$promoBannerColor         = get_field( 'promoBannerColor', $promoID ); // banner background color
				$promoBannerShadow        = get_field( 'promoBannerShadow', $promoID ); // shadow?!
				if ( $promoBannerShadow == 1 ) {
					$promoShadow = "
                            -webkit-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.75);
                            -moz-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.75);
                            box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.75);";
				}
				$promoBannerBorder = get_field( 'promoBannerBorder', $promoID ); // border?!
				if ( $promoBannerBorder == 1 ) {
					$promoBorder = "border:1px solid #fff;";
				}

				if ( $promoLink == "" ) {
					//find page id with the template promotions! (in case they move it or we update the site we always know where is at)
					$promoPageID   = getPromoPageID();
					$promoPageLink = get_the_permalink( $promoPageID );
					$promoLink     = $promoPageLink . "#promo" . $promoID;
				}
				//promo date
				$promoStartDate = get_field( 'promoStartDate', $promoID );
				$promoEndDate   = get_field( 'promoEndDate', $promoID );
				//get background image
				$promoBannerImg = get_field( 'promoBannerImg', $promoID );
				$promoDirection = get_field( 'promoBannerDirection', $promoID );
				//delay on the banner
				$promoBannerDelay = get_field( 'promoBannerDelay', $promoID );

				if ( $promoBannerTitleDisplay == 1 ) {
					$promoBannerTitleDescription = '
                              <div class="promoTitleContainer">
                                <h3 style="text-align:center;">' . $promoTitle . '</h3>
                                ' . $promoSubTitle . '
                              </div>';
				}
				?>

				<div id="promoBanner<?php echo $promoID; ?>"
				     style="background: <?php echo $promoBannerColor; ?>; position:relative; z-index:10000;">
					<style>
						.bd<?php echo $promoDType; ?> {
						<?php echo $promoBorder . $promoShadow; ?>
						}

						#promoBanner<?php echo $promoID; ?> .promoTitleContainer {
							background: <?php echo $promoBannerColor; ?>;
							color: <?php echo $promoBannerTextColor; ?>;
						}
					</style>
					<a href="<?php echo $promoLink; ?>">
						<?php
						if ( $promoBannerTitlePosition == "top" ) {
							echo $promoBannerTitleDescription;
						}
						?>
						<img src="<?php echo $promoBannerImg; ?>" alt="PromoBanner">
						<?php
						if ( $promoBannerTitlePosition == "bottom" ) {
							echo $promoBannerTitleDescription;
						}
						?>
					</a>
				</div>
			<?php } ?>
		</div>
		<script>
			$('#promo<?php echo $promoDType; ?>Slider').slick({
				dots: false,
				arrows: true,
				speed: 300,
				adaptiveHeight: true,
				autoplay: true,
				autoplaySpeed: 10000,
			});
			function openPopup() {
				$("#promo<?php echo $promoDType; ?>").animate({<?php echo $promoDirectionX; ?>:
				80, opacity
			:
				1
			},
				1000
			)
				;
			}
			$("#closeB<?php echo $promoDType; ?>").click(function () {
				$("#promo<?php echo $promoDType; ?>").animate({<?php echo $promoDirectionX; ?>:
				0, opacity
				:
				0
			},
				500
				)
				;
				$("#promo<?php echo $promoDType; ?>").delay(600).hide(0);
			});
			setTimeout(openPopup, <?php echo $promoBannerDelay; ?>);
		</script>
	</div>
<?php }

function promoDisplayNotificationBar( $pageID ) {
//find out if you need to display a promotion notification bar
	//query the database for all promos that match the page ID
	$promotions = get_posts( array(
		'post_type'  => 'promotions',
		'meta_query' => array(
			array(
				'key'     => 'promoNBWhereDisplayed',
				'value'   => '"' . $pageID . '"',
				'compare' => 'LIKE'
			)
		)
	) );

	if ( $promotions ) { ?>
		<style>
			.bodyContainer {
				position: absolute;
				width: 100%;
				height: 100%;
				top: 100px;
				-webkit-transition: all 1s ease-in-out;
				-moz-transition: all 1s ease-in-out;
				-o-transition: all 1s ease-in-out;
				transition: all 1s ease-in-out;
			}

			#closeNB {
				position: absolute;
				right: 15px;
				top: 35px;
				height: 30px;
				width: 30px;
				background: white;
				color: black;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
				padding-top: 3px;
				text-align: center;
				border-radius: 50%;
				z-index: 100000;
			}

			#promoBars {
				position: absolute;
				height: 100px;
				width: 100%;
				color: white;
				display: block;
				top: 0;
			}

			.promoNBContent {
				padding: 25px;
			}

			.promoNB {
				height: 100px;
				width: 100%;
			}

			.promoNBWithBtn, .promoNBWithImg {
				width: 100%;
				text-align: center;
			}

			.promoNBWithBtn h3, .promoNBWithImg h3 {
				display: inline-block;
				vertical-align: middle;
			}

			.promoNBWithImg img {
				display: inline-block;
				vertical-align: middle;
				max-height: 60px;
				margin: 0 15px;
			}

			.promoNBWithBtn .btn {
				display: inline-block;
				vertical-align: middle;
				padding: 12px !important;
				margin: 0 15px !important;
			}

			.promoNBContent h3 {
				text-align: center;
			}

			@media screen and (max-width: 1080px) {
				.promoNBContent h3, .promoNBWithBtn h3, .promoNBWithImg h3, .promoNBWithBtn .btn {
					font-size: 18px !important;
				}
			}

			@media screen and (max-width: 700px) {
				.promoNBContent h3, .promoNBWithBtn h3, .promoNBWithImg h3, .promoNBWithBtn .btn {
					font-size: 16px !important;
				}
			}
		</style>
		<div id="promoBars">
			<div id="closeNB" style="cursor:pointer">X</div>
			<div id="promoSlider">
				<?php
				foreach ( $promotions as $promotion ) {
					$promoID = $promotion->ID;
					//Content
					$promoNBType      = get_field( 'promoNBType', $promoID );
					$promoNBText      = get_field( 'promoNBText', $promoID );
					$promoNBTypeofImg = get_field( 'promoNBTypeofImg', $promoID );
					if ( $promoNBTypeofImg == "media" ) {
						$promoNBImg = get_field( 'promoNBImg', $promoID );
					} else {
						$promoNBImg = get_field( 'promoNBXImg', $promoID );
					}
					$promoNBImgPosition = get_field( 'promoNBImgPosition', $promoID );
					$promoNBBtnLabel    = get_field( 'promoNBBtnLabel', $promoID );
					$promoNBBtnURL      = get_field( 'promoNBBtnURL', $promoID );
					$promoNBBtnPosition = get_field( 'promoNBBtnPosition', $promoID );

					$promoNBTypeofBG = get_field( 'promoNBTypeofBG', $promoID );
					if ( $promoNBTypeofBG == "solid" ) {
						$promoNBColor1 = get_field( 'promoNBPrimaryColor', $promoID );
						$promoNBBG     = "background: " . $promoNBColor1 . ";";
					}
					if ( $promoNBTypeofBG == "gradient" ) {
						$promoNBColor1 = get_field( 'promoNBPrimaryColor', $promoID );
						$promoNBColor2 = get_field( 'promoNBGradientColor', $promoID );
						$promoNBBG     = 'background: linear-gradient(' . $promoNBColor1 . ',' . $promoNBColor2 . ');';

					}
					$promoTextColor = get_field( 'promoNBTextColor', $promoID );
					//promo date
					$promoStartDate = get_field( 'promoStartDate', $promoID );
					$promoEndDate   = get_field( 'promoEndDate', $promoID );
					$promoLink      = get_field( 'promoLink', $promoID );
					if ( $promoLink == "" ) {
						//find page id with the template promotions! (in case they move it or we update the site we always know where is at)
						$promoPageID   = getPromoPageID();
						$promoPageLink = get_the_permalink( $promoPageID );
						$promoLink     = $promoPageLink . "#promo" . $promoID;
					}
					//get color or gradient or image background
					//get text in notification
					//get link if need it or the default to promotions
					?>
					<div id="promoNotificationBar<?php echo $promoID; ?>" class="promoNB"
					     style="<?php echo $promoNBBG; ?>color:<?php echo $promoTextColor; ?>">
						<div class="promoNBContent">
							<?php
							//text only
							if ( $promoNBType == "tOnly" ) {
								echo "<h3>" . $promoNBText . "</h3>";
							}
							//text and img
							if ( $promoNBType == "tImg" ) {
								if ( $promoNBImgPosition == "left" ) { ?>
									<div class="promoNBWithImg">
										<img src="<?php echo $promoNBImg; ?>" alt="promoImg">
										<h3><?php echo $promoNBText; ?></h3>
									</div>
								<?php } else { ?>
									<div class="promoNBWithImg">
										<h3><?php echo $promoNBText; ?></h3>
										<img src="<?php echo $promoNBImg; ?>" alt="promoImg">
									</div>
								<?php }
							}
							//text and btn
							if ( $promoNBType == "tBtn" ) {
								if ( $promoNBBtnPosition == "left" ) { ?>
									<div class="promoNBWithBtn">
										<a href="<?php echo $promoNBBtnURL; ?>"
										   class="btn"><?php echo $promoNBBtnLabel; ?></a>
										<h3><?php echo $promoNBText; ?></h3>
									</div>
								<?php } else { ?>
									<div class="promoNBWithBtn">
										<h3><?php echo $promoNBText; ?></h3>
										<a href="<?php echo $promoNBBtnURL; ?>"
										   class="btn"><?php echo $promoNBBtnLabel; ?></a>
									</div>
								<?php }
							} ?>
						</div>
					</div>
				<?php } ?>
			</div>
			<script>
				$("#closeNB").click(function () {
					$("#promoBars").fadeOut();
					$(".bodyContainer").css("top", "0");
				});
				$('#promoSlider').slick({
					dots: false,
					arrows: false,
					speed: 300,
					adaptiveHeight: true,
					autoplay: true,
					autoplaySpeed: 10000,
				});
			</script>
		</div>
	<?php }
}