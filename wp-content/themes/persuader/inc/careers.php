<?php
/* Career function */
function getCareerContent() {

	// retrieve post_id, and sanitize it to enhance security
	$postID = intval( $_POST['careerID'] );

	// Check if the input was a valid integer
	if ( $postID == 0 ) {

		$response['error']  = 'true';
		$response['result'] = 'Invalid Input';

	} else {

		// get the post
		$careerPost = get_post( $postID );

		// check if post exists
		if ( ! is_object( $careerPost ) ) {

			$response['error']  = 'true';
			$response['result'] = 'There is no post with the ID ' . $postID;

		} else {

			$response['error']  = 'false';
			$response['title']  = $careerPost->post_title;
			$response['result'] = wpautop( $careerPost->post_content );

		}

	}
	wp_send_json( $response );
}

add_action( 'wp_ajax_getCareerContent', 'getCareerContent' );    // If called from admin panel
add_action( 'wp_ajax_nopriv_getCareerContent', 'getCareerContent' );
