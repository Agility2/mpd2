<?php

//get option if enable custom content types

$cctPromo   = get_field( 'cctPromos', 'option' );
$cctReviews = get_field( 'cctReviews', 'option' );
$cctEvents  = get_field( 'cctEvents', 'option' );
$cctCareers = get_field( 'cctCareers', 'option' );

if ( $cctPromo == 0 ) {

	add_action( 'admin_head', 'hidePromos' );

	function hidePromos() {
		echo '<style>
    #menu-posts-promotions {
      display:none !important;
    }
  </style>';
	}
}
if ( $cctReviews == 0 ) {

	add_action( 'admin_head', 'hideReviews' );

	function hideReviews() {
		echo '<style>
      #menu-posts-reviews {
        display:none !important;
      }
    </style>';
	}
}


if ( $cctEvents == 0 ) {

	add_action( 'admin_head', 'hideEvents' );

	function hideEvents() {
		echo '<style>
      #menu-posts-events {
        display:none !important;
      }
    </style>';
	}
}

if ( $cctCareers == 0 ) {

	add_action( 'admin_head', 'hideCareers' );

	function hideCareers() {
		echo '<style>
      #menu-posts-careers {
        display:none !important;
      }
    </style>';
	}
}
