<?php
// MAIN BLOCK SECTION BACKGROUND STYLING OPTIONS

function gcbBackground() {


	$blockID                       = get_the_ID();
	$primaryBgColor                = get_field( 'bg_type_twonew_color_primary' );
	$secondaryBgColor              = get_field( 'bg_type_twonew_color_second' );
	$primaryBgOpacity              = get_field( 'bg_type_twonew_color_primary_op' );
	$secondaryBgOpacity            = get_field( 'bg_type_twonew_color_second_op' );
	$primaryBgLocation             = get_field( 'bg_type_twonew_color_primary_loc' );
	$secondaryBgLocation           = get_field( 'bg_type_twonew_color_second_loc' );
	$backgroundContainerType       = get_field( 'background_type_twonew_contain' );

	?>
	<?php switch ( $backgroundContainerType ) {
		// Basic BG Color
		case 'color': ?>
			<style>
				.gcbBG<?php echo $blockID; ?> {
					background-color: <?php echo $primaryBgColor ?>;
				}
			</style>
			<?php break;
		// Gradient BG
		case 'gradient':
			// Gradient Direction Options
			$gradientDirection = get_field( 'bg_type_twonew_gradient_direct' );

			switch ( $gradientDirection ) {
				case 'horizontal': ?>
					<style media="screen">
						.twoColumnSectionID<?php echo $blockID; ?> {
							background: -webkit-linear-gradient(left, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: -o-linear-gradient(left, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: -moz-linear-gradient(left, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: linear-gradient(to left, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%)
					</style>
					<?php
					break;
				case 'vertical': ?>
					<style>
						.twoColumnSectionID<?php echo $blockID; ?> {
							background: -webkit-linear-gradient(<?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: -o-linear-gradient(<?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: -moz-linear-gradient(<?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: linear-gradient(<?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%)
						}
					</style>
					<?php
					break;
				case 'diagonal': ?>
					<style media="screen">
						.twoColumnSectionID<?php echo $blockID; ?> {
							background: -webkit-linear-gradient(left top, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: -o-linear-gradient(bottom left, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: -moz-linear-gradient(bottom left, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%);
							background: linear-gradient(to bottom left, <?php echo changeHex($primaryBgColor, $primaryBgOpacity)?> <?php echo $primaryBgLocation ?>%, <?php echo changeHex($secondaryBgColor, $secondaryBgOpacity)?> <?php echo $secondaryBgLocation ?>%)
						}
					</style>
					<?php
					break;
				case 'radial': ?>

					<style media="screen">
						.twoColumnSectionID<?php echo $blockID; ?> {
							background: radial-gradient(circle at <?php echo $secondaryBgLocation ?>% <?php echo $secondaryBgLocation ?>%, <?php echo $primaryBgColor ?>, transparent <?php echo $primaryBgOpacity ?>%), radial-gradient(circle at <?php echo $secondaryBgLocation ?>% <?php echo $secondaryBgLocation ?>%, <?php echo $secondaryBgColor ?>, <?php echo $secondaryBgColor ?> <?php echo $secondaryBgOpacity ?>%);
						}
					</style>
					<?php
					break;
				default:
					# code...
					break;
			}
			break;

		case 'image':

			// Background IMAGE ACFs
			$backgroundImageOpt = get_field( 'bg_type_twonew_bg_img' );
			$backgroundImageUpload = get_field( 'bg_type_twonew_bg_img_up' );
			$backgroundImageURL    = get_field( 'bg_type_twonew_bg_img_url' );
			switch ( $backgroundImageOpt ) {
				case 'url': ?>
					<style>
						.gcbBG<?php echo $blockID; ?> {
							background: url(<?php echo $backgroundImageURL ?>);
							background-size: cover;
							background-repeat: no-repeat;
							background-position: 50% 50%;
						}
					</style>
					<?php
					break;
				case 'media_gallery': ?>
					<style>
						.gcbBG<?php echo $blockID; ?> {
							background: url(<?php echo $backgroundImageUpload ?>);
							background-size: cover;
							background-repeat: no-repeat;
							background-position: 50% 50%;
						}
					</style>
					<?php
					break;
				default:
					# code...
					break;
			}
			?>
			<?php break;
		// This is the multiple Images Option for Backgrounds (I.E. Fade in/out)
		case 'multiple_images':
			// ACF Fields
			$multiBgImgOpt = get_field( 'bg_type_twonew_bg_imgs' );
			switch ( $multiBgImgOpt ) {
				// Repeater IMG URLS
				case 'url':
					// check if the repeater field has rows of data
					if ( have_rows( 'bg_type_twonew_bg_imgs_urls' ) ):
						// loop through the rows of data
						while ( have_rows( 'bg_type_twonew_bg_imgs_urls' ) ) : the_row();
							// display a sub field value
							$bgImgsUrl = get_sub_field( 'twonew_bgs_imgs_url' );
						endwhile;
					else :
						// no rows found
					endif;
					break;

				case 'media_gallery':
					$multipleBGImages       = get_field( 'bg_type_twonew_bg_imgs_up' );
					$multipleBgImgsDuration = get_field( 'bg_type_twonew_bg_imgs_dur' );
					$multipleBgImgsFade     = get_field( 'bg_type_twonew_bg_imgs_fade' );

					if ( $multipleBGImages ):
						$multiBgImgsStyles = "<style>
							#slideshow" . $blockID . " div{
							  position: absolute;
							  top: 0;
							  left: 0;
							  width: 100%;
							  height:100%;
							}
							@media(max-width:992px) {
								#slideshow" . $blockID . " {
									display: none;
								}
							}
							</style>";
						foreach ( $multipleBGImages as $BGImage ):
							// This is the href of each image in the array of media gallery images for bg.
							$image = $BGImage['url'];
							$blockFIImages .= "<div style='height:100%; background:url(" . $image . "); background-size:cover;'> </div>";
							?>

							<?php
						endforeach;
					endif;
					// Adding Overlay Options to the Multi-Img Background
					$multiBgImgsStyles .= "
						<style>
							#slideshow" . $blockID . ":after {
								content: '';
								position: absolute;
								// background: rgba(255, 0, 0, 0.29);
								z-index: 4;
								top: 0;
								left: 0;
								width: 100%;
								height: 100%;
							}
						</style>";

					break;

				default:
					# code...
					break;
			}
			break;

		case 'video':
			// Main container background VIDEO Option. More. Options! More.. Options!
			// ACF Fields for Video
			$bgVideoMp4Url  = get_field( 'bg_type_twonew_bg_vid_mp4' );
			$bgVideoWebmUrl = get_field( 'bg_type_twonew_bg_vid_webm' );
			$bgVideoMobOpt  = get_field( 'bg_type_twonew_bg_vid_mob' );
			// Video Mobile Image type check
			if ( $bgVideoMobOpt == "url" ) {
				$bgVideoImg = get_field( 'bg_type_twonew_bg_vid_mob_url' );
			} else {
				$bgVideoImg = get_field( 'bg_type_twonew_bg_vid_mob_up' );
			}
			$bgVideoStyles = '<style>
												.ID' . $blockID . ' video {
												top: 0;
										    left: 0;
										    min-width: 100%;
										    min-height: 100%;
										    object-fit: cover;
										    display: block;
										    position: absolute;
										    z-index: 0;
												}
												@media(max-width:992px) {
													.ID' . $blockID . ' video {
														display: none;
													}
												}
												.ID' . $blockID . '{
													overflow: hidden;
													background: url(' . $bgVideoImg . ');
													background-repeat: no-repeat;
													background-size: cover;
													background-position: 50%;
												}
												@media(min-width:993px) {
													.ID' . $blockID . '{
														background: none;
													}
												}
												</style>';
			$bgVideo       = '<video id="' . $blockID . '" autoplay loop>
			<source src="' . $bgVideoWebmUrl . '" type="video/webm" codecs="vp8.0, vorbis"/>
		 <source src="' . $bgVideoMp4Url . '" type="video/mp4">
			</video>';

			break;


		default:
			# code...
			break;
	} ?>


	<!-- MAIN BLOCK SECTION BACKGROUND OVERLAY STYLING OPTIONS  -->
	<?php
	// ACF FIELDS
	$containBgOvlerayOpts     = get_field( 'bg_type_twonew_overlay_type' );
	$containBgOverlayPrimary  = get_field( 'bg_type_twonew_overlay_primary' );
	$containBgOverlayOpacity  = get_field( 'bg_type_twonew_overlay_op' );
	$containBgOverlayOpacity2 = get_field( 'bg_type_twonew_overlay_op2' );
	$containBgOverlaySecond   = get_field( 'bg_type_twonew_overlay_second' );
	$containBgLocationPrimary = get_field( 'bg_type_twonew_overlay_loc' );
	$containBgLocationSecond  = get_field( 'bg_type_twonew_overlay_loc2' );

	switch ( $containBgOvlerayOpts ) {
		case 'color':
			$containBgOverlayStyle .= "
	<style>
			.ID" . $blockID . " .overlay {
				content: ' ';
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background: " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . ";
				z-index:1;
			}
	</style>
			";
			break;
		case 'gradient':
			$overlayGradientDirection = get_field( 'bg_type_twonew_overlay_grad_dir' );
			switch ( $overlayGradientDirection ) {
				case 'horizontal':
					$gradientStyle .= "
					background: -webkit-linear-gradient(left," . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%," . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					background: -o-linear-gradient(left, " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					background: -moz-linear-gradient(left, " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					background: linear-gradient(to left, " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					";

					break;

				case 'vertical':
					$gradientStyle .= "
				background: -webkit-linear-gradient(" . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%," . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
				background: -o-linear-gradient(" . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
				background: -moz-linear-gradient(" . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
				background: linear-gradient(" . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
				";
					break;

				case 'diagonal':
					$gradientStyle .= "
					background: -webkit-linear-gradient(left top, " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%," . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					background: -o-linear-gradient(bottom left, " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					background: -moz-linear-gradient(bottom left, " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					background: linear-gradient(to bottom left, " . changeHex( $containBgOverlayPrimary, $containBgOverlayOpacity ) . $containBgLocationPrimary . "%, " . changeHex( $containBgOverlaySecond, $containBgOverlayOpacity2 ) . $containBgLocationSecond . "%);
					";
					break;

				case 'radial':
					$gradientStyle .= "
					background: radial-gradient(circle at " . $containBgLocationPrimary . "% " . $containBgLocationPrimary . "%, " . $containBgOverlayPrimary . ", transparent " . $containBgOverlayOpacity . "%), radial-gradient(circle at " . $containBgLocationSecond . "% " . $containBgLocationSecond . "%, " . $containBgOverlaySecond . ", " . $containBgOverlaySecond . " " . $containBgOverlayOpacity2 . "%);
					opacity:." . $containBgOverlayOpacity . ";";
					break;

				default:
					# code...
					break;
			}

			$containBgOverlayStyle .= "
			<style>
				.ID" . $blockID . " .overlay {
					content: '';
					position: absolute;
					width: 100%;height: 100%;
					top: 0;
					left: 0;
					z-index: 1;
					" . $gradientStyle . "
				}
			</style>
			";

			break;

		default:
			$containBgOverlayStyle .= "";
			break;
	}

	// TEXTURE / ANIMATION OVERLAY//////////////////////////////////

	$containerBgTextOverlay         = get_field( 'bg_type_twonew_textoverlay' );
	$containerTextureOverlayOpacity = get_field( 'bg_type_twonew_texture_opacity' );
	switch ( $containerBgTextOverlay ) {

		case 'repeating_texture':
			$overlayTextureBG = get_field( 'bg_type_twonew_texture_type' );
			switch ( $overlayTextureBG ) {
				case 'horizontal_stripes':
					$textureBackgroundURL = "/wp-content/themes/prelude/assets/img/textures/subtle-stripes.png";
					break;
				case 'vertical_stripes':
					$textureBackgroundURL = "/wp-content/themes/prelude/assets/img/textures/blu-stripes.png";
					break;

				default:
					break;
			}
			$containBgTextOverlayStyle .= "
		<style>
		.ID" . $blockID . " .texture-overlay {
			background: url(" . $textureBackgroundURL . ");
			position: absolute;
			background-repeat: repeat;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			z-index: 4;
			opacity: ." . $containerTextureOverlayOpacity . ";
		}
		</style>
		";
			break;

		case 'animation':
			$containBgTextOverlayStyle .= "
		<style>
		.ID" . $BlockID . ":after {

		}
		</style>
		";
			break;

		default:
			# code...
			break;
	}


	//  Echo the styles for the main container overlay.
	echo $containBgOverlayStyle;
	echo $containBgTextOverlayStyle;

	// If Multiple Image Background
	if ( $backgroundContainerType == "multiple_images" ) { ?>
		<?php echo $multiBgImgsStyles ?>
		<div id="slideshow<?php echo $blockID ?>"><?php echo $blockFIImages ?></div>
		<?php
	} // End If Multiple Image Background

	// If Video BG
	if ( $backgroundContainerType == "video" ) {
		echo $bgVideoStyles;
		echo $bgVideo;
	}
}

/*
 * Function gcb_shape_top
 *
 * Adds a shape to the top of a GCB's container
 * MUST be called within each gcb file
 *
 */

function gcb_shapes_top() {
	/*Hero Shapes*/
	$showShapeTop   = get_field( 'overlay_shape_two_column_top' );
	$shapeTypeTop   = get_field( 'shape_type_top' );
	$shapeHeightTop = get_field( 'overlay_shape_height_top' );
	$shapeColor     = get_field( 'shape_color_top' );
	$shapeBgTop     = get_field( 'shape_color_top_bg' );
	$blockID        = get_the_ID();

	switch ( $shapeTypeTop ) {
		case 'ellipse':
			$shapeTop = '<svg class="shape" preserveAspectRatio="none" height="100%" width="100%" data-name="shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1691.92 103.49"><defs></defs><title>semi-circle</title><path class="d\" d="M129.13,258.1H1821s-331.48-103.49-846-103.49S129.13,258.1,129.13,258.1Z" transform="translate(-129.13 -154.6)"></path></svg>';

			break;

		case 'slant':
			$shapeTop = '<svg class="shape" data-name="shape" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px"
   viewBox="0 0 1920 148.3" style="enable-background:new 0 0 1920 148.3;" xml:space="preserve">
  <style type="text/css">
  .st0{display:none;}
  .st1{display:inline;}
  </style>
  <g id="Layer_1" class="st0">
  <ellipse class="st1" cx="964.5" cy="149" rx="985.4" ry="101.2"/>
  </g>
  <g id="Layer_2">

    <rect x="-17.8" y="71.9" transform="matrix(-0.9972 7.513312e-002 -7.513312e-002 -0.9972 2011.8533 259.8076)" class="st2" width="2037.6" height="191.8"/>
  </g>

  </svg>
  ';
			break;

		case 'wave':
			$shapeTop = '<svg class="shape" data-name="shape" preserveAspectRatio="none" height="100%" width="100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 30.62"><defs></defs><title>wave</title><path class="shape" d="M-126,96.5s97.67-29.58,243.67.08S345,106.67,374,97.33V114H-126V96.5Z" transform="translate(126 -83.38)" /></svg>';
			break;
		default:
			# code...
			break;
	}

	if ( $showShapeTop ) { ?>

		<style media="screen">
			.shape-top<?php echo $blockID; ?> svg {
				fill: <?php echo $shapeColor; ?>;
				height: <?php echo $shapeHeightTop ?>px;
				background-color: <?php echo $shapeBgTop ?>;
			}
		</style>

		<div class="shape-top<?php echo $blockID; ?> hidden-md-down" style="margin-top: -2px">

			<?php echo $shapeTop; ?>

		</div>

	<?php } ?>

<?php }


/*
 * Function gcb_shape_bottom
 *
 * Adds a shape to the top of a GCB's container
 * MUST be called within each gcb file
 * MUST add styles for
 *
 */
function gcb_shapes_bottom() {

	?>

	<?php


	$showShapeBottom   = get_field( 'overlay_shape_two_column_bottom' );
	$shapeTypeBottom   = get_field( 'shape_type_bottom' );
	$shapeHeightBottom = get_field( 'overlay_shape_height_bottom' );
	$shapeCustom       = get_field( 'custom_shape_svg' );
	$blockID           = get_the_ID();
	$shapeBgBottom     = get_field( 'shape_color_bottom' );

	switch ( $shapeTypeBottom ) {
		case 'ellipse':

			$shapeBottom = '<svg class="shape" preserveAspectRatio="none" height="100%" width="100%" data-name="shape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1691.92 103.49" transform="rotate(180)"><defs></defs><title>semi-circle</title><path class="d\" d="M129.13,258.1H1821s-331.48-103.49-846-103.49S129.13,258.1,129.13,258.1Z" transform="translate(-129.13 -154.6)"></path></svg>';

			break;

		case 'slant':

			$shapeBottom = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 1920 148.3" style="enable-background:new 0 0 1920 148.3;" xml:space="preserve" transform="rotate(180)">
  <style type="text/css">
  .st0{display:none;}
  .st1{display:inline;}
  </style>
  <g id="Layer_1" class="st0">
  <ellipse class="st1" cx="964.5" cy="149" rx="985.4" ry="101.2"/>
  </g>
  <g id="Layer_2">

    <rect x="-17.8" y="71.9" transform="matrix(-0.9972 7.513312e-002 -7.513312e-002 -0.9972 2011.8533 259.8076)" class="st2" width="2037.6" height="191.8"/>
  </g>
  </svg>';

			break;

		case 'wave':
			$shapeBottom = '<svg class="shape" data-name="shape" preserveAspectRatio="none" height="100%" width="100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 30.62"><defs></defs><title>wave</title><path class="shape" d="M-126,96.5s97.67-29.58,243.67.08S345,106.67,374,97.33V114H-126V96.5Z" transform="translate(126 -83.38)" /></svg>';
			break;

		case 'chevron' :
			$shapeBottom = '<svg class="shape" data-name="shape" preserveAspectRatio="none" height="100%" width="100%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 127.4" style="enable-background:new 0 0 1920 127.4;" xml:space="preserve">

	<style type="text/css">
		.st0 {
			display: none;
		}

		
	</style>
					<g id="Layer_1_1_" class="st0">
					</g>
					<g id="Layer_2">

						<rect x="440.4" y="-414.2" transform="matrix(0.1235 -0.9923 0.9923 0.1235 312.1011 652.6793)" class="st1" width="170.3" height="1127.7"></rect>

						<rect x="953" y="56.6" transform="matrix(0.9939 -0.1102 0.1102 0.9939 -6.482 170.4129)" class="st1" width="1170.9" height="174.5"></rect>
					</g>
	</svg>'; ?>

			<style>
				.shape-bottom<?php echo $blockID; ?> {
					/*position: absolute;*/
					bottom: 0;
					width: 100vw;
					z-index: 999;
				}
			</style>

			<?php break;
		case 'custom':
			$shapeBottom = $shapeCustom;
			break;
		default:
			# code...
			break;
	} ?>

	<?php if ( $showShapeBottom ) { ?>

		<style media="screen">


			.shape-bottom<?php echo $blockID; ?> svg {
				fill: <?php echo $shapeBgBottom; ?>;
				height: <?php echo $shapeHeightBottom ?>px;
			}

		</style>

		<div class="shape-bottom<?php echo $blockID; ?> hidden-md-down" style="margin-top: -2px">

			<?php echo $shapeBottom; ?>

		</div>

	<?php } ?>

	<?php

}