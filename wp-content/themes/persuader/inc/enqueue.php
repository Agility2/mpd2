<?php

//Add CSS
function dsPrelude_enqueue_style() {

	/*Font awesome*/
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );

	/*Bootstrap*/
	wp_enqueue_style( 'bootstrapCSS', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css' );

	/*Main stylesheet*/
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.min.css', false );

	/*Child stylesheet*/
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', false );
}

add_action( 'wp_enqueue_scripts', 'dsPrelude_enqueue_style' );

//Add Javascript
function dsPrelude_enqueue_script() {

	/*Slim*/
	wp_enqueue_script( 'jquery', ( "https://code.jquery.com/jquery-3.2.1.slim.min.js" ) );

	/*Popper*/
	wp_enqueue_script( 'popper', ( "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ) );

	/*jQuery UI*/
	wp_deregister_script( 'jquery-ui' );
	wp_register_script( 'jquery-ui', ( "//code.jquery.com/ui/1.11.4/jquery-ui.min.js" ), false, '1.11.4' );
	wp_enqueue_script( 'jquery-ui' );

	/*Bootstrap JS*/
	wp_enqueue_script( 'bootstrapJS', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js' );

	/*Vendors*/
	wp_enqueue_script( 'vendors', get_template_directory_uri() . '/assets/js/vendors.min.js', false );

	/*Custom Persuader*/
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/custom.min.js', false );

	/*Custom Child JS*/
	wp_enqueue_script( 'Ccustom', get_stylesheet_directory_uri() . '/assets/js/child.js', false );
}

add_action( 'wp_enqueue_scripts', 'dsPrelude_enqueue_script' );
