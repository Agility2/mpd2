<?php
//MENUS

 
function register_my_menus() {
	register_nav_menus(
		array(
			'utilityMenu' => __( 'Utility Menu' ),
			'primaryMenu' => __( 'Primary Menu' ),
			'primaryMobile' => __( 'Primary Mobile' ),
			'footerMenu'  => __( 'Footer Menu' )
		)
	);
}

add_action( 'init', 'register_my_menus' );

/*Changes the designstudio logo in the footer*/

function ds_logo_color() {

	$ds_logo_color = get_field( 'ds_logo_color', 'option' );

	switch ( $ds_logo_color ) {
		case 'white':
			$img = "<span> | Site Maintained by <a href='https://designstudio.com/'><img src='" . get_template_directory_uri() . "/assets/img/BUTTON-LOGO-WHITE.png'></a></span>";
			break;

		case 'black':
			$img = "<span> | Site Maintained by <a href='https://designstudio.com/'><img src='" . get_template_directory_uri() . "/assets/img/BUTTON-LOGO-BLACK.png'></a></span>";
			break;

		default:
			# code...
			break;
	}

	return $img;
}

function changeHex( $theColor, $theOpacity ) {
	if ( $theOpacity == "10" OR $theOpacity == "100" ) {
		$realOpacity = "10";
	} else {
		$realOpacity = $theOpacity;
	}
	$cNCRGB   = hex2rgb( $theColor );
	$theColor = 'rgba(' . $cNCRGB['red'] . ',' . $cNCRGB['green'] . ',' . $cNCRGB['blue'] . ',.' . $realOpacity . ')';

	return $theColor;
}

//update logo on wp-login

function my_login_logo() {
	$logoImg = get_field( 'headerLogo', 'option' );
	?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url('<?php echo $logoImg; ?>');
			padding-bottom: 30px;
			background-size: contain !important;
			width: auto !important;
		}
	</style>
<?php }

add_action( 'login_enqueue_scripts', 'my_login_logo' );

//Add featured images to theme
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	// additional image sizes
	// delete the next line if you do not need additional image sizes
	add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
}

if ( current_user_can( 'administrator' ) ) {
//Create General Options for Theme
	if ( function_exists( 'acf_add_options_page' ) ) {
		acf_add_options_page( array(
			'page_title' => 'General Options',
			'menu_title' => 'General Options',
			'menu_slug'  => 'options',
			'capability' => 'edit_posts',
			'redirect'   => false
		) );

	}
}

/*SVG Support*/
add_filter( 'upload_mimes', 'custom_upload_mimes' );


function custom_upload_mimes( $existing_mimes = array() ) {
	// add the file extension to the array
	$existing_mimes['svg'] = 'mime/type';

	// call the modified list of extensions
	return $existing_mimes;
}

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}

add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 *
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 *
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}

	return $urls;
}

/*Removing WP Embed script*/
function speed_stop_loading_wp_embed() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'wp-embed' );
	}
}

add_action( 'init', 'speed_stop_loading_wp_embed' );

function my_acf_add_local_field_groups() {
	add_filter( 'acf_the_content', 'wpautop' );
}

add_action( 'acf/init', 'my_acf_add_local_field_groups' );


// Allow SVG's in the Media Gallery
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
