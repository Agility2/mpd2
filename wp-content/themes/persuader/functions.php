<?php
/**
 * MPD2 functions and definitions
 *
 */

/**
 * Enqueue styles and scripts
 *
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Theme Setup
 *
 */
require get_template_directory() . '/inc/setup.php';

/**
 * Custom Post Types
 *
 */
require get_template_directory() . '/inc/register-custom-posts.php';

/**
 * General Options
 *
 */
require get_template_directory() . '/inc/general-options.php';

/**
 * Shortcodes
 *
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Woocommerce
 *
 */

if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}
/**
 * Posts
 *
 */
require get_template_directory() . '/inc/posts.php';

/**
 * Promotions
 *
 */
require get_template_directory() . '/inc/promotions.php';

/**
 * Careers
 *
 */
require get_template_directory() . '/inc/careers.php';

/**
 * Triggers
 *
 */
require get_template_directory() . '/inc/triggers.php';



