<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage DesignStudio Prelude
 * @since DS Prelude 1.0
 */
get_header();
global $wp_query;
?>


<?php

// Search Results Filter

function bac_filter_wp_search($query) {


  // remove list for the search. goes in the query args.
  $sitemapExcludes = get_field('sitemap_exclude');
  if($sitemapExcludes):
    foreach ($sitemapExcludes as $page ) {
      $pageString .= $page.', ';
    }
  endif;


    //Filter Search only in the front-end and Not in the Admin.
    if (!$query->is_admin && $query->is_search) {
    //Excludes specific Pages and Posts by ID. Separated by comma.
    $query->set('post__not_in', array($pageString));
    }
    return $query;
}
// add_filter('pre_get_posts','bac_filter_wp_search');


 ?>

<style media="screen">

  .search-item .info-col {
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  }

	.search-pagination {
		margin-top: 1rem;
	}

	body.search .search-title {
		margin-bottom: 15px;
		margin-top: 3rem;
	}

	.search-item img {
    max-height: 200px;
    margin: auto;
    display: block;
  }

	.search-items-container {
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
	}
	.search-item-title hr {
		margin-top: 0;
		margin-bottom: 25px;
	}

	nav#search-results-filter li {
		display: inline-block;
    padding: 10px 30px;
	}

	nav#search-results-filter li a {
		cursor: pointer;
	}

	.search-item.hide {
		display: none;
	}

	h3.search-title span.catName {
		margin-left: 3px;
		margin-right: 9px;

	}

	nav#search-results-filter li a.active {

	}

	nav#search-results-filter li {
		margin-bottom: 10px;
	}

	nav#search-results-filter {
		border-bottom: 1px solid #eee;
		margin-bottom: 50px;
	}


</style>
<div class="wrapper">
	<div class="contentarea clearfix">
		<div class="content search-results">
			<div class="container">

				<?php if ( have_posts() ) { ?>

				<div class="search-items-container row">

					<?php
					$postCat;
					$resultCats = [];
					$resultCatsNum = [];


					 ?>

					<?php while ( have_posts() ) {
						the_post(); ?>

						<?php// var_dump($post); ?>

						<?php
						$postType = $post->post_type;


							switch ($postType) {

								case 'page':
										$postCat = 'page';
										$postCatName = 'Pages';


									break;
								case 'post':
									$postCat = 'post';
									$postCatName = 'Posts';

									break;


								case 'product':
									$postCat = 'products';
									$postCatName = 'Products';
									break;


								default:
									# code...
									break;


							}

							$resultCatsNum[] = $postCat;

							if(!in_array($postCat, $resultCats)):
							 $resultCats[$postCat] = $postCatName;
						 endif;


							$resultTitle = get_the_title();
              $resultThumb = get_the_post_thumbnail_url();
              $resultExcerpt = get_the_excerpt();
							$resultLink = get_the_permalink();

              if ($resultThumb):
                $resultThumb = '<a href="'.$resultLink.'"><img class="img-fluid pt-5" src="'.$resultThumb.'"></a>';
              else:
                $resultThumb = '';
              endif;

							$resultsHTML .=

              '<div data-category="'.$postCat.'" class="search-item row">
                  <div class="col-md-4 col-lg-4">'
                    .$resultThumb.
                  '</div>
                  <div class="col-sm-12 col-md-8 info-col pt-5 pb-5">
                    <h4><a href="'.$resultLink.'">'.$resultTitle.'</a></h4>
                    <p class="post-excerpt">'.$resultExcerpt.'</p>
                    <div class="h-readmore">
                      <a class="font-weight-bold" href="'.$resultLink.'">Read More</a>
                    </div>
                  </div>
                </div>';

						?>




					<?php } ?>

					<?php $catsNums = array_count_values($resultCatsNum); ?>


					<div class="col-xs-12 text-lg-center">

								<h3 class="search-title"> <?php echo $wp_query->found_posts; ?>
									<?php _e( 'Search Results Found For', 'locale' ); ?>: "<?php the_search_query(); ?>" </h3>

									<!-- <span><span class="catName"></span></span> -->


						<nav id="search-results-filter">
						<ul>
							<li><a data-id="All" class="active">All</a></li>
						<?php
						foreach ($resultCats as $key => $value) {

							echo '<li><a data-id="'.$key.'">'. $value .'</a></li>';
						}
						 ?>
					 </ul>
				 </nav>
					</div>

          <div class="container">
					<?php echo $resultsHTML; ?>
          </div>



					<!-- <?php paginate_links(); ?> -->

					<?php
					global $wp_query;

					$big = 999999999; // need an unlikely integer
					echo '<div class="search-pagination col-xs-12 col-md-12 col-lg-5 offset-lg-1"><p>';
					echo paginate_links( array(
						'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format'  => '?paged=%#%',
						'current' => max( 1, get_query_var( 'paged' ) ),
						'total'   => $wp_query->max_num_pages
					) );
					?>
					</p></div>


			</div>


			<?php } ?>

		</div>
	</div>
</div>

<?php get_footer();


?>

<script type="text/javascript">
	$(document).ready(function(){
		$('nav#search-results-filter li a').on('click', function(){
			var catSlug = $(this).data('id');
			var catText = $(this).text();
			console.log(catSlug);
			$('nav#search-results-filter li a').removeClass('active');
			$('.search-title .catName').text(catText);
			$(this).addClass('active');

			// now add class hide to all search results without this slug
			$('.search-item').each(function(){
				if(catSlug == 'All'){
					$(this).removeClass('hide');
				} else {
					if($(this).data('category') != catSlug) {
						$(this).addClass('hide');
					} else {
						$(this).removeClass('hide');
					}
				}

			});

		});
	});
</script>
