<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage DesignStudio Persuader
 * @since DS Persuader 1.0
 */

get_header();
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1>Error: 404 | Page Not Found</h1>
		</div>
	</div>
</div>

<?php get_footer(); ?>
