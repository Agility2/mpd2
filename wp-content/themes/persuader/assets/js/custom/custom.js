/**
 * Custom JS.
 *
 * Custom JS scripts.
 *
 * @since 1.0.0
 */

(function ($) {

    $(document).ready(function () {
        // Hot Tubs Jets Coordinates Section
        $(".accordion-jets-container .elementor-accordion-item").mousedown(function (e) {
            if ($(window).width() > 1024) {
                var jetID = e.target.dataset.tab;
                for (i = 0; i < 10; i++) {

                    if (jetID == i) {

                        if ($('.jets-video-' + jetID).length) {

                            $('.jets-video-' + jetID)[0].currentTime = 0;
                            $('.jets-video')[0].pause();
                            $('.jets-video-' + jetID)[0].play();

                        } else {
                            $('.jets-video')[0].pause();
                        }

                        //enable icon on the spa and make it blink, jump and talk
                        $('a[id^=Circle]').hide();
                        $('a[id^=Circle_' + i + ']').show();
                    }
                }
            }

            if ($(window).width() <= 1024) {
                var jetID = e.target.dataset.tab;
                for (i = 0; i < 10; i++) {

                    if (jetID == i) {
                        $('a[id^=Circle]').hide();
                        $('a[id^=Circle_' + i + ']').show();
                    }
                }
            }
        });
    });
    /*Search modal in header*/
    $(document).ready(function () {
        $(function () {
            $('.header .search').on('click', function (event) {
                event.preventDefault();
                $('#search').addClass('open');
                $('#search > form > input[type="search"]').focus();
            });

            $('#search, #search button.close').on('click keyup', function (event) {
                if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
                    $(this).removeClass('open');
                }
            });

        });
    });
    /*End search modal*/

    /*Woocommerce shell & cabinet switcher*/

    $(document).ready(function () {

        var cabinetName = $(".cabinetImages .active img").data('cabinet');
        $('.cabinetNames span').text(cabinetName);

        var shellName = $(".shellImages .active img").data('shell');
        $('.shellNames span').text(shellName);

        // on click for cabinets
        $(".cabinetImages .swatch").on("click", function (e) {
            e.preventDefault();
            $('.cabinetImages .active').removeClass('active');
            $(this).addClass('active');
            var cabinet = $('img', this).data('cabinet');
            $('.cabinetNames span').text(cabinet);
            $(".shellImages").each(function () {
                var shell = $('.swatch', this);
                var shellfor = $(this).data('shellfor');
                $(this).hide();
                if (shellfor == cabinet) {
                    $(this).show();
                    $(shell).removeClass('active');
                    //reset to 1st shell
                    $(shell).first().addClass('active');
                    $('.spaTop img').hide();
                    $('.spaTop img').first().show();
                    var shellName = $("img", shell).data('shell');
                    $('.shellNames span').text(shellName);
                }
            });
            //show cabinet in product image so we do an each on all of them
            $('.spaBottom img').each(function () {
                var cabinetImg = $(this).data('cabinet');
                $(this).hide();
                if (cabinetImg == cabinet) {
                    $(this).show();
                }
            });
        });

        //on click for shells
        $(".shellImages .swatch").on("click", function (e) {
            e.preventDefault();
            $('.shellImages .active').removeClass('active');
            $(this).addClass('active');
            var shell = $('img', this).data('shell');
            $('.shellNames span').text(shell);

            //show shell in product image so we do an each on all of them
            $('.spaTop img').each(function () {
                var shellImg = $(this).data('shell');
                $(this).hide();
                if (shellImg == shell) {
                    $(this).show();
                }
            });
        });
    });

    /*End switcher*/

    /*Smoothscroll*/
    $('.smoothScroll').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000); // The number here represents the speed of the scroll in milliseconds
                return false;
            }
        }
    });

    /*End smoothscroll*/

    /*Woocommerce slider on hot tub product pages*/

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });

    $('.productThumb').click(function () {
        $('.modal-body').empty();
        $($(this).parents('div').html()).appendTo('.modal-body');
        $('#productGModal').modal({
            show: true
        });
    });

    /*end woocommerce hot tub slider*/


    // Sticky Nav
    if ($(window).width() > 768) {

        // grab the initial top offset of the navigation
        var stickyNavTop = $('header').height();

        // our function that decides weather the navigation bar should have "fixed" css position or not.
        var stickyNav = function () {
            var scrollTop = $(window).scrollTop(); // our current vertical position from the top
            // if we've scrolled more than the navigation, change its position to fixed to stick to top,
            // otherwise change it back to relative
            if (scrollTop > stickyNavTop) {
                $('.stickyNav').addClass('stick');
            } else {
                $('.stickyNav').removeClass('stick');
            }
        };

        stickyNav();
        // and run it again every time you scroll
        $(window).scroll(function () {
            stickyNav();

        });

    }

    // ANOTHER RIDICULOUS SUPERSCRIPT FUNCTION
    /*  $(function() {
     $('body :not(script)').contents().filter(function() {
     return this.nodeType === 3;
     }).replaceWith(function() {
     return this.nodeValue.replace(/[™®©]/g, '<sup class="tinySup">$&</sup>');
     });
     });
     */


    $('.search__close').on('click', function () {
        $('#search').removeClass('open');

        var animClass = $(this).data('animation');
        var removeTime = $(this).data('remove');
        if ($(this).hasClass(animClass)) {
            $(this).removeClass(animClass);
        } else {
            $(this).addClass(animClass);
            if (typeof removeTime != 'undefined') {
                var el = $(this);
                setTimeout(function () {
                    el.removeClass(animClass);
                }, removeTime);
            }
        }
    });

})(jQuery);

jQuery(document).ready(function ($) {

    var productViewer = function (element) {
        this.element = element;
        this.handleContainer = this.element.find('.cd-product-viewer-handle');
        this.handleFill = this.handleContainer.children('.fill');
        this.handle = this.handleContainer.children('.handle');
        this.imageWrapper = this.element.find('.product-viewer');
        this.slideShow = this.imageWrapper.children('.product-sprite');
        this.frames = this.element.data('frame');
        //increase this value to increase the friction while dragging on the image - it has to be bigger than zero
        this.friction = this.element.data('friction');
        this.visibleFrame = 0;
        this.loaded = false;
        this.animating = false;
        this.xPosition = 0;
        this.loadFrames();
    }

    productViewer.prototype.loadFrames = function () {
        var self = this,
            imageUrl = this.slideShow.data('image'),
            newImg = $('<img/>');
        this.loading('0.5');
        //you need this to check if the image sprite has been loaded
        newImg.load(function () {
            $(this).remove();
            self.loaded = true;
        });

        setTimeout(function () {
            newImg.attr('src', imageUrl)
        }, 50);
    }

    productViewer.prototype.loading = function (percentage) {
        var self = this;
        transformElement(this.handleFill, 'scaleX(' + percentage + ')');
        setTimeout(function () {
            if (self.loaded) {
                //sprite image has been loaded
                self.element.addClass('loaded');
                transformElement(self.handleFill, 'scaleX(1)');
                self.dragImage();
                if (self.handle) self.dragHandle();
            } else {
                //sprite image has not been loaded - increase self.handleFill scale value
                var newPercentage = parseFloat(percentage) + .1;
                if (newPercentage < 1) {
                    self.loading(newPercentage);
                }
            }
        }, 1500);
    }
    //draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
    productViewer.prototype.dragHandle = function () {
        //implement handle draggability
        var self = this;
        self.handle.on('mousedown vmousedown', function (e) {
            self.handle.addClass('cd-draggable');
            var dragWidth = self.handle.outerWidth(),
                containerOffset = self.handleContainer.offset().left,
                containerWidth = self.handleContainer.outerWidth(),
                minLeft = containerOffset - dragWidth / 2,
                maxLeft = containerOffset + containerWidth - dragWidth / 2;

            self.xPosition = self.handle.offset().left + dragWidth - e.pageX;

            self.element.on('mousemove vmousemove', function (e) {
                if (!self.animating) {
                    self.animating = true;
                    (!window.requestAnimationFrame) ?
                        setTimeout(function () {
                            self.animateDraggedHandle(e, dragWidth, containerOffset, containerWidth, minLeft, maxLeft);
                        }, 100) : requestAnimationFrame(function () {
                        self.animateDraggedHandle(e, dragWidth, containerOffset, containerWidth, minLeft, maxLeft);
                    });
                }
            }).one('mouseup vmouseup', function (e) {
                self.handle.removeClass('cd-draggable');
                self.element.off('mousemove vmousemove');
            });

            e.preventDefault();

        }).on('mouseup vmouseup', function (e) {
            self.handle.removeClass('cd-draggable');
        });
    }

    productViewer.prototype.animateDraggedHandle = function (e, dragWidth, containerOffset, containerWidth, minLeft, maxLeft) {
        var self = this;
        var leftValue = e.pageX + self.xPosition - dragWidth;
        // constrain the draggable element to move inside his container
        if (leftValue < minLeft) {
            leftValue = minLeft;
        } else if (leftValue > maxLeft) {
            leftValue = maxLeft;
        }

        var widthValue = Math.ceil((leftValue + dragWidth / 2 - containerOffset) * 1000 / containerWidth) / 10;
        self.visibleFrame = Math.ceil((widthValue * (self.frames - 1)) / 100);

        //update image frame
        self.updateFrame();
        //update handle position
        $('.cd-draggable', self.handleContainer).css('left', widthValue + '%').one('mouseup vmouseup', function () {
            $(this).removeClass('cd-draggable');
        });

        self.animating = false;
    }

    productViewer.prototype.dragImage = function () {
        //implement image draggability
        var self = this;
        self.slideShow.on('mousedown vmousedown', function (e) {
            self.slideShow.addClass('cd-draggable');
            var containerOffset = self.imageWrapper.offset().left,
                containerWidth = self.imageWrapper.outerWidth(),
                minFrame = 0,
                maxFrame = self.frames - 1;

            self.xPosition = e.pageX;

            self.element.on('mousemove vmousemove', function (e) {
                if (!self.animating) {
                    self.animating = true;
                    (!window.requestAnimationFrame) ?
                        setTimeout(function () {
                            self.animateDraggedImage(e, containerOffset, containerWidth);
                        }, 100) : requestAnimationFrame(function () {
                        self.animateDraggedImage(e, containerOffset, containerWidth);
                    });
                }
            }).one('mouseup vmouseup', function (e) {
                self.slideShow.removeClass('cd-draggable');
                self.element.off('mousemove vmousemove');
                self.updateHandle();
            });

            e.preventDefault();

        }).on('mouseup vmouseup', function (e) {
            self.slideShow.removeClass('cd-draggable');
        });
    }

    productViewer.prototype.animateDraggedImage = function (e, containerOffset, containerWidth) {
        var self = this;
        var leftValue = self.xPosition - e.pageX;
        var widthValue = Math.ceil((leftValue) * 100 / (containerWidth * self.friction));
        var frame = (widthValue * (self.frames - 1)) / 100;
        if (frame > 0) {
            frame = Math.floor(frame);
        } else {
            frame = Math.ceil(frame);
        }
        var newFrame = self.visibleFrame + frame;

        if (newFrame < 0) {
            newFrame = self.frames - 1;
        } else if (newFrame > self.frames - 1) {
            newFrame = 0;
        }

        if (newFrame != self.visibleFrame) {
            self.visibleFrame = newFrame;
            self.updateFrame();
            self.xPosition = e.pageX;
        }

        self.animating = false;
    }

    productViewer.prototype.updateHandle = function () {
        if (this.handle) {
            var widthValue = 100 * this.visibleFrame / this.frames;
            this.handle.animate({
                'left': widthValue + '%'
            }, 200);
        }
    }

    productViewer.prototype.updateFrame = function () {
        var transformValue = -(100 * this.visibleFrame / this.frames);
        transformElement(this.slideShow, 'translateX(' + transformValue + '%)');
    }

    function transformElement(element, value) {
        element.css({
            '-moz-transform': value,
            '-webkit-transform': value,
            '-ms-transform': value,
            '-o-transform': value,
            'transform': value,
        });
    }

    var productToursWrapper = $('.cd-product-viewer-wrapper');
    productToursWrapper.each(function () {
        new productViewer($(this));
    });


});
