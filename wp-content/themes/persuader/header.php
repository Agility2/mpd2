<?php
header( 'Access-Control-Allow-Origin: *' );
/**
 * The template for displaying the header
 *
 * Displays all of the head element up to the switcher for headers
 *
 * PERSUADER DESIGNSTUDIO THEME
 */
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<title><?php wp_title( '' ); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>


	<!-- Favicon -->
	<?php
	//variables
	$favIcon        = get_field( 'favIcon', 'option' );
	$favApple       = get_field( 'favApple', 'option' );
	$fav32          = get_field( 'fav32', 'option' );
	$fav16          = get_field( 'fav16', 'option' );
	$favSafari      = get_field( 'favSafari', 'option' );
	$favSafariColor = get_field( 'favSafariColor', 'option' );
	?>

	<link rel="icon" href="<?php echo $favIcon; ?>" type="image/x-icon"/>
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $favApple; ?>">
	<link rel="icon" type="image/png" href="<?php echo $fav32; ?>" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo $fav16; ?>" sizes="16x16">

	<link rel="mask-icon" href="<?php echo $favSafari; ?>" color="<?php echo $favSafariColor; ?>">
	<meta name="theme-color" content="#ffffff">


	<?php
	$GACode     = get_field( 'gaNumber', 'option' );
	$customCode = get_field( 'customCode', 'option' );
	?>

	<!-- Google Analytics -->
	<?php if ( $GACode ) { ?>
		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

			ga('create', '<?php echo $GACode; ?>', 'auto');
			ga('send', 'pageview');
		</script>
		<!-- End Google Analytics -->

	<?php } ?>

	<!-- Other Javascript Embeded -->
	<?php echo $customCode; ?>

</head>
<?php
/* check if promo is needed */
$pageID = get_the_ID();
echo promoDisplay( $pageID );
?>

<body <?php body_class(); ?>>
<a class="anchorLink currentAnchorLink firstAnchorLink elementor-menu-anchor" id="top"></a>
