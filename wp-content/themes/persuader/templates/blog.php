<?php
/**
 * Template Name: Blog Template
 *
 * @package WordPress
 * @subpackage DesignStudio Prelude
 * @since DS Prelude 1.0
 */
get_header();

?>

<?php the_content(); ?>


<div class="container filter-search mt-3">
	<div class="row justify-content-center">
		<!-- Filters -->
		<div class="col-12 col-sm-4 d-flex justify-content-sm-end align-items-center">

			<div class="dropdown">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
				        aria-haspopup="true" aria-expanded="false">
					Select Category
				</button>
				<div class="dropdown-menu" id="alm-filter-nav">
					<a class="dropdown-item" href="#" data-repeater="default" data-post-type="post"
					   data-category=""
					   data-posts-per-page="8" data-scroll="true" data-button-label="Load More">All
						Categories</a>

					<?php

					$categories = get_categories( array(
						'orderby' => 'name',
						'parent'  => 0
					) );

					foreach ( $categories as $category ) { ?>

						<a class="dropdown-item" href="#" data-repeater="default" data-post-type="post"
						   data-category="<?php echo $category->slug; ?>" data-posts-per-page="8"
						   data-scroll="true"><?php echo $category->name; ?></a>
					<?php } ?>

				</div>
			</div>
		</div>
		<div class="col-12 col-sm-4 mt-3 mt-sm-0">
			<div class="searchBox d-flex justify-content-sm-start align-items-center">
				<form method="get" action="">
					<input class="text-center" type="text" value="" placeholder="SEARCH" id="term" name="term">
				</form>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="bPostWrapper col-12 text-center pt-3">
			<?php
			// This is for blog category links to filter using the ajax load more. We don't want to use the standard blog category wordpress pages.
			$url = $_SERVER["REQUEST_URI"];
			if ( strpos( $url, '?category=' ) == true ) {
				$parts = parse_url( $url );
				parse_str( $parts['query'], $query );
				$queryCat = $query['category'];

				echo '<h3 class="bPostMessage">Displaying posts for the "' . $queryCat . '" category</h3>';

			} else if ( strpos( $url, '?tag=' ) == true ) {
				$parts = parse_url( $url );
				parse_str( $parts['query'], $query );
				$queryTag = $query['tag'];

				echo '<h3 class="bPostMessage">Displaying posts for the "' . $queryTag . '" category</h3>';

			} else if ( $_GET ) {
				$term = $_GET['term'];
				echo '<h3 class="bPostMessage">Displaying posts for "' . $term . '"</h3>';
			} else {
				$term = "";
			}
			?>
		</div>
	</div>
</div>

<!--Ajax shortcode-->
<div class="container blog-container d-flex justify-content-center align-items-center">
	<div class="row">
		<?php echo do_shortcode( '[ajax_load_more id="blog-temp" post_type="post" posts_per_page="6" scroll_distance="" max_pages="3" button_label="More Posts"]' ); ?>
	</div>
</div>

<script>
	(function ($) {
		// Blog Template Scripts
		$(document).ready(function () {

			// This beautiful function detects the category select dropdown and changes the message to the category being displayed.
			$(".dropdown-item").click(function () {
				var $bPostMessage = $(".bPostMessage");
				var catName = $(this).text();
				console.log(catName)
				if ($(".bPostMessage").length < 1) {
					console.log('no post message');
					var postMessage = '<h3 class="bPostMessage">Displaying posts for the "' + catName + '" category</h3>';
					$(postMessage).appendTo('.bPostWrapper');
				} else {
					$('.bPostMessage').replaceWith('<h3 class="bPostMessage">Displaying posts for the "' + catName + '" category</h3>');
				}

			});

			$(function () {

				// Filter Ajax Load More
				var alm_is_animating = false;
				$('#alm-filter-nav a').eq(0).addClass('active'); // Set the initial button active state

				// Nav btn click event
				$('#alm-filter-nav a').on('click', function (e) {
					e.preventDefault();
					var el = $(this); // Our selected element

					if (!el.hasClass('active') && !alm_is_animating) { // Check for active and !alm_is_animating
						alm_is_animating = true;
						el.addClass('active');
						el.siblings().removeClass('active'); // Add active state

						var data = el.data(), // Get data values from selected menu item
							transition = 'fade', // 'slide' | 'fade' | null
							speed = '300'; //in milliseconds

						$.fn.almFilter(transition, speed, data); // reset Ajax Load More (transition, speed, data)
					}
				});

				$.fn.almFilterComplete = function () {
					alm_is_animating = false; // clear alm_isanimating flag
				};

			});

		});

	})(jQuery);
</script>
<?php
//footer.php - includes the footer switcher and version
get_footer();
?>
