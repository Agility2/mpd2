<?php
/**
 * Template Name: Promotions
 *
 * @package WordPress
 * @subpackage DesignStudio Prelude
 * @since DS Prelude 1.0
 */
get_header();
?>
<?php the_content(); ?>

<!-- Container Div -->
<div class="container">

<!-- Display Promotions from the Promotions custom post type -->
<?php

// Promotions loop get from arrange promotions
$promotionsIDs = get_field('arrangePromotions');
if(!$promotionsIDs) {
// Today's date to figure out what to display
$today = date("m/d/Y");
// Args for promotions only get the valid promotions that are current depending on date.
$args = array(
    'post_type' => 'promotions',
    'posts_per_page' => -1,

);
$promotions = new WP_Query( $args );
// The Loop
if ( $promotions->have_posts() ) {
	while ( $promotions->have_posts() ) {
		$promotions->the_post();
        //get the ids to do some magic later
        $promotionsIDs[] .= get_the_ID();
    }
}
}
// Reset columns for magic
$column = 0;

// Let the magic begin
foreach($promotionsIDs as $key => $promoID) {
        // What column are we in
        $column ++; // starts at 1 and resets at 2

        // Prev PromoSize
        $prevPromo = $promotionsIDs[$key -1];
        if($prevPromo) {
        $prevPromoSize = get_field('promoMainSize' , $prevPromo);
        }

        // Present Promo Size
        $presentPromo = $promotionsIDs[$key];
        $presentPromoSize = get_field('promoMainSize', $presentPromo);

        // Next Promotion Size
        $nextPromo = $promotionsIDs[$key +1];
        if($nextPromo) {
         $nextPromoSize = get_field('promoMainSize', $nextPromo);
        }

        // Create filler content if is nesessary
        // How do we know if its nesessary to create filler content?
        // If prev promo is small and the present one is large then we need
        // A filler to have  [ small ] [ filler ] instead of [ small ] [     large    ]
        // I also need to know if this is a new row or to complete a row
        // [present promo ] [next promo]  OR  [ prev promo ] [ present promo ]
        // We check our variable column to see that

        // If this is the 1st column then we make sure if its a large column it takes 2 columns
        if($column == 1) {
                // Target large columns
            if($presentPromoSize == 2 OR $presentPromoSize == 4) {
                // Set column as 2 so the next one starts at 1 again
                $column = 2;

                // If is medium display the medium size promo (check functions on the bottom)
                if($presentPromoSize == 2) {
                  mediumPromo($presentPromo);
                }
                // If is large display the large size promo (check functions on the bottom)
                if($presentPromoSize == 4) {
                  largePromo($presentPromo);
                }

            }  else {
                // Start row since its a small promo and is the start of the column
                echo "<div class='row'>";
                // Display small size promo (check functions on the bottom)
                smallPromo($presentPromo);

            }
            // Display fillerPromo if the last promo is size 1 just to complete the grid
            if($presentPromoSize == 1 AND !$nextPromo) {
              fillerPromo($presentPromo);
            }

            // Now lets check if this is column 2
            } else if ($column == 2) {

            // If this is column 2 we need to make sure the next one is not a large one
            // If so then we add a filler so the large one goes to another row
            if(($presentPromoSize == 2 AND $prevPromoSize == 1) OR
            ($presentPromoSize == 4 AND $prevPromoSize == 1)) {

                    // Here it means we might have a problem so lets add a filler
                    fillerPromo($presentPromo);

                    // Now display the next promotion on different row
                    // If is medium display the medium size promo (check functions on the bottom)
                    if($presentPromoSize == 2) {
                      mediumPromo($presentPromo);
                    }
                    // If is large display the large size promo (check functions on the bottom)
                    if($presentPromoSize == 4) {
                      largePromo($presentPromo);
                    }
                    // If its not a large one then continue using the small column
             } else {
                    // Display small size promo (check functions on the bottom)
                    smallPromo($presentPromo);
                    // Close row since this is column 2
                    echo "</div>";
             }

        }

         if ($column == 2) {
            // If this is column 2 reset it to 0  since the next one needs to be a new row!
            $column = 0;
        }
}
// Fin  ?>
<!-- End Container -->
</div></div>
<br>

<?php
get_footer();



 //Functions for promos
 function smallPromo($presentPromo) {
   //get promo fields for small and medium
 // Background Image
 $promoBackgroundImage = get_field('promoBackgroundImage', $presentPromo);

 // Title
 $promoTitle = get_the_title($presentPromo);
 $promoTitleColor = get_field('promoTitleColor', $presentPromo);
 // Description
 $promoDescription = get_field('promoDescription', $presentPromo);

$promoPricingDescription = get_field('promoPricingDescription', $presentPromo);
$promoPricingNumber = get_field('promoPricingNumber', $presentPromo);
$promoPricingSub = get_field('promoPricingSub', $presentPromo);
$promoPricingAdditionSub = get_field('promoPricingAdditionSub', $presentPromo);
$promoPricingDescriptionColor = get_field('promoPricingDescriptionColor', $presentPromo);
$promoPricingNumberColor = get_field('promoPricingNumberColor', $presentPromo);
$promoPricingSubColor = get_field('promoPricingSubColor', $presentPromo);
$promoPricingAdditionSubColor = get_field('promoPricingAdditionSubColor', $presentPromo);


$promoPricingDescriptionLength = mb_strlen($promoPricingDescription);
$promoPricingNumberLength = mb_strlen($promoPricingNumber);
$promoPricingSubLength = mb_strlen($promoPricingSub);
$promoPricingAdditionSubLength = mb_strlen($promoPricingAdditionSub);

if($promoPricingDescriptionLength <= 25 AND $promoPricingDescriptionLength >= 17) {
  $pdSize = 20;
} elseif ($promoPricingDescriptionLength <= 16 AND $promoPricingDescriptionLength >= 8) {
  $pdSize = 30;
} elseif ($promoPricingDescriptionLength <= 7) {
  $pdSize = 50;
}
$pdSizeLine = $pdSize + 5;


  if($promoPricingNumberLength <= 25 AND $promoPricingNumberLength >= 17) {
    $pnSize = 20;
  } elseif ($promoPricingNumberLength <= 16 AND $promoPricingNumberLength >= 8) {
    $pnSize = 30;
  } elseif ($promoPricingNumberLength <= 7) {
    $pnSize = 65;
  }

switch(true) {
  case $promoPricingSubLength >= 5:
    $psSize = 25;
  break;
  case $promoPricingSubLength <= 4:
    $psSize = 65;
  break;
}

?>
<style>
#promo<?php echo $presentPromo; ?> .pricingDes {
  font-size:<?php echo $pdSize; ?>px;
  line-height:<?php echo $pdSize; ?>px;
  color:<?php echo $promoPricingDescriptionColor; ?>;
  font-weight:bold;
  width:75%;
  text-align:center;
}

#promo<?php echo $presentPromo; ?> .pricingNum {
  font-size:<?php echo $pnSize; ?>px;
  line-height:<?php echo $pnSize + 5; ?>px;
  color:<?php echo $promoPricingNumberColor; ?>;
  font-weight:bold;
  width:75%;
  text-align:center;
}
#promo<?php echo $presentPromo; ?> .pricingSub {
  font-size:<?php echo $psSize; ?>px;
  line-height:<?php echo $psSize + 5; ?>px;
  color:<?php echo $promoPricingSubColor; ?>;
  font-weight:bold;
  width:75%;
  text-align:center;
}
#promo<?php echo $presentPromo; ?> .princingASub {
  width:75%;
  text-align:center;
  color:<?php echo $promoPricingAdditionSubColor; ?>;
}
#promo<?php echo $presentPromo; ?> .pHeader h2 {
  color:<?php echo $promoTitleColor; ?>;
}

@media screen and (max-width:680px) {
  #promo<?php echo $presentPromo; ?> .pricingDes, #promo<?php echo $presentPromo; ?> .pricingNum,  #promo<?php echo $presentPromo; ?> .pricingSub, #promo<?php echo $presentPromo; ?> .princingASub{
    font-size:20px;
    line-height:20px;
    text-align:left;
    width:55%;
  }
}
</style>
    <div id="promo<?php echo $presentPromo; ?>" class="col-md-6">
     <div class="ppanel" style="background:url('<?php echo $promoBackgroundImage; ?>'); background-size:cover; background-position:center center;  width:100%;">
     <div class='pHeader'>
       <h2><?php echo $promoTitle; ?></h2>
       <p><?php echo $promoDescription; ?></p>
     </div>
<?php 
$pricingArray = [$promoPricingDescription, $promoPricingNumber, $promoPricingSub, $promoPricingAdditionSub];
$validPrice = false;

foreach ($pricingArray as $pricing) {
    if(!empty($pricing)) {
      $validPricing = true;
    }
}

if($validPricing) {  
  ?>
     <div class="pPrice">
     <div class="pricingDes"><?php echo $promoPricingDescription; ?></div>
     <div class="pricingNum"><?php echo $promoPricingNumber; ?></div>
    <div class="pricingSub"><?php echo $promoPricingSub; ?></div>
    <div class="princingASub"><?php echo $promoPricingAdditionSub; ?></div>
    </div>
    <?php } ?>
</div>
<?php if($validPricing) {   ?>
<div class="pPriceMobile">
<div class="pricingDes"><?php echo $promoPricingDescription; ?></div>
<div class="pricingNum"><?php echo $promoPricingNumber; ?></div>
<div class="pricingSub"><?php echo $promoPricingSub; ?></div>
<div class="princingASub"><?php echo $promoPricingAdditionSub; ?></div>
</div>
<?php } ?>


</div>


<?php  }

 function mediumPromo($presentPromo) {
  //get promo fields for small and medium
 // Background Image
 $promoBackgroundImage = get_field('promoBackgroundImage', $presentPromo);

 // Title
 $promoTitle = get_the_title($presentPromo);
 $promoTitleColor = get_field('promoTitleColor', $presentPromo);
 // Description
 $promoDescription = get_field('promoDescription', $presentPromo);

$promoPricingDescription = get_field('promoPricingDescription', $presentPromo);
$promoPricingNumber = get_field('promoPricingNumber', $presentPromo);
$promoPricingSub = get_field('promoPricingSub', $presentPromo);
$promoPricingAdditionSub = get_field('promoPricingAdditionSub', $presentPromo);
$promoPricingDescriptionColor = get_field('promoPricingDescriptionColor', $presentPromo);
$promoPricingNumberColor = get_field('promoPricingNumberColor', $presentPromo);
$promoPricingSubColor = get_field('promoPricingSubColor', $presentPromo);
$promoPricingAdditionSubColor = get_field('promoPricingAdditionSubColor', $presentPromo);


$promoPricingDescriptionLength = mb_strlen($promoPricingDescription);
$promoPricingNumberLength = mb_strlen($promoPricingNumber);
$promoPricingSubLength = mb_strlen($promoPricingSub);
$promoPricingAdditionSubLength = mb_strlen($promoPricingAdditionSub);

if($promoPricingDescriptionLength <= 25 AND $promoPricingDescriptionLength >= 17) {
  $pdSize = 20;
} elseif ($promoPricingDescriptionLength <= 16 AND $promoPricingDescriptionLength >= 8) {
  $pdSize = 30;
} elseif ($promoPricingDescriptionLength <= 7) {
  $pdSize = 50;
}
$pdSizeLine = $pdSize + 5;


  if($promoPricingNumberLength <= 25 AND $promoPricingNumberLength >= 17) {
    $pnSize = 20;
  } elseif ($promoPricingNumberLength <= 16 AND $promoPricingNumberLength >= 8) {
    $pnSize = 30;
  } elseif ($promoPricingNumberLength <= 7) {
    $pnSize = 65;
  }

switch(true) {
  case $promoPricingSubLength >= 5:
    $psSize = 25;
  break;
  case $promoPricingSubLength <= 4:
    $psSize = 65;
  break;
}

?>
<style>
#promo<?php echo $presentPromo; ?> .pricingDes {
  font-size:<?php echo $pdSize; ?>px;
  line-height:<?php echo $pdSize; ?>px;
  color:<?php echo $promoPricingDescriptionColor; ?>;
  font-weight:bold;
  width:100%;
  text-align:center;
}

#promo<?php echo $presentPromo; ?> .pricingNum {
  font-size:<?php echo $pnSize; ?>px;
  line-height:<?php echo $pnSize + 5; ?>px;
  color:<?php echo $promoPricingNumberColor; ?>;
  font-weight:bold;
  width:100%;
  text-align:center;
}
#promo<?php echo $presentPromo; ?> .pricingSub {
  font-size:<?php echo $psSize; ?>px;
  line-height:<?php echo $psSize + 5; ?>px;
  color:<?php echo $promoPricingSubColor; ?>;
  font-weight:bold;
  width:100%;
  text-align:center;
}
#promo<?php echo $presentPromo; ?> .princingASub {
  width:100%;
  text-align:center;
  color:<?php echo $promoPricingAdditionSubColor; ?>;
}
#promo<?php echo $presentPromo; ?> h2 {
  color:<?php echo $promoTitleColor; ?>;
}

@media screen and (max-width:680px) {
  #promo<?php echo $presentPromo; ?> .pricingDes, #promo<?php echo $presentPromo; ?> .pricingNum,  #promo<?php echo $presentPromo; ?> .pricingSub, #promo<?php echo $presentPromo; ?> .princingASub{
    font-size:20px;
    line-height:20px;
    text-align:left;
    width:55%;
  }
}
</style>

   <div class="row">
     <div id="promo<?php echo $presentPromo; ?>" class="col-md-12">

       <div class="ppanel mediumPromo" style="background:url('<?php echo $promoBackgroundImage; ?>'); background-size:cover; background-position:center center;  width:100%;">
         <div class="promoTitleMobile pHeader">
           <h2 style="color: "><?php echo $promoTitle; ?></h2>
          <?php echo $promoDescription; ?>
         </div>
          <div class="promoContent">

         <div class='pLeftBanner'>
           <h2><?php echo $promoTitle; ?></h2>
          <?php echo $promoDescription; ?>
         </div>

         <div class="pCirclePrice">
           <div class="pCirclePriceContent">
            <div class="pricingDes"><?php echo $promoPricingDescription; ?></div>
             <div class="pricingNum"><?php echo $promoPricingNumber; ?></div>
            <div class="pricingSub"><?php echo $promoPricingSub; ?></div>
            <div class="princingASub"><?php echo $promoPricingAdditionSub; ?></div>
         </div></div>
       </div>
       </div>

<div class="promoContentMobile">
 <div class="pricingDes"><?php echo $promoPricingDescription; ?></div>
  <div class="pricingNum"><?php echo $promoPricingNumber; ?></div>
 <div class="pricingSub"><?php echo $promoPricingSub; ?></div>
 <div class="princingASub"><?php echo $promoPricingAdditionSub; ?></div>
</div>

       </div>
       </div>


 <?php }

 function largePromo($presentPromo) {
    $permaLink = get_field('promoLink', $presentPromo);
    $largeTypeofImg = get_field('lpTypeImg', $presentPromo);
    if($largeTypeofImg == "media") {
      $largeImg = get_field('lpMLImage', $presentPromo);
    }

    if($largeTypeofImg == "external") {
      $largeImg = get_field('lpEImg', $presentPromo);
    }
?>

   <div class="row">
     <div id="promo<?php echo $presentPromo; ?>" class="col-md-12">
      <a href="<?php echo $permaLink; ?>"><img src="<?php echo $largeImg; ?>" alt="Large Promo" style="width:100%;"></a>
     </div>
   </div>

 <?php }

 function fillerPromo($presentPromo) {
    // Add filler content ?>
    <div class="col-md-6 filterPromo promo<?php echo $presentPromo; ?>">
      <div class="ppanel" style="background:url('<?php echo get_stylesheet_directory_uri(); ?>/assets/img/extra.jpg'); background-size:cover; background-position:center right;  width:100%;">

    </div>
      </div>
</div> <!-- ends row -->
  <?php } ?>



<style>
.ppanel {
    background:black;
    margin-top:30px;
    color:white;
    font-size:1rem;
    height:455px;
    overflow:hidden;
    position:relative;
}

.filterPromo {
  display:block;
}
@media screen and (max-width:760px) {
  .filterPromo {
    display:none;
  }
}
@media screen and (max-width:680px) {
  .ppanel {
    height:300px;
  }
}

.pHeader {
  background: -moz-linear-gradient(top,  rgba(0,0,0,0.85) 0%, rgba(0,0,0,0.24) 72%, rgba(125,185,232,0) 100%); /* FF3.6-15 */
  background: -webkit-linear-gradient(top,  rgba(0,0,0,0.85) 0%,rgba(0,0,0,0.24) 72%,rgba(125,185,232,0) 100%); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(to bottom,  rgba(0,0,0,0.85) 0%,rgba(0,0,0,0.24) 72%,rgba(125,185,232,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d9000000', endColorstr='#007db9e8',GradientType=0 ); /* IE6-9 */
  height:50%;
  padding:15px 30px;

}

.pHeader h2, .pLeftBanner h2 {
  margin:0;
}
.pHeader p {
  width:53%;
  margin:0;
}

.pPrice {
  background:black;
  padding-left:55px;
  padding-top:40px;
  border-radius:50%;
  width:320px;
  height:320px;
  position:absolute;
  bottom:-60px;
  right:-60px;
}

.promoContent {
  position:absolute;
  top:0;
  left:0;
  right:0;
  bottom:0;
  margin:auto;
  width:760px;
  padding-top:100px;
}
.pLeftBanner {
  background:#1b7142;
  position:relative;
  height:180px;
  width:500px;
  padding:15px 60px 15px 30px;
  display:inline-block;
  vertical-align: middle;
}
.pLeftBanner h2 {
  margin-bottom:0;
}

.pCirclePrice {
  position:relative;
  background:black;
  padding-left:0px;
  padding-top:0px;
  border-radius:50%;
  width:250px;
  height:250px;
  display:inline-block;
  vertical-align:middle;
  left:-50px;
  }

  .pCirclePriceContent {
    text-align:center;
    margin:13% auto;
  }
  .pPriceMobile {
    display:none;
    text-align:center !important;
  }
  .promoContentMobile {
    display:none;
  }

      .promoTitleMobile {
          display:none;
      }

  @media screen and (max-width:992px) {
        .pHeader p, .pHeader h2 {
           width:98% !important;
        }
  }
  @media screen and (max-width:680px) {
    .ppanel {
      height:300px;
    }
    .pPrice {
      display:none;
    }
    .pPriceMobile {
      display:block;
      background:black;
      width:100%;
      height:auto;
      padding:15px;
    }
    .pHeader p {
      width:100% !important;
    }
    .promoContent {
      display:none;
    }
    .promoContentMobile {
      display:block;
      background:black;
      width:100%;
      padding:15px;
      position:relative;
      margin:0 auto;
    }
    .promoContentMobile h2, .promoContentMobile p {
       color:white;
    }
    .pricingDes, .pricingNum, .pricingSub, .pricingASub {
      width:95% !important;
    }
    .promoTitleMobile {
        display:block;
    }

  }

@media screen and (max-width:992px) {
  .container {
    padding: 10px !important;
  }
}

</style>

<?php promoCronJob(); ?>

<?php get_footer(); ?>
