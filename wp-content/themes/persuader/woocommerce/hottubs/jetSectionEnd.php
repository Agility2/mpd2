<div id="stopSpa"><br/></div>
<script>
	jQuery(function ($) {
		$('#spaPic').sticky({
			'offset': 100,
			stopper: '#stopSpa'
		});
		$(document).ready(function () {
			$('[id^=jetSystemcollapse]').collapse();
			$('[id^=jetSystemcollapse]').addClass('collapsed');
			$('[id^=jetSystemcollapse1]').addClass('collapse in');

			var $myGroup = $('#jetSystem');
			$myGroup.on('show.bs.collapse', '.collapse', function () {
				$myGroup.find('.collapse.in').collapse('hide');
			});

		});
	});
	$(function () {
		if ($(window).width() > 1024) {
			$(".collapse").on('show.bs.collapse', function (e) {
				if ($(this).is(e.target)) {
					var jetID = this.id;
					console.log(jetID);
					for (i = 0; i < 10; i++) {

						if (jetID == "jetSystemcollapse" + i) {
							if ($('[id^=video' + i + ']').length) {

								$('[id^=video' + i + ']')[0].currentTime = 0;
								$('[id^=video]')[0].pause();
								$('[id^=video' + i + ']')[0].play();
								console.log('video ' + i + ' playing');

							} else {
								// $('[id^=video]')[0].pause();
							}
							//enable icon on the spa and make it blink, jump and talk
							$('image[id^=Circle]').hide();
							$('image[id^=Circle_' + i + ']').show();
						}

					}

				}
			});
		}
	});


</script>

</div>
