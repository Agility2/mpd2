<?php
/**
 * The template for displaying product content in the single-product.php template for MPD2
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */ ?>

<?php
$image  = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );
$pageID = $post->ID;
$terms  = get_the_terms( $post->ID, 'product_cat' );
foreach ( $terms as $term ) {
	$categories[] = $term->slug;
}
?>
<div itemtype="<?php echo woocommerce_get_product_schema(); ?>"
     id="product-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?>>
	<?php if ( $image ) { ?>
		<div class="container mt-4 mb-4">
			<div class="row">
				<div class="col-sm-12 col-md-5">
					<?php do_action( 'woocommerce_before_single_product_summary' ); ?>
				</div>
				<div class="elementor-widget-heading elementor-widget-text-editor col-sm-12 col-md-7 mb-md-0 mt-2 mt-md-0">
					<?php do_action( 'woocommerce_single_product_summary' );
					?>
					<?php if ( ( in_array( "endless-pools-fitness-systems", $categories ) ) ) { ?>
						<a href="<?php the_field( 'get_pricing_endless' ); ?>" class="btn btn-primary">Get
							Pricing</a>

						<a href="<?php the_field( 'download_brochure_endless' ); ?>" class="btn btn-primary">Download
							Brochure</a>
					<?php } else { ?>
						<a href="/product-inquiry/?product_name_field=<?php the_title(); ?>" class="btn btn-primary">Get
							Pricing</a>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>

<?php the_content(); ?>

