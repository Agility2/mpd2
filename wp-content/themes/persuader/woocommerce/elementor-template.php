<?php
/*
 * Template Name: Elementor
 * Template Post Type: product
 */

get_header(); ?>
<?php
/*Template for MPD2*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>


	<?php the_content(); ?>


	<!--Structured Data-->
	<?php
	global $post;
	$terms = get_the_terms( $post->ID, 'product_cat' );
	foreach ( $terms as $term ) {
		$product_cat = $term->name;
		break;
	}
	$large_image_url      = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	$sdBrand              = $product_cat;
	$sdProductName        = get_the_title();
	$sdProductImage       = esc_url( $large_image_url[0] );
	$sdProductDescription = strip_tags( get_the_excerpt() );
	?>

	<script type='application/ld+json'>
    {
      "@context": "http://www.schema.org",
      "@type": "product",
      "brand": "<?php echo $sdBrand; ?>",
      "name": "<?php echo $sdProductName; ?>",
      "image": "<?php echo $sdProductImage; ?>",
      "description": "<?php echo $sdProductDescription; ?>"
    }












	</script>
	<!--End Structured Data-->

	<?php get_footer(); ?>
