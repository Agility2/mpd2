<?php
/*
 * Template Name: Hot Tub
 * Template Post Type: product
 */

get_header(); ?>
<?php
/*Template for MPD2*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$pageID = $post->ID;
$terms  = get_the_terms( $post->ID, 'product_cat' );
foreach ( $terms as $term ) {
	$categories[] = $term->slug;
}
?>

<style>

	/* =========================================================================
	Fixed Menu
	========================================================================== */
	#dsMenu {
		background-color: <?php the_field('whtSectionNavBG'); ?>;
	}

	#dsMenu ul a {
		color: <?php the_field('whtSectionNavTextColor'); ?>;
	}

	#dsMenu.fixed {
		background-color: <?php the_field('whtSectionNavBG'); ?>;
	}

	.dsCSLogo {
		display: none;
	}

	/* =========================================================================
	Jet Specs
	========================================================================== */
	.jetSystemASpot {
		background: #444 url('<?php echo $jetSystemASpot; ?>');
	}

	/* =========================================================================
360 Views
========================================================================== */

	.cd-product-viewer-wrapper .product-sprite {
		position: absolute;
		z-index: 2;
		top: 0;
		left: 0;
		height: 100%;
		/* our image sprite is composed by 37 frames */

		background: url('<?php the_field('three_sixty_model'); ?>') no-repeat center center;
		background-size: 100%;
		opacity: 0;
		-webkit-transition: opacity 0.3s;
		-moz-transition: opacity 0.3s;
		transition: opacity 0.3s;
	}
</style>

<?php
if ( ( in_array( "caldera-spas", $categories ) ) OR ( in_array( "hot-spring", $categories ) ) OR ( in_array( "fantasy-spas", $categories ) ) OR ( in_array( "freeflow-spas", $categories ) ) ) {

//WATKINS HOT TUB TEMPLATE
//get all the variables here
$breadcrumbCheck  = get_field( "defaultBreadcrumbsQ" ); // true or false
$whtTitleOverride = get_field( 'whtTitleOverride' ); // Text
$htDescription    = get_field( 'whtDescription' ); // content
$reviewWQ         = get_field( 'whtReviewWQ' ); // true or false
$reviewCode       = get_field( 'whtRWCode' ); // code
$three60Btn       = get_field( 'wht360Btn' ); // true or false
$CTANav           = get_field( 'whtCTANav' ); // repeater
$colorSelectors   = get_field( 'whtColorSelector' ); //repeater
$brandLogo        = get_field( 'spa-brand-logo' ); //text

if ( ! ( $brandLogo ) ) {
	// This is to fill the brand logo automatically if one isn't entered.
	$product_brand = '';
	$brands        = array( 'hot-spring', 'caldera-spas', 'fantasy-spas', 'freeflow-spas', 'endless-pools' );
	$current_url   = get_permalink();
	$brandSlug;
	foreach ( $brands as $brand ) {
		if ( stripos( $current_url, $brand ) !== false ) {
			$brandLogo = 'https://watkinsdealer.s3.amazonaws.com/branding/icons/' . $brand . '-mark.png';
			$brandSlug = $brand;
		} else {
			null;
		}
	}

	if ( stripos( $current_url, 'hot-spring' ) !== false ) {
		$brandSlogan = "<strong>Hot Spring<sup>®</sup>Spas</strong> <p>Quality made to last.</p>";
	} elseif ( stripos( $current_url, 'caldera-spas' ) !== false ) {
		$brandSlogan = "Experience the Pure Comfort<sup>®</sup>, design and performance of Caldera Spas.";
	} elseif ( stripos( $current_url, 'freeflow-spas' ) !== false ) {
		$brandSlogan = "Plug-in to the joys of hot tubbing!";
	} elseif ( stripos( $current_url, 'fantasy-spas' ) !== false ) {
		$brandSlogan = "The wait is over.  Step into your fantasy!";
	}

} // End automatic brand logo.


?>

<?php
global $post;
$post_slug = $post->post_name;
?>

<div class="container pt-3 pb-md-5 <?php echo $post_slug; ?>">
	<div class="row productInfo-container pt-md-5">
		<div class="elementor-widget-container productInfo col-12 col-md-6 col-lg-5">
			<div
				class="elementor-widget-heading elementor-widget-text-editor align-items-center d-none d-md-flex d-lg-flex">
				<?php if ( $brandLogo ) { ?>
					<div class="brand-logo mr-3 d-block">
						<a href="/hot-tubs/<?php echo $brandSlug; ?>/"><img src="<?php echo $brandLogo ?>"></a>
						<div class="talk-bubble tri-right round border right-top">
							<div class="talktext">
								<p><?php echo $brandSlogan ?></p>
							</div>
						</div>
					</div>
				<?php } ?>


				<div class="heading-container">

					<h1 class="elementor-heading-title title-lg-up"><?php //Title of Hot Tub
						// If title override, use that. else, use regular title.
						if ( ! empty( $whtTitleOverride ) ) {
							echo $whtTitleOverride;
						} else {
							the_title();
						}
						?>
					</h1>
					<?php if ( the_field( 'sub-title-hot-tub' ) ): ?>
						<p class="font-weight-bold title-lg-up"><?php the_field( 'sub-title-hot-tub' ); ?></p>
					<?php endif; ?>

				</div>


			</div>

			<div class="middle-container d-flex align-items-center justify-content-between mb-3 mt-md-3 pt-2 pb-2">
				<?php if ( $reviewWQ == 0 && $three60Btn == 0 ) { ?>
					<style>
						.middle-container {
							display: none !important;
						}
					</style>
				<?php } ?>

				<?php if ( $reviewWQ == 1 ) { ?>
					<div class="productReviews">
						<?php echo $reviewCode; ?>
					</div>
				<?php } ?>


				<?php if ( $three60Btn ) { ?>
				<div class="button360-container">
					<a href="" data-toggle="modal"
					   data-target="#modal360"><img
							src="https://csp-mpd2.s3.amazonaws.com/360-icon.png"></a>
				</div>
				<!--360 Modal-->
				<div class="modal fade" id="modal360" tabindex="-1" role="dialog"
				     aria-labelledby="modal360Label<?php echo the_title(); ?>" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<div class="the360container">
									<div id="spin360imgs" class="spritespin-instance with-canvas">

										<?php
										$actual_link = "$_SERVER[REQUEST_URI]";

											/*If SX or TX series*/
											if ( $actual_link == '/product/hot-tubs/hot-spring/sx/' || $actual_link == '/product/hot-tubs/hot-spring/tx/' ) { ?>
											<style>
												.cd-product-viewer-wrapper .product-sprite {
													width: 3700%;
												}
											</style>
											<div class="cd-product-viewer-wrapper" data-frame="37"
											     data-friction="0.33">
												<?php }
												else { ?>
												<style>
													.cd-product-viewer-wrapper .product-sprite {
														width: 1600%;
													}
												</style>

												<div class="cd-product-viewer-wrapper" data-frame="16"
												     data-friction="0.33">
													<?php } ?>


													<div>
														<figure class="product-viewer">
															<img
																src="https://csp-caldera-spas.s3.amazonaws.com/products/360-views/cantabria/img-360-loading.jpg"
																alt="Product Preview">
															<div class="product-sprite"
															     data-image="<?php the_field( 'three_sixty_model' ); ?>"></div>
														</figure> <!-- .product-viewer -->
														<div class="cd-product-viewer-handle">
															<span class="fill"></span>
															<span class="handle">Handle</span>
														</div>
													</div> <!-- .cd-product-viewer-handle -->
												</div> <!-- .cd-product-viewer-wrapper -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>

					<div class="spa-excerpt elementor-widget-text-editor">
						<?php //Description of Hot Tub
						echo $htDescription; ?>
					</div>
					<!-- Call to Action Navigation -->
					<?php if ( get_field( 'whtCTANav' ) ) { ?>
						<?php $count = 0; ?>
						<div
							class="productCTAs elementor-widget-text-editor d-flex flex-wrap align-items-md-center flex-md-row">
						<?php while ( has_sub_field( 'whtCTANav' ) ) {
							$ctaName  = get_sub_field( 'ctaName' );
							$ctaURL   = get_sub_field( 'ctaURL' );
							$ctaModal = get_sub_field( 'cta_single_modal' );
							if ( $ctaModal ) { ?>
								<a data-toggle="modal" data-target="#ctaVideo<?php echo $count; ?>"
								   class="btn ctaNav"><?php echo $ctaName; ?></a>
								<!-- Modal -->
								<div class="modal fade" id="ctaVideo<?php echo $count; ?>">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-body">
												<video style="width: 100%;"
												       poster="<?php the_sub_field( 'modal_single_poster_image' ); ?>"
												       controls="" src="<?php echo $ctaURL; ?>" frameborder="0"
												       allowfullscreen=""></video>
											</div>
										</div>
									</div>
								</div> <!-- end modal -->
							<?php } else { ?>

								<a href="<?php echo $ctaURL; ?>"
								   class="btn btn-primary mb-2"><?php echo $ctaName; ?></a>

							<?php } ?>
							<?php $count ++ ?>
						<?php } ?></div><?php } ?>
				</div>


				<div
					class="productStylesWrap elementor-widget-heading elementor-widget-text-editor col-12 col-md-6 col-lg-7 d-flex flex-column">
					<div
						class="elementor-widget-text-editor d-flex align-items-center flex-wrap mt-4 mb-5 d-md-none d-lg-none">
						<?php if ( $brandLogo ) { ?>
							<div class="brand-logo mr-3">
								<a href="/hot-tubs/<?php echo $brandSlug; ?>/">
									<img src="<?php echo $brandLogo ?>">
								</a>
							</div>
						<?php } ?>
						<div class="elementor-widget-heading">
							<h1 class="title-md-down elementor-heading-title"><?php //Title of Hot Tub
								// If title override, use that. else, use regular title.
								if ( ! empty( $whtTitleOverride ) ) {
									echo $whtTitleOverride;
								} else {
									the_title();
								}
								?>
							</h1>
							<span
								class="font-weight-bold title-md-down"><?php the_field( 'sub-title-hot-tub' ); ?></span>
						</div>
					</div>


					<div class="productStyles">
						<div
							class="productStylesIntro elementor-widget-text-editor text-md-center mt-3 mb-md-3 pt-1 pb-1">

							<h4 class="elementor-heading-title mb-0">Design Your Hot Tub</h4>

						</div>

						<div class="productSelectors d-md-flex justify-content-between">
							<div class="cabinetImages elementor-widget-text-editor mr-md-3 mt-3 mt-md-0">
								<!--cabinet swatch images-->

								<?php if ( have_rows( 'shellColors' ) ) { ?>

									<h5 class="cabinetNames"><strong>Cabinets:</strong> <span></span></h5>

								<?php } else { ?>

									<h5 class="cabinetNames"><strong>Cabinets:</strong> <span></span></h5>


								<?php } ?>
								<?php
								if ( have_rows( 'whtColorSelector' ) ) {
									$cabinetNum = 0;
									$cabinets   = [];
									while ( have_rows( 'whtColorSelector' ) ) {
										the_row();

										$cabinetName              = get_sub_field( 'cabinetColor' ); //text
										$cabinetSwatchImage       = get_sub_field( 'cabinetColorImage' ); //text cdn url
										$cabinetImage             = get_sub_field( 'cabinetImage' ); //text cdn url
										$cabinetShellColors       = get_sub_field( 'shellColors' ); //repeater
										$cabinets[ $cabinetName ] = $cabinetImage; ?>

										<div class="swatch<?php if ( $cabinetNum == "0" ) {
											echo " active";
										} ?>">
											<img data-cabinet="<?php echo $cabinetName; ?>"
											     src="<?php echo $cabinetSwatchImage; ?>"/>
											<!-- <p><?php echo $cabinetName; ?></p> -->
										</div>
										<?php $cabinetNum ++;
									}
								} ?>
							</div> <!--end cabinet image swatches-->
							<?php
							//get shells
							if ( have_rows( 'whtColorSelector' ) ) {
								$cabinetNum = 0;
								while ( have_rows( 'whtColorSelector' ) ) {
									the_row();
									$cabinetName = get_sub_field( 'cabinetColor' ); //text
									?>

									<?php
									if ( have_rows( 'shellColors' ) ) {
										$shellNum = 0; ?>

										<div class="shellImages elementor-widget-text-editor mt-3 mt-md-0"
										     data-shellfor="<?php echo $cabinetName; ?>">
											<h5 class="shellNames text-md-right"><strong>Shells:</strong> <span></span>
											</h5>
											<?php
											while ( have_rows( 'shellColors' ) ) {
												the_row();
												$shellName            = get_sub_field( 'shellColor' );
												$shellSwatchImage     = get_sub_field( 'shellColorImage' );
												$shellImage           = get_sub_field( 'shellImage' );
												$shells[ $shellName ] = $shellImage;
												?>
												<div class="swatch<?php if ( $shellNum == "0" ) {
													echo " active";
												} ?>">
													<img data-shell="<?php echo $shellName; ?>"
													     src="<?php echo $shellSwatchImage; ?>">
													<!-- <p><?php echo $shellName; ?></p> -->
												</div>
												<?php $shellNum ++;
											} ?>
										</div>
									<?php } ?>

								<?php }
							} ?>
						</div>
					</div>

					<div class="productImage">
						<div class="spaTop">
							<?php if ( in_array( "caldera-spas", $categories ) OR in_array( "hot-spring", $categories ) ) { ?>
								<?php
								$shells = array_unique( $shells );
								foreach ( $shells as $shellName => $shellImg ) {
									echo '<img data-shell="' . $shellName . '" src="' . $shellImg . '" />';
								}
							}
							?>
						</div>
						<div class="spaBottom">
							<?php
							$cabinets = array_unique( $cabinets );
							foreach ( $cabinets as $cabName => $cabImg ) {
								echo '<img data-cabinet="' . $cabName . '" src="' . $cabImg . '" />';
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div> <!--end container-->

		<?php } ?>

		<?php the_content(); ?>


		<!--Structured Data-->
		<?php
		global $post;
		$terms = get_the_terms( $post->ID, 'product_cat' );
		foreach ( $terms as $term ) {
			$product_cat = $term->name;
			break;
		}
		$large_image_url      = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
		$sdBrand              = $product_cat;
		$sdProductName        = get_the_title();
		$sdProductImage       = esc_url( $large_image_url[0] );
		$sdProductDescription = strip_tags( get_the_excerpt() );
		?>

		<script type='application/ld+json'>
    {
      "@context": "http://www.schema.org",
      "@type": "product",
      "brand": "<?php echo $sdBrand; ?>",
      "name": "<?php echo $sdProductName; ?>",
      "image": "<?php echo $sdProductImage; ?>",
      "description": "<?php echo $sdProductDescription; ?>"
    }





		</script>
		<!--End Structured Data-->

		<!-- Fancy Box -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css"/>
		<script type="text/javascript"
		        src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
		<script>
			(function ($) {
				$(document).ready(function () {
					$(".fancybox").attr('rel', 'image').fancybox({
						type: 'iframe',
						autoSize: true,
						helpers: {
							overlay: {
								locked: false
							}
						}
					});
				});
			})(jQuery);
		</script>

		<?php get_footer(); ?>
