<?php
/**
 * Template Name: Front Page
 *
 * @package WordPress
 * @subpackage DesignStudio
 */
get_header();
?>


<?php
if ( have_posts() ) :

	/* Start the Loop */
	while ( have_posts() ) : the_post();

		the_content();

	endwhile;

else :

endif;
?>

<?php
//footer.php - includes the footer switcher and version
get_footer(); ?>
